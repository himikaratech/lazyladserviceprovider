package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by user on 17-07-2015.
 */
public class AccountAdapter extends BaseAdapter {

    private Activity m_activity;
    private ArrayList<AccountSnippet> m_accountList;

    private class ViewHolder
    {
        TextView descriptionTextView;
        TextView amountTextView;
        TextView timestampTextView;
        TextView transactionIdTextView;
        TextView cordTextView;
        TextView statusTextView;
        TextView transactionText;
        View transactionView;

    }

    public AccountAdapter(Activity activity, ArrayList<AccountSnippet> accountList) {
        m_activity = activity;
        m_accountList = accountList;
    }

    @Override
    public int getCount() {
        int count=0;
        if(m_accountList != null){
            count=m_accountList.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if(m_accountList != null){
            return m_accountList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if(m_accountList != null){
            return Integer.parseInt(m_accountList.get(position).trans_id);
        }
        return 0;
    }


    public static String GetHumanReadableDate(long epochSec, String dateFormatStr) {
        Date date = new Date(epochSec * 1000);
        SimpleDateFormat format = new SimpleDateFormat(dateFormatStr);
        return format.format(date);

    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(m_accountList!=null){

            String accountDesc=((AccountSnippet) getItem(position)).description;
            String accountCord=((AccountSnippet) getItem(position)).credit_debit;
            Double accountAmount=((AccountSnippet) getItem(position)).amount;
           // Long accountTime=((AccountSnippet) getItem(position)).timestamp;
            Long accountTime = ((AccountSnippet) getItem(position)).timestamp;
            final String t_type=((AccountSnippet) getItem(position)).trans_type;
            String accountExpectedTime = GetHumanReadableDate(accountTime, "E LLL dd, yyyy HH:mm");

            String accountStatus=((AccountSnippet) getItem(position)).status;
            final String transactionId=((AccountSnippet) getItem(position)).trans_id;

            ViewHolder vh ;
            if(null == convertView) {
                vh = new ViewHolder();

                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.account_display_list, (ViewGroup) null);

                vh.transactionIdTextView = (TextView) convertView.findViewById(R.id.transaction_id_textview);
                vh.descriptionTextView = (TextView) convertView.findViewById(R.id.description_textview);
                vh.cordTextView = (TextView) convertView.findViewById(R.id.cord_texview);
                vh.amountTextView = (TextView) convertView.findViewById(R.id.amount_textview);
                vh.timestampTextView = (TextView) convertView.findViewById(R.id.timestamp_textview);
                vh.statusTextView = (TextView) convertView.findViewById(R.id.status_textview);
                vh.transactionText = (TextView)convertView.findViewById(R.id.transaction_no);
                vh.transactionView=convertView.findViewById(R.id.transaction_view);
                convertView.setTag(vh);
            }
            else
                vh = (ViewHolder)convertView.getTag();


            vh.transactionIdTextView.setText(transactionId);
            vh.descriptionTextView.setText(accountDesc);
            vh.cordTextView.setText(accountCord);
            vh.timestampTextView.setText(accountExpectedTime);
            vh.statusTextView.setText(accountStatus);


            if (accountCord.equals("Credit")) {
                vh.amountTextView.setTextColor(Color.parseColor("#78bd1e"));
                vh.cordTextView.setTextColor(Color.parseColor("#78bd1e"));
                vh.amountTextView.setText("+ ₹ " + String.valueOf(accountAmount));
                vh.cordTextView.setText(accountCord);
            } else if (accountCord.equals("Debit")) {
                vh.amountTextView.setTextColor(Color.parseColor("#ff4f4f"));
                vh.cordTextView.setTextColor(Color.parseColor("#ff4f4f"));
                vh.amountTextView.setText("- ₹ " + String.valueOf(accountAmount));
                vh.cordTextView.setText(accountCord);
            }


            vh.transactionView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(t_type.equals("ORDER_FINISH_SELLER"))
                    {
                        Intent intent = new Intent(m_activity, OrderFinishedDetails.class);
                        intent.putExtra(LazyCosntant.T_ID,transactionId);
                        m_activity.startActivity(intent);
                    }
                }
            });


            return convertView;
        }
        return null;
    }

    private void startCurrentOrderDetilsActivity(String transactionID){
        Intent intent = new Intent(m_activity, AccountListDetails.class);
        String data[] = {transactionID};
        intent.putExtra("data_send", data);
        m_activity.startActivity(intent);
    }
}