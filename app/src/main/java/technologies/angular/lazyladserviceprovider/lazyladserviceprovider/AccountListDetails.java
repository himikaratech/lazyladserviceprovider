package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by user on 17-07-2015.
 */
public class AccountListDetails extends ActionBarActivity{

    private String m_spCode;
    private AccountListDetailsAdapter m_accountDetailsAdapter;
    private ProgressDialog pDialog;
    private ListView m_accountDetails;
    ArrayList<AccountListDetailsSnippet> m_account;

    private Boolean dataLoading = false;
    private String transactionId;

    private JSONObject m_responseFromServer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.accountdetails);

        Intent in = new Intent();
        transactionId = in.getExtras().getString("data_send");

        if(!Utils.isNetworkAvailable(this)){
            Toast.makeText(this, "Please Check Your Network Connectivity", Toast.LENGTH_SHORT).show();
        }

        Cursor m_cursor = getContentResolver().query(
                LazyLadServiceProviderContract.UserDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if(m_cursor!=null && m_cursor.moveToFirst()){
            if(m_cursor.getCount()!=0){
                for(int i=0; i<m_cursor.getCount(); i++){
                    m_spCode=m_cursor.getString(1);
                    m_cursor.moveToNext();
                }
                //m_addressTextView.setText(m_userAddress);
            }else{
                //TODO Some UI Element asking to add address
            }
        }else{
            //TODO Error Handling
        }
        if(m_cursor != null)m_cursor.close();

        m_accountDetails = (ListView)findViewById(R.id.account_details_detailed_list_view);
        getReadyAccountList();
    }

    private void getReadyAccountList() {
        SessionManager m_sessionManager;
        m_sessionManager=new SessionManager(AccountListDetails.this);

        final ProgressDialog pDialog;

        pDialog = new ProgressDialog(AccountListDetails.this);
        pDialog.setMessage("Signing In. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        String url_items_serviceProvider = "http://www.angulartechnologies.com/task_manager/v1/accountDetailsDetailed";
        String param=m_spCode;
        url_items_serviceProvider=url_items_serviceProvider + "/" + param + "/" + transactionId;

        GsonRequest<AccountDetailsModel> jsObjRequest = new GsonRequest<AccountDetailsModel>(Request.Method.POST,
                url_items_serviceProvider, AccountDetailsModel.class, null, new Response.Listener<AccountDetailsModel>() {
            @Override
            public void onResponse(AccountDetailsModel response) {
                m_account=new ArrayList<AccountListDetailsSnippet>();
                int success =response.error;
                if(success == 1) {
                    m_account = response.account_details_detailed;
                    m_accountDetailsAdapter = new AccountListDetailsAdapter(AccountListDetails.this, m_account);
                    m_accountDetails.setAdapter((ListAdapter) m_accountDetailsAdapter);
                    pDialog.dismiss();
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error even: ", error+" retro");
                pDialog.dismiss();

            }
        });
        m_sessionManager.addToRequestQueue(jsObjRequest);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (pDialog != null) {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
        }
    }

}