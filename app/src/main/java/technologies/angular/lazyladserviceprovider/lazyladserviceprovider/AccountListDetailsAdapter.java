package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by user on 17-07-2015.
 */
public class AccountListDetailsAdapter extends BaseAdapter {

    private Activity m_activity;
    private ArrayList<AccountListDetailsSnippet> m_accountDetailsList;

    private class ViewHolder
    {
        TextView priceDifferenceTextView;
        TextView lazyChargeTextView;
        TextView promotionAppliedTextView;
        TextView otherChargeTextView;
    }

    public AccountListDetailsAdapter(Activity activity, ArrayList<AccountListDetailsSnippet> accountList) {
        m_activity = activity;
        m_accountDetailsList = accountList;
    }

    @Override
    public int getCount() {
        int count=0;
        if(m_accountDetailsList != null){
            count=m_accountDetailsList.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if(m_accountDetailsList != null){
            return m_accountDetailsList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if(m_accountDetailsList != null){
            return m_accountDetailsList.get(position).m_netTotal;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(m_accountDetailsList!=null){

            final int orderId=((AccountListDetailsSnippet) getItem(position)).m_orderId;
            int accountpDifference=((AccountListDetailsSnippet) getItem(position)).m_pDiff;
            int accountpApplied=((AccountListDetailsSnippet) getItem(position)).m_pAppiled;
            int accountLazyCharge=((AccountListDetailsSnippet) getItem(position)).m_lazyCharge;
            int accountOtherCharge=((AccountListDetailsSnippet) getItem(position)).m_otherCharge;
            int accountNetTotal=((AccountListDetailsSnippet) getItem(position)).m_netTotal;
            final String transactionId=((AccountSnippet) getItem(position)).trans_id;

            ViewHolder vh ;
            if(null == convertView) {
                vh = new ViewHolder();

                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.account_details_display_list, (ViewGroup) null);

                vh.priceDifferenceTextView = (TextView) convertView.findViewById(R.id.pDiff_texview);
                vh.promotionAppliedTextView = (TextView) convertView.findViewById(R.id.pApplied_textview);
                vh.otherChargeTextView = (TextView) convertView.findViewById(R.id.otherCharges_textview);
                vh.lazyChargeTextView = (TextView) convertView.findViewById(R.id.lazyladCharge_textview);
                convertView.setTag(vh);
            }
            else
                vh = (ViewHolder)convertView.getTag();

            vh.priceDifferenceTextView.setText(accountpDifference);
            vh.promotionAppliedTextView.setText(accountpApplied);
            vh.otherChargeTextView.setText(accountOtherCharge);
            vh.lazyChargeTextView.setText(accountLazyCharge);

            vh.priceDifferenceTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startPriceDifferenceActivity(orderId, transactionId);
                }
            });

            return convertView;
        }
        return null;
    }

    private void startPriceDifferenceActivity(int orderId, String transactionId) {
        Intent intent = new Intent(m_activity, PriceDifferenceActivity.class);
        int orderId1 = orderId;
        String transaction = transactionId;
        intent.putExtra("data_orderID", orderId1);
        intent.putExtra("data_transactionID", transaction);
        m_activity.startActivity(intent);
    }

}