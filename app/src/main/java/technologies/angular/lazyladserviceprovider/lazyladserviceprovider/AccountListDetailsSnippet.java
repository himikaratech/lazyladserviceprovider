package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 17-07-2015.
 */
public class AccountListDetailsSnippet {

    @SerializedName("order_id")
    public int m_orderId;

    @SerializedName("price_difference")
    public int m_pDiff;

    @SerializedName("promotion_applied")
    public int m_pAppiled;

    @SerializedName("lazylad_charge")
    public int m_lazyCharge;

    @SerializedName("other_charge")
    public int m_otherCharge;

    @SerializedName("net_total")
    public int m_netTotal;

    public AccountListDetailsSnippet(){

    }

    public AccountListDetailsSnippet(int orderID, int pDiff, int pApplied, int lazyCharge, int otherCharge, int netTotal){
        m_lazyCharge = lazyCharge;
        m_netTotal = netTotal;
        m_orderId = orderID;
        m_pAppiled=pApplied;
        m_pDiff=pDiff;
        m_otherCharge=otherCharge;
    }
}