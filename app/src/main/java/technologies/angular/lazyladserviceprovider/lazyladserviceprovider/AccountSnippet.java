package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by user on 17-07-2015.
 */
public class AccountSnippet implements Serializable{

    @SerializedName("t_id")
    public String trans_id;

    @SerializedName("t_type")
    public String trans_type;

    @SerializedName("description")
    public String description;

    @SerializedName("CorD")
    public String credit_debit;

    @SerializedName("amount")
    public Double amount;

    @SerializedName("status")
    public String status;

    @SerializedName("time_transaction")
    public Long timestamp;



    public AccountSnippet(){

    }

    public AccountSnippet(String id, String type, String description, String cord, Double amount, String status, Long timestamp){
        trans_id = id;
        trans_type = type;
        this.description=description;
        credit_debit = cord;
        this.amount = amount;
        this.status = status;
        this.timestamp = timestamp;
    }
}