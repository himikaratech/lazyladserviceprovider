package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiRequestModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiResponseModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiSuggestionService;

/**
 * Created by user on 17-07-2015.
 */
public class Accounts extends Fragment {

    private Boolean dataLoading = false;
    private String m_spCode;
    private ListView m_accountList;
    private ArrayList<AccountSnippet> m_accountSnippet = null;
    private AccountAdapter m_accountAdapter = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTracker();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.account, container, false);

        if (!Utils.isNetworkAvailable(getActivity())) {
            Toast.makeText(getActivity(), "Please Check Your Network Connectivity", Toast.LENGTH_SHORT).show();
        }

        Cursor m_cursor = getActivity().getContentResolver().query(
                LazyLadServiceProviderContract.UserDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    m_spCode = m_cursor.getString(1);
                    m_cursor.moveToNext();
                }
            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }
        if (m_cursor != null) m_cursor.close();

        m_accountList = (ListView) rootView.findViewById(R.id.account_details_list_view);

        m_accountList.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) { // TODO Auto-generated method stub
                if (null == m_accountList)
                    return;

                int threshold = 1;
                int count = m_accountList.getCount();

                if (scrollState == SCROLL_STATE_IDLE) {
                    if (!dataLoading && m_accountList.getLastVisiblePosition() >= count - threshold) {
                        dataLoading = true;
                        getReadyAccountList();
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                //leave this empty
            }
        });
        return rootView;
    }

    private void getReadyAccountList() {

        final ProgressDialog pDialog;
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();



        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://www.angulartechnologies.com").setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);

        ApiRequestModel requestModel = new ApiRequestModel();
        ApiRequestModel.AccountsInput accountsinput = requestModel.new AccountsInput();
        accountsinput.user_code = String.valueOf(m_spCode);
        accountsinput.owner_type = 1;
        if (m_accountSnippet == null) {
            accountsinput.previous_tid = "0";
        } else {
            accountsinput.previous_tid = m_accountSnippet.get(m_accountSnippet.size() - 1).trans_id;
        }

        apiSuggestionsService.accountTransactions(accountsinput.user_code, accountsinput.owner_type,
                accountsinput.previous_tid, new Callback<ApiResponseModel.AccountsOutput>() {

            @Override
            public void success(ApiResponseModel.AccountsOutput accountsOutput, retrofit.client.Response response) {
                Boolean success = accountsOutput.success;
                if (success == true) {
                    if (null != accountsOutput.transactionsList && !accountsOutput.transactionsList.isEmpty()) {

                        if (m_accountSnippet == null) {
                            m_accountSnippet = accountsOutput.transactionsList;
                            m_accountAdapter = new AccountAdapter(getActivity(), m_accountSnippet);
                            m_accountList.setAdapter((ListAdapter) m_accountAdapter);
                        } else {
                            m_accountSnippet.addAll(accountsOutput.transactionsList);
                            m_accountAdapter.notifyDataSetChanged();
                        }
                    }
                }
                if (pDialog.isShowing())
                    pDialog.dismiss();
                    dataLoading = false;
            }

            @Override
            public void failure(final RetrofitError error) {
                if (pDialog.isShowing())
                    pDialog.dismiss();
                   dataLoading = false;
            }
        });
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getActivity().getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(this.getClass().getCanonicalName());

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    public void onStart() {
        super.onStart();
        m_accountSnippet = null;
        getReadyAccountList();
    }
}