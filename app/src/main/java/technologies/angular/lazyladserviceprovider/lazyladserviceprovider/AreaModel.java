package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Saurabh on 03/04/15.
 */
public class AreaModel {

    public int error;

    public ArrayList<AreaSnippet> areas;

    @SerializedName("city_image_add")
    public String m_imgAddress;

    @SerializedName("city_name")
    public String cityName;

}
