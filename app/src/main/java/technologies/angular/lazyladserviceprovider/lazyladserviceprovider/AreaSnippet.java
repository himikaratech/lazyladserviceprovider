package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Saurabh on 04/03/15.
 */
public class AreaSnippet {

    public int m_id;
    @SerializedName("area_code")
    public String m_areaCode;
    @SerializedName("area_name")
    public String m_areaName;
    @SerializedName("city_code")
    public String m_cityCode;

    public AreaSnippet() {
    }

    public AreaSnippet(int id, String areaCode, String areaName, String cityCode) {
        m_id = id;
        m_areaCode = areaCode;
        m_areaName = areaName;
        m_cityCode = cityCode;
    }
}
