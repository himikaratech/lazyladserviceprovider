package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiRequestModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiResponseModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiSuggestionService;

/**
 * Created by Amud on 17/02/16.
 */


public class ChargesTab extends Fragment {





    TextView m_numberOfOrders;
    TextView m_chargeAmount;
    TextView m_serviceTaxAmount;
    TextView m_totalChargeAmount;
    TextView m_lastPaymentDate;
    private String m_spCode;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTracker();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.charges_tab_layout, container, false);

        if (!Utils.isNetworkAvailable(getActivity())) {
            Toast.makeText(getActivity(), "Please Check Your Network Connectivity", Toast.LENGTH_SHORT).show();
        }

         m_numberOfOrders=(TextView)v.findViewById(R.id.no_of_orders);
         m_chargeAmount=(TextView)v.findViewById(R.id.charge_amount);
         m_serviceTaxAmount=(TextView)v.findViewById(R.id.service_tax_charged);
         m_totalChargeAmount=(TextView)v.findViewById(R.id.total_charge_amount);
         m_lastPaymentDate=(TextView)v.findViewById(R.id.last_payment_date);



        getChargeDetails();


        return v;
    }

    private void getChargeDetails() {


        Cursor m_cursor = getActivity().getContentResolver().query(
                LazyLadServiceProviderContract.UserDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    m_spCode = m_cursor.getString(1);
                    m_cursor.moveToNext();
                }
            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }
        if(m_cursor != null)m_cursor.close();

        final ProgressDialog pDialog;
        pDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
        pDialog.setMessage("Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        pDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://www.angulartechnologies.com").build();


        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
        ApiRequestModel requestModel = new ApiRequestModel();
        ApiRequestModel.RequestBalanceInput input = requestModel.new RequestBalanceInput(m_spCode, 1,1,1);
        apiSuggestionsService.requestBalance(input, new Callback<ApiResponseModel.ResponseDataBalance>() {

            @Override
            public void success(ApiResponseModel.ResponseDataBalance output, retrofit.client.Response response) {
                boolean success = output.success;
                if (success == true) {

                    double charge_amount=output.charge_amount;
                    double service_tax_charged= output.charge_tax_amount;
                    int cut_order_no= output.charge_order_nos;
                      String last_payment_date=output.charge_last_settled;
                    double total_charge_amount=output.total_charge_amount;

                     m_numberOfOrders.setText(String.valueOf(cut_order_no));
                     m_chargeAmount.setText("₹ " +String.valueOf(charge_amount));
                     m_serviceTaxAmount.setText("₹ " +String.valueOf(service_tax_charged));
                     m_totalChargeAmount.setText("₹ " +String.valueOf(total_charge_amount));
                   m_lastPaymentDate.setText(String.valueOf(last_payment_date));


                }
                if (pDialog.isShowing())
                    pDialog.dismiss();
            }

            @Override
            public void failure(final RetrofitError error) {
                if (pDialog.isShowing())
                    pDialog.dismiss();
            }
        });
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getActivity().getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(this.getClass().getCanonicalName());

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }

}
