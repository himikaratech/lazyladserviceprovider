package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

public class ContactsAdapter extends BaseAdapter {
    private Activity m_activity;
    private ArrayList<Contact> m_contactsList;
    public ArrayList<Boolean> cblist;

    public ContactsAdapter(Activity activity, ArrayList<Contact> contactsList) {
        m_activity = activity;
        m_contactsList = contactsList;
        cblist = new ArrayList<>(Collections.nCopies(m_contactsList.size(), false));
    }

    private class ViewHolder{
        TextView personNameView;
        TextView personMobileView;
        CheckBox contactCB;
    }

    @Override
    public int getCount() {
        int count=0;
        if(m_contactsList != null){
            count=m_contactsList.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if(m_contactsList != null){
            return m_contactsList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if(m_contactsList != null){
            //return Long.parseLong(m_contactsList.get(position).getMobile());
            return 1;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(m_contactsList != null) {
            ViewHolder vh;
            if (null == convertView) {
                vh = new ViewHolder();
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.listcontacts, parent, false);

                vh.personNameView = (TextView) (convertView.findViewById(R.id.person_name));
                vh.personMobileView = (TextView) (convertView.findViewById(R.id.person_mobile));
                vh.contactCB = (CheckBox) (convertView.findViewById(R.id.select_checkbox));
                convertView.setTag(vh);
            } else
                vh = (ViewHolder) convertView.getTag();

            vh.personNameView.setText(m_contactsList.get(position).getName());
            vh.personMobileView.setText(m_contactsList.get(position).getMobile());
            vh.contactCB.setTag(position);
            vh.contactCB.setChecked(cblist.get(position));
            vh.contactCB.setOnCheckedChangeListener(
                    new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView,
                                                     boolean isChecked) {
                            // Get the position that set for the checkbox using setTag.
                            int getPosition = (Integer) buttonView.getTag();
                            // Set the value of checkbox to maintain its state.
                            cblist.set(getPosition, buttonView.isChecked());
                        }
                    });
            return convertView;
        }
        return null;
    }
}