package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.UserDetails;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiRequestModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiResponseModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiSuggestionService;

public class CurrentOrderDetails extends AppCompatActivity {

    private static final String TAG = "CurrentOrderDetails";

    private String m_servProvId;
    private String m_currentOrderCode;
    private String m_userCode;
    Toolbar toolbar;

    private ArrayList<CurrentOrderDetailsSnippet> m_currentOrderDetailsList;
    private CurrentOrderDetailsAdapter m_currentOrderDetailsAdapter;
    private ListView m_currentOrderDetails;
    private Button m_confirmButton;

    private TextView m_to_be_paid;
    private TextView m_already_paid_online;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.current_order_details);

        if (!Utils.isNetworkAvailable(this)) {
            Toast.makeText(this, "Please Check Your Network Connectivity", Toast.LENGTH_SHORT).show();
        }


        m_currentOrderDetails = (ListView) findViewById(R.id.current_order_details_list_view);
        m_confirmButton = (Button) findViewById(R.id.confirm_order_items_button);
        m_confirmButton.setVisibility(View.INVISIBLE);   //Set Visible in post execute of load order details


        Intent intent = getIntent();
        String data[] = intent.getStringArrayExtra("data_send");
        m_currentOrderCode = data[0];
        m_userCode = data[1];

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("New Order:" + " " + m_currentOrderCode);

        Cursor m_cursor = getContentResolver().query(
                UserDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    m_servProvId = m_cursor.getString(1);
                    m_cursor.moveToNext();
                }
            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }

        LoadCurrentOrderDetails();

        m_already_paid_online = (TextView) findViewById(R.id.already_paid_online);
        m_to_be_paid = (TextView) findViewById(R.id.to_be_paid);

        loadOrderDetails();

        m_confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConifrmCurrentOrder();
            }
        });
        setTracker();
    }

    private void ConifrmCurrentOrder() {
        ConfirmCurrentOrder();
    }

    private void loadOrderDetails() {
        final ProgressDialog pDialog = new ProgressDialog(CurrentOrderDetails.this);
        pDialog.setMessage("Loading Orders Details. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        RestAdapter restAdapter = Utils.providesRestAdapter("http://www.angulartechnologies.com");
        restAdapter.setLogLevel(RestAdapter.LogLevel.FULL);
        ApiRequestModel requestModel = new ApiRequestModel();
        final ApiRequestModel.LoadOrderRequestModel loadOrderRequestModel = requestModel.new LoadOrderRequestModel();
        loadOrderRequestModel.user_code = m_userCode;
        loadOrderRequestModel.order_code = m_currentOrderCode;
        ApiSuggestionService apiSuggestionService = restAdapter.create(ApiSuggestionService.class);
        apiSuggestionService.getOrderDetails(loadOrderRequestModel, new Callback<ApiResponseModel.LoadOrderResponseModel>() {
            @Override
            public void success(ApiResponseModel.LoadOrderResponseModel loadOrderResponseModel, Response response) {
                int status = loadOrderResponseModel.error;
                ApiResponseModel.OrderPaymentDetails paymentDetails = loadOrderResponseModel.order_payment_details;
                if (status == 1) {
                    m_to_be_paid.setText(paymentDetails.to_be_paid);
                    m_already_paid_online.setText(paymentDetails.paid);
                    pDialog.dismiss();
                } else {
                    pDialog.dismiss();
                    Toast.makeText(CurrentOrderDetails.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                pDialog.dismiss();
                Log.e(TAG, error.getMessage());
                Toast.makeText(CurrentOrderDetails.this, getString(R.string.network_error), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void ConfirmCurrentOrder() {

        final ProgressDialog pDialog;

        pDialog = new ProgressDialog(CurrentOrderDetails.this);
        pDialog.setMessage("Confirming Orders Details. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        JSONArray jsonObject = new JSONArray();
        for (int i = 0; i < m_currentOrderDetailsList.size(); i++) {
            if (m_currentOrderDetailsList.get(i).m_itemConfirmed == 1) {
                JSONObject smallJsonObject = new JSONObject();

                String m_itemCode = String.valueOf(m_currentOrderDetailsList.get(i).m_itemCode);
                double m_itemCost = m_currentOrderDetailsList.get(i).m_itemCost;
                try {
                    smallJsonObject.put("item_code", m_itemCode);
                    smallJsonObject.put("item_cost", m_itemCost);
                    if (m_currentOrderDetailsList.get(i).m_priceChanged) {
                        smallJsonObject.put("price_flag", 1);
                    } else {
                        smallJsonObject.put("price_flag", 0);
                    }
                    jsonObject.put(smallJsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Utils.RestApiPathUrl).build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
        ApiRequestModel requestModel = new ApiRequestModel();
        ApiRequestModel.InputForConfirmCurrentOrders confirmCurrent = requestModel.new InputForConfirmCurrentOrders(jsonObject.toString(), m_currentOrderCode, m_userCode, m_servProvId);
        apiSuggestionsService.confirmCurrentOrdersDetails(confirmCurrent, new Callback<ApiResponseModel.ConfirmCurrentOrderDetailsModel>() {

            @Override
            public void success(ApiResponseModel.ConfirmCurrentOrderDetailsModel responseConfirmCurrentOrders, Response response) {
                int success = responseConfirmCurrentOrders.error;
                if (success == 1) {
                    Log.i(TAG, "Successful");
                } else {
                    //do nothing
                }
                pDialog.dismiss();
                onBackPressed();
            }

            @Override
            public void failure(final RetrofitError error) {
                pDialog.dismiss();
            }
        });
    }

    private void LoadCurrentOrderDetails() {

        final ProgressDialog pDialog;

        pDialog = new ProgressDialog(CurrentOrderDetails.this);
        pDialog.setMessage("Loading Orders Details. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Utils.RestApiPathUrl).build();

        ApiSuggestionService api = restAdapter.create(ApiSuggestionService.class);
        ApiRequestModel requestModel = new ApiRequestModel();
        ApiRequestModel.InputForCurrentOrders current = requestModel.new InputForCurrentOrders(m_servProvId, m_currentOrderCode);
        api.currentOrdersDetails(current, new Callback<ApiResponseModel.CurrentOrderDetailsModel>() {

            @Override
            public void success(ApiResponseModel.CurrentOrderDetailsModel responseCurrentOrders, Response response) {

                int success = responseCurrentOrders.error;
                if (success == 1) {

                    if (null != responseCurrentOrders.currentOrderDetailsList && !responseCurrentOrders.currentOrderDetailsList.isEmpty()) {

                        m_currentOrderDetailsList = responseCurrentOrders.currentOrderDetailsList;
                        m_currentOrderDetailsAdapter = new CurrentOrderDetailsAdapter(CurrentOrderDetails.this, m_currentOrderDetailsList);
                        m_currentOrderDetails.setAdapter((ListAdapter) m_currentOrderDetailsAdapter);
                        m_confirmButton.setVisibility(View.VISIBLE);
                    }
                }
                pDialog.dismiss();
            }

            @Override
            public void failure(final RetrofitError error) {
                pDialog.dismiss();
            }
        });
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(this.getClass().getCanonicalName());

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }
}
