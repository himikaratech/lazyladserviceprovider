package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Saurabh on 30/01/15.
 */
public class CurrentOrderDetailsAdapter extends BaseAdapter {


    private ArrayList<CurrentOrderDetailsSnippet> m_currentOrderDetails;
    private Activity m_activity;

    private class ViewHolder {
        TextView itemNameTextView;
        TextView itemQuantityTextView;
        TextView itemCostTextView;
        TextView itemDescTextView;
        CheckBox itemConfirmedCheckBox;
        EditText itemCostEditText;
        TextView itemPrice;
        TextView quantity;
        ImageButton changePriceButton;
        ImageView imageView;
    }

    public CurrentOrderDetailsAdapter(Activity activity, ArrayList<CurrentOrderDetailsSnippet> currentOrderDetails) {
        m_activity = activity;
        m_currentOrderDetails = currentOrderDetails;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (m_currentOrderDetails != null) {
            count = m_currentOrderDetails.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (m_currentOrderDetails != null) {
            return m_currentOrderDetails.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_currentOrderDetails != null) {
            return m_currentOrderDetails.get(position).m_itemId;
        }
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (m_currentOrderDetails != null) {

            final ViewHolder holder;

            String itemCode = ((CurrentOrderDetailsSnippet) getItem(position)).m_itemCode;
            String itemName = ((CurrentOrderDetailsSnippet) getItem(position)).m_itemName;
            String itemShortDesc = ((CurrentOrderDetailsSnippet) getItem(position)).m_itemShortDesc;
            int itemQuantity = ((CurrentOrderDetailsSnippet) getItem(position)).m_itemQuantity;
            double itemCost = ((CurrentOrderDetailsSnippet) getItem(position)).m_itemCost;
            String itemDesc = ((CurrentOrderDetailsSnippet) getItem(position)).m_itemDesc;
            String itemImageAdd = ((CurrentOrderDetailsSnippet) getItem(position)).m_itemImgAdd;

            final int pos = position;

            if (convertView == null) {
                holder = new ViewHolder();
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.current_order_details_display_list, (ViewGroup) null);
                holder.itemNameTextView = (TextView) convertView.findViewById(R.id.item_name_textview);
                holder.itemQuantityTextView = (TextView) convertView.findViewById(R.id.item_quantity_textview);
                holder.itemCostTextView = (TextView) convertView.findViewById(R.id.item_cost_textview);
                holder.itemDescTextView = (TextView) convertView.findViewById(R.id.item_desc_textview);
                holder.itemConfirmedCheckBox = (CheckBox) convertView.findViewById(R.id.item_confirmed_checbox);
                holder.itemCostEditText = (EditText) convertView.findViewById(R.id.cost_editview);
                holder.itemPrice = (TextView) convertView.findViewById(R.id.price_textview);
                holder.quantity = (TextView) convertView.findViewById(R.id.quantity_textview);
                holder.changePriceButton = (ImageButton) convertView.findViewById(R.id.price_button);
                holder.imageView = (ImageView) convertView.findViewById(R.id.current_item_image);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
                holder.itemConfirmedCheckBox.setOnCheckedChangeListener(null);
            }

            holder.itemNameTextView.setText(itemName);
            holder.itemQuantityTextView.setText(Integer.toString(itemQuantity));
            holder.itemCostTextView.setText(Double.toString(itemCost));
            holder.itemDescTextView.setText(itemDesc);

            Typeface typeFace = Typeface.createFromAsset(m_activity.getAssets(), "fonts/Roboto_Regular.ttf");
            holder.itemNameTextView.setTypeface(typeFace);
            holder.itemCostTextView.setTypeface(typeFace);
            holder.itemPrice.setTypeface(typeFace);
            holder.itemQuantityTextView.setTypeface(typeFace);
            holder.quantity.setTypeface(typeFace);

            if (itemImageAdd == null || itemImageAdd == "") itemImageAdd = "\"\"";
            Picasso.with(parent.getContext())
                    .load(itemImageAdd)
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.no_image)
                    .fit().centerInside()
                    .into(holder.imageView);

            if (1 == ((CurrentOrderDetailsSnippet) getItem(pos)).m_itemConfirmed)
                holder.itemConfirmedCheckBox.setChecked(true);
            else
                holder.itemConfirmedCheckBox.setChecked(false);

            if (holder.itemConfirmedCheckBox.isChecked() == false) {
                holder.itemConfirmedCheckBox.setTextColor(Color.parseColor("#b3b3b3"));
                holder.itemConfirmedCheckBox.setButtonDrawable(R.drawable.inactive);
            } else {
                holder.itemConfirmedCheckBox.setTextColor(Color.parseColor("#ffffff"));
                holder.itemConfirmedCheckBox.setButtonDrawable(R.drawable.active);
            }

            holder.itemConfirmedCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        ((CurrentOrderDetailsSnippet) getItem(pos)).m_itemConfirmed = 1;
                        holder.itemConfirmedCheckBox.setTextColor(Color.parseColor("#ffffff"));
                        holder.itemConfirmedCheckBox.setButtonDrawable(R.drawable.active);

                    } else {
                        ((CurrentOrderDetailsSnippet) getItem(pos)).m_itemConfirmed = 0;
                        holder.itemConfirmedCheckBox.setTextColor(Color.parseColor("#b3b3b3"));
                        holder.itemConfirmedCheckBox.setButtonDrawable(R.drawable.inactive);
                    }
                }
            });

            holder.changePriceButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    double cost;
                    String myString = holder.itemCostEditText.getText().toString();
                    if (myString.matches("^[0-9]*\\.?[0-9]*$") && !myString.isEmpty()) {
                        cost = Double.parseDouble(myString);
                        m_currentOrderDetails.get(position).m_itemCost = cost;
                        holder.itemCostTextView.setText(Double.toString(cost));
                        m_currentOrderDetails.get(position).m_priceChanged = true;
                    }
                }
            });
        }
        return convertView;
    }
}
