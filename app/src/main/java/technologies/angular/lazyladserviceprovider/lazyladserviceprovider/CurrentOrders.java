package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.lang.String;

import java.util.ArrayList;

import android.widget.AbsListView;


import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.UserDetails;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiRequestModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiResponseModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiSuggestionService;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class CurrentOrders extends Fragment {

    private Boolean dataLoading = false;

    private String m_servProvId;
    private ArrayList<CurrentOrdersSnippet> m_currentOrdersList = null;
    private CurrentOrdersAdapter m_currentOrdersAdapter = null;
    private ListView m_currentOrdersDetails;

    private static final String TAG = "CurrentOrders";
    public static final String EXTRA_MESSAGE = "message";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTracker();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.current_orders, container, false);

        if (!Utils.isNetworkAvailable(getActivity())) {
            Toast.makeText(getActivity(), "Please Check Your Network Connectivity", Toast.LENGTH_SHORT).show();
        }

        Cursor m_cursor = getActivity().getContentResolver().query(
                UserDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    m_servProvId = m_cursor.getString(1);
                    m_cursor.moveToNext();
                }
            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }

        m_currentOrdersDetails = (ListView) rootView.findViewById(R.id.current_orders_list_view);

        m_currentOrdersDetails.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) { // TODO Auto-generated method stub
                if (null == m_currentOrdersDetails)
                    return;

                int threshold = 1;
                int count = m_currentOrdersDetails.getCount();

                if (scrollState == SCROLL_STATE_IDLE) {
                    if (!dataLoading && m_currentOrdersDetails.getLastVisiblePosition() >= count - threshold) {
                        dataLoading = true;
                        LoadCurrentOrders();
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                //leave this empty
            }
        });
        return rootView;
    }

    /**
     * Receiving push messages
     */
    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
            // Waking up mobile if it is sleeping
            WakeLocker.acquire(getActivity());
            newMessage = "love is life";
            /**
             * Take appropriate action on this message
             * depending upon your app requirement
             * For now i am just displaying it on the screen
             * */

            // Showing received message
            Toast.makeText(getActivity(), "New Message: " + newMessage, Toast.LENGTH_LONG).show();

            // Releasing wake lock
            WakeLocker.release();

            LoadCurrentOrders();

        }
    };

    @Override
    public void onStart() {
        super.onStart();
        m_currentOrdersList = null;
        LoadCurrentOrders();
    }

    private void LoadCurrentOrders() {

        final ProgressDialog pDialog;
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Loading Orders. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        String orderId;
        if (m_currentOrdersList == null) {
            orderId = "0";
        } else {
            orderId = m_currentOrdersList.get(m_currentOrdersList.size() - 1).m_currentOrderCode;
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Utils.RestApiPathUrl).build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
        ApiRequestModel requestModel = new ApiRequestModel();
        ApiRequestModel.InputForOrders current = requestModel.new InputForOrders(m_servProvId, orderId);
        apiSuggestionsService.currentOrders(current, new Callback<ApiResponseModel.CurrentOrdersModel>() {

            @Override
            public void success(ApiResponseModel.CurrentOrdersModel responseCurrentOrders, Response response) {
                int success = responseCurrentOrders.error;
                if (success == 1) {
                    if (null != responseCurrentOrders.currentOrdersList && !responseCurrentOrders.currentOrdersList.isEmpty()) {

                        if (m_currentOrdersList == null) {

                            m_currentOrdersList = responseCurrentOrders.currentOrdersList;
                            m_currentOrdersAdapter = new CurrentOrdersAdapter(getActivity(), m_currentOrdersList);
                            m_currentOrdersDetails.setAdapter((ListAdapter) m_currentOrdersAdapter);

                        } else {
                            m_currentOrdersList.addAll(responseCurrentOrders.currentOrdersList);
                            m_currentOrdersAdapter.notifyDataSetChanged();
                        }
                    }
                }
                pDialog.dismiss();
                dataLoading = false;
            }

            @Override
            public void failure(final RetrofitError error) {
                Log.e(TAG + "Failure", error +" retro");
                pDialog.dismiss();
                dataLoading = false;
            }
        });
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getActivity().getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(this.getClass().getCanonicalName());

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }
}
