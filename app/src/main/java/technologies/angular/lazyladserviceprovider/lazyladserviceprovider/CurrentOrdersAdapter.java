package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Saurabh on 30/01/15.
 */
public class CurrentOrdersAdapter extends BaseAdapter {

    private ArrayList<CurrentOrdersSnippet> m_currentOrdersDetails;
    private Activity m_activity;

    public CurrentOrdersAdapter(Activity activity, ArrayList<CurrentOrdersSnippet> currentOrdersDetails) {
        m_activity = activity;
        m_currentOrdersDetails = currentOrdersDetails;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (m_currentOrdersDetails != null) {
            count = m_currentOrdersDetails.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (m_currentOrdersDetails != null) {
            return m_currentOrdersDetails.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_currentOrdersDetails != null) {
            return m_currentOrdersDetails.get(position).currentOrdersId;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (m_currentOrdersDetails != null) {
            final String currentOrderCode = ((CurrentOrdersSnippet) getItem(position)).m_currentOrderCode;
            String currentOrderAddress = ((CurrentOrdersSnippet) getItem(position)).m_address;
            int currentOrderAmount = ((CurrentOrdersSnippet) getItem(position)).m_amount;
            String currentOrderTimePlaced = ((CurrentOrdersSnippet) getItem(position)).m_timeOrderPlaced;
            String currentOrderUserName = ((CurrentOrdersSnippet) getItem(position)).m_userName;
            final String currentOrderPhoneNumber = ((CurrentOrdersSnippet) getItem(position)).m_userPhoneNum;
            final String userCode = ((CurrentOrdersSnippet) getItem(position)).m_userCode;
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.current_orders_display_list, (ViewGroup) null);
            }
            TextView currentOrderCodeTextView = (TextView) convertView.findViewById(R.id.current_order_code_textview);
            TextView currentOrderAddressTextView = (TextView) convertView.findViewById(R.id.current_order_address_textview);
            TextView currentOrderAmountTextView = (TextView) convertView.findViewById(R.id.amount_current_order_textview);
            TextView currentOrderTimePlacedTextView = (TextView) convertView.findViewById(R.id.current_order_del_textview);
            TextView currentOrderUserNameTextView = (TextView) convertView.findViewById(R.id.current_order_username_textview);
            TextView currentOrderUserPhoneTextView = (TextView) convertView.findViewById(R.id.current_order_number_textview);
            TextView currentOrderNumber = (TextView) convertView.findViewById(R.id.order_number);
            TextView viewDetail = (TextView) convertView.findViewById(R.id.view_detail_textview);
            LinearLayout currentOrderDetailView = (LinearLayout) convertView.findViewById(R.id.detail_layout);
            LinearLayout numberLayoutView = (LinearLayout) convertView.findViewById(R.id.number_layout);

            Typeface typeFace = Typeface.createFromAsset(m_activity.getAssets(), "fonts/Roboto_Medium.ttf");
            Typeface typeFace1 = Typeface.createFromAsset(m_activity.getAssets(), "fonts/Roboto_Regular.ttf");
            Typeface typeFace2 = Typeface.createFromAsset(m_activity.getAssets(), "fonts/Roboto_Light.ttf");

            currentOrderNumber.setTypeface(typeFace);
            currentOrderCodeTextView.setTypeface(typeFace);
            currentOrderAmountTextView.setTypeface(typeFace1);
            currentOrderTimePlacedTextView.setTypeface(typeFace1);
            currentOrderUserNameTextView.setTypeface(typeFace);
            currentOrderAddressTextView.setTypeface(typeFace1);
            currentOrderUserPhoneTextView.setTypeface(typeFace2);
            viewDetail.setTypeface(typeFace2);

            currentOrderCodeTextView.setText(currentOrderCode);
            currentOrderAddressTextView.setText(currentOrderAddress);
            currentOrderAmountTextView.setText("₹ " + Integer.toString(currentOrderAmount));
            currentOrderTimePlacedTextView.setText(currentOrderTimePlaced);
            currentOrderUserNameTextView.setText(currentOrderUserName);
            currentOrderUserPhoneTextView.setText(currentOrderPhoneNumber);

            numberLayoutView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel: " + currentOrderPhoneNumber));
                    m_activity.startActivity(callIntent);
                }
            });

            currentOrderDetailView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startCurrentOrderDetilsActivity(currentOrderCode, userCode);
                }
            });
        }
        return convertView;
    }

    private void startCurrentOrderDetilsActivity(String currentOrderCode, String userCode) {
        Intent intent = new Intent(m_activity, CurrentOrderDetails.class);
        String data[] = {currentOrderCode, userCode};
        intent.putExtra("data_send", data);
        m_activity.startActivity(intent);
    }

}
