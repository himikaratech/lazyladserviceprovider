package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.UserDetails;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.NonAvailableItems;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.OutOfStockItems;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.AvailableItems;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.ServiceTypeCategories;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.SearchResult;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.RegistrationSellerDetails;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.RegistrationSellerAreas;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.RegistrationInventoryDetails;

/**
 * Created by Saurabh on 11/02/15.
 */
public class DBHelper extends SQLiteOpenHelper {

    //latest release version in db = 2
    private static int DATABASE_VERSION = 7;

    private static String DATABASE_NAME = "LazyLadServProvData";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Version 1 tables

        final String SQL_CREATE_USER_DETAILS_TABLE = "CREATE TABLE IF NOT EXISTS " + UserDetails.TABLE_NAME + "(" +
                UserDetails._ID + " INTEGER PRIMARY KEY, " +
                UserDetails.COLUMN_SP_CODE + " TEXT, " +
                UserDetails.COLUMN_SP_EMAIL + " TEXT, " +
                UserDetails.COLUMN_SP_PASSWORD + " TEXT, " +
                UserDetails.COLUMN_ST_CODE + " TEXT " +
                " );";

        final String SQL_CREATE_ITEMS_NOT_AVAILABLE_TABLE = "CREATE TABLE IF NOT EXISTS " + NonAvailableItems.TABLE_NAME + " (" +
                NonAvailableItems._ID + " INTEGER PRIMARY KEY, " +
                NonAvailableItems.COLUMN_ITEM_CODE + " TEXT, " +
                NonAvailableItems.COLUMN_ITEM_NAME + " TEXT, " +
                NonAvailableItems.COLUMN_ITEM_IMG_FLAG + " INTEGER, " +
                NonAvailableItems.COLUMN_ITEM_IMG_ADD + " TEXT, " +
                NonAvailableItems.COLUMN_ITEM_UNIT + " TEXT, " +
                NonAvailableItems.COLUMN_ITEM_SHORT_DESC + " TEXT, " +
                NonAvailableItems.COLUMN_ITEM_DESC + " TEXT, " +
                NonAvailableItems.COLUMN_ITEM_STATUS + " INTEGER, " +
                NonAvailableItems.COLUMN_ITEM_COST + " DOUBLE, " +
                NonAvailableItems.COLUMN_ITEM_TYPE_CODE + " TEXT, " +
                NonAvailableItems.COLUMN_ITEM_QUANTITY + " TEXT " +
                " );";

        final String SQL_CREATE_ITEMS_OUT_OF_STOCK_TABLE = "CREATE TABLE IF NOT EXISTS " + OutOfStockItems.TABLE_NAME + " (" +
                OutOfStockItems._ID + " INTEGER PRIMARY KEY, " +
                OutOfStockItems.COLUMN_ITEM_CODE + " TEXT, " +
                OutOfStockItems.COLUMN_ITEM_NAME + " TEXT, " +
                OutOfStockItems.COLUMN_ITEM_IMG_FLAG + " INTEGER, " +
                OutOfStockItems.COLUMN_ITEM_IMG_ADD + " TEXT, " +
                OutOfStockItems.COLUMN_ITEM_UNIT + " TEXT, " +
                OutOfStockItems.COLUMN_ITEM_SHORT_DESC + " TEXT, " +
                OutOfStockItems.COLUMN_ITEM_DESC + " TEXT, " +
                OutOfStockItems.COLUMN_ITEM_STATUS + " INTEGER, " +
                OutOfStockItems.COLUMN_ITEM_COST + " DOUBLE, " +
                OutOfStockItems.COLUMN_ITEM_TYPE_CODE + " TEXT, " +
                OutOfStockItems.COLUMN_ITEM_QUANTITY + " TEXT " +
                " );";


        final String SQL_CREATE_ITEMS_ITEMS_AVAILABLE_TABLE = "CREATE TABLE IF NOT EXISTS " + AvailableItems.TABLE_NAME + " (" +
                AvailableItems._ID + " INTEGER PRIMARY KEY, " +
                AvailableItems.COLUMN_ITEM_CODE + " TEXT, " +
                AvailableItems.COLUMN_ITEM_NAME + " TEXT, " +
                AvailableItems.COLUMN_ITEM_IMG_FLAG + " INTEGER, " +
                AvailableItems.COLUMN_ITEM_IMG_ADD + " TEXT, " +
                AvailableItems.COLUMN_ITEM_UNIT + " TEXT, " +
                AvailableItems.COLUMN_ITEM_SHORT_DESC + " TEXT, " +
                AvailableItems.COLUMN_ITEM_DESC + " TEXT, " +
                AvailableItems.COLUMN_ITEM_STATUS + " INTEGER, " +
                AvailableItems.COLUMN_ITEM_COST + " DOUBLE, " +
                AvailableItems.COLUMN_ITEM_TYPE_CODE + " TEXT, " +
                AvailableItems.COLUMN_ITEM_QUANTITY + " TEXT " +
                " );";

        final String SQL_CREATE_SEARCH_RESULT_TABLE = "CREATE TABLE IF NOT EXISTS " + SearchResult.TABLE_NAME + " (" +
                SearchResult._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY ," +
                SearchResult.COLUMN_ITEM_ID + " TEXT, " +
                SearchResult.COLUMN_ITEM_CODE + " TEXT, " +
                SearchResult.COLUMN_ITEM_NAME + " TEXT, " +
                SearchResult.COLUMN_ITEM_IMG_FLAG + " TEXT, " +
                SearchResult.COLUMN_ITEM_IMG_ADD + " TEXT, " +
                SearchResult.COLUMN_ITEM_UNIT + " TEXT, " +
                SearchResult.COLUMN_ITEM_COST + " DOUBLE, " +
                SearchResult.COLUMN_ITEM_SHORT_DESC + " TEXT, " +
                SearchResult.COLUMN_ITEM_DESC + " TEXT, " +
                SearchResult.COLUMN_ITEM_STATUS + " INTEGER, " +
                SearchResult.COLUMN_ITEM_TYPE + " TEXT, " +
                SearchResult.COLUMN_ITEM_QUANTITY_SELECTED + " TEXT, " +
                SearchResult.COLUMN_SC_CODE + " TEXT " +
                " );";

        final String SQL_CREATE_SERVICE_TYPE_CATEGORY_TABLE = "CREATE TABLE IF NOT EXISTS " + ServiceTypeCategories.TABLE_NAME + " (" +
                ServiceTypeCategories._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY ," +
                ServiceTypeCategories.COLUMN_ST_CODE + " TEXT, " +
                ServiceTypeCategories.COLUMN_SC_CODE + " TEXT, " +
                ServiceTypeCategories.COLUMN_SC_NAME + " TEXT " +
                " );";

        final String SQL_CREATE_Registration_Service_Areas_TABLE = "CREATE TABLE IF NOT EXISTS " + RegistrationSellerAreas.TABLE_NAME + " (" +
                RegistrationSellerAreas._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY ," +
                RegistrationSellerAreas.COLUMN_AREA_CODE + " INTEGER, " +
                RegistrationSellerAreas.COLUMN_AREA_NAME + " TEXT, " +
                RegistrationSellerAreas.COLUMN_DELIVERABLE + " INTEGER DEFAULT 0 " +
                " );";

        final String SQL_CREATE_Registration_Inventory_Details_TABLE = "CREATE TABLE IF NOT EXISTS " + RegistrationInventoryDetails.TABLE_NAME + " (" +
                RegistrationInventoryDetails._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY ," +
                RegistrationInventoryDetails.COLUMN_ITEM_CODE + " INTEGER, " +
                RegistrationInventoryDetails.COLUMN_ITEM_NAME + " TEXT, " +
                RegistrationInventoryDetails.COLUMN_ITEM_COST + " DOUBLE, " +
                RegistrationInventoryDetails.COLUMN_ITEM_DESC + " TEXT, " +
                RegistrationInventoryDetails.COLUMN_ITEM_IMG_FLAG + " INTEGER, " +
                RegistrationInventoryDetails.COLUMN_ITEM_IMG_ADD + " TEXT, " +
                RegistrationInventoryDetails.COLUMN_AVAILABLE + " TEXT DEFAULT 1 " +
                " );";

        final String SQL_CREATE_Registration_Seller_Details_TABLE = "CREATE TABLE IF NOT EXISTS " + RegistrationSellerDetails.TABLE_NAME + " (" +
                RegistrationSellerDetails._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY ," +
                RegistrationSellerDetails.COLUMN_SELLER_NAME + " TEXT, " +
                RegistrationSellerDetails.COLUMN_SELLER_PHONE + " TEXT, " +
                RegistrationSellerDetails.COLUMN_SELLER_PHONE_VAL_FLAG + " INTEGER DEFAULT 0, " +
                RegistrationSellerDetails.COLUMN_PASSWORD + " TEXT, " +
                RegistrationSellerDetails.COLUMN_SHOP_NAME + " TEXT, " +
                RegistrationSellerDetails.COLUMN_SHOP_ADDRESS + " TEXT, " +
                RegistrationSellerDetails.COLUMN_SHOP_CITY + " INTEGER, " +
                RegistrationSellerDetails.COLUMN_SHOP_AREAS + " INTEGER, " +
                RegistrationSellerDetails.COLUMN_SHOP_TYPE + " INTEGER, " +
                RegistrationSellerDetails.COLUMN_SHOP_OPEN_TIME + " INTEGER DEFAULT 540, " +
                RegistrationSellerDetails.COLUMN_SHOP_CLOSE_TIME + " INTEGER DEFAULT 1260, " +
                RegistrationSellerDetails.COLUMN_DELIVERY_TIME + " INTEGER DEFAULT 90, " +
                RegistrationSellerDetails.COLUMN_MIN_DELIVERY_AMT + " INTEGER DEFAULT 200, " +
                RegistrationSellerDetails.COLUMN_SHOP_OPEN_BITS + " INTEGER DEFAULT 0, " +
                RegistrationSellerDetails.COLUMN_INVENTORY_BY_US + " INTEGER DEFAULT 0, " +
                RegistrationSellerDetails.COLUMN_LOGISTICS_SERVICES + " INTEGER DEFAULT 0, " +
                RegistrationSellerDetails.COLUMN_SODEXO_COUPONS + " INTEGER DEFAULT 0, " +
                RegistrationSellerDetails.COLUMN_MIDNIGHT_DELIVERY + " INTEGER DEFAULT 0, " +
                RegistrationSellerDetails.COLUMN_REFERRAL + " TEXT, " +
                RegistrationSellerDetails.COLUMN_VERIFICATION_SOURCE + " TEXT " +
                " );";

        db.execSQL(SQL_CREATE_USER_DETAILS_TABLE);
        db.execSQL(SQL_CREATE_ITEMS_NOT_AVAILABLE_TABLE);
        db.execSQL(SQL_CREATE_ITEMS_OUT_OF_STOCK_TABLE);
        db.execSQL(SQL_CREATE_ITEMS_ITEMS_AVAILABLE_TABLE);
        db.execSQL(SQL_CREATE_SERVICE_TYPE_CATEGORY_TABLE);
        db.execSQL(SQL_CREATE_SEARCH_RESULT_TABLE);
        db.execSQL(SQL_CREATE_Registration_Service_Areas_TABLE);
        db.execSQL(SQL_CREATE_Registration_Inventory_Details_TABLE);
        db.execSQL(SQL_CREATE_Registration_Seller_Details_TABLE);

        //Version 3 table updates
        // Added shop_area column in Registration_Seller_Details, its directly added to create table query for onCreate

        //Version 4
        //Added monday to sunday shop open bit column, inventory by us or not column

        //Version 5
        //Added logistics yes or no column, sodexo coupons yes or no column, midnight delivery yes or no column

        //Version 6
        //Added referral code String
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if (2 > oldVersion && 2 <= newVersion) {
            db.execSQL("CREATE TABLE IF NOT EXISTS " + RegistrationSellerDetails.TABLE_NAME + " (" +
                    RegistrationSellerDetails._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY ," +
                    RegistrationSellerDetails.COLUMN_SELLER_NAME + " TEXT, " +
                    RegistrationSellerDetails.COLUMN_SELLER_PHONE + " TEXT, " +
                    RegistrationSellerDetails.COLUMN_PASSWORD + " TEXT, " +
                    RegistrationSellerDetails.COLUMN_SHOP_NAME + " TEXT, " +
                    RegistrationSellerDetails.COLUMN_SHOP_ADDRESS + " TEXT, " +
                    RegistrationSellerDetails.COLUMN_SHOP_CITY + " INTEGER, " +
                    RegistrationSellerDetails.COLUMN_SHOP_TYPE + " INTEGER, " +
                    RegistrationSellerDetails.COLUMN_SHOP_OPEN_TIME + " INTEGER DEFAULT 540, " +
                    RegistrationSellerDetails.COLUMN_SHOP_CLOSE_TIME + " INTEGER DEFAULT 1260, " +
                    RegistrationSellerDetails.COLUMN_DELIVERY_TIME + " INTEGER DEFAULT 90, " +
                    RegistrationSellerDetails.COLUMN_MIN_DELIVERY_AMT + " INTEGER DEFAULT 200 " +
                    " );");

            db.execSQL("CREATE TABLE IF NOT EXISTS " + RegistrationSellerAreas.TABLE_NAME + " (" +
                    RegistrationSellerAreas._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY," +
                    RegistrationSellerAreas.COLUMN_AREA_CODE + " INTEGER, " +
                    RegistrationSellerAreas.COLUMN_AREA_NAME + " TEXT, " +
                    RegistrationSellerAreas.COLUMN_DELIVERABLE + " INTEGER DEFAULT 0 " +
                    " );");

            db.execSQL("CREATE TABLE IF NOT EXISTS " + RegistrationInventoryDetails.TABLE_NAME + " (" +
                    RegistrationInventoryDetails._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY ," +
                    RegistrationInventoryDetails.COLUMN_ITEM_CODE + " INTEGER, " +
                    RegistrationInventoryDetails.COLUMN_ITEM_NAME + " TEXT, " +
                    RegistrationInventoryDetails.COLUMN_ITEM_COST + " DOUBLE, " +
                    RegistrationInventoryDetails.COLUMN_ITEM_DESC + " TEXT, " +
                    RegistrationInventoryDetails.COLUMN_ITEM_IMG_FLAG + " INTEGER, " +
                    RegistrationInventoryDetails.COLUMN_ITEM_IMG_ADD + " TEXT, " +
                    RegistrationInventoryDetails.COLUMN_AVAILABLE + " TEXT DEFAULT 1 " +
                    " );");
        }

        if (3 > oldVersion && 3 <= newVersion) {
            db.execSQL("DROP TABLE " + RegistrationSellerDetails.TABLE_NAME);
            db.execSQL("CREATE TABLE IF NOT EXISTS " + RegistrationSellerDetails.TABLE_NAME + " (" +
                    RegistrationSellerDetails._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY ," +
                    RegistrationSellerDetails.COLUMN_SELLER_NAME + " TEXT, " +
                    RegistrationSellerDetails.COLUMN_SELLER_PHONE + " TEXT, " +
                    RegistrationSellerDetails.COLUMN_PASSWORD + " TEXT, " +
                    RegistrationSellerDetails.COLUMN_SHOP_NAME + " TEXT, " +
                    RegistrationSellerDetails.COLUMN_SHOP_ADDRESS + " TEXT, " +
                    RegistrationSellerDetails.COLUMN_SHOP_CITY + " INTEGER, " +
                    RegistrationSellerDetails.COLUMN_SHOP_AREAS + " INTEGER, " +
                    RegistrationSellerDetails.COLUMN_SHOP_TYPE + " INTEGER, " +
                    RegistrationSellerDetails.COLUMN_SHOP_OPEN_TIME + " INTEGER DEFAULT 540, " +
                    RegistrationSellerDetails.COLUMN_SHOP_CLOSE_TIME + " INTEGER DEFAULT 1260, " +
                    RegistrationSellerDetails.COLUMN_DELIVERY_TIME + " INTEGER DEFAULT 90, " +
                    RegistrationSellerDetails.COLUMN_MIN_DELIVERY_AMT + " INTEGER DEFAULT 200 " +
                    " );");
        }

        if (4 > oldVersion && 4 <= newVersion) {
            db.execSQL("DROP TABLE " + RegistrationSellerDetails.TABLE_NAME);
            db.execSQL("CREATE TABLE IF NOT EXISTS " + RegistrationSellerDetails.TABLE_NAME + " (" +
                    RegistrationSellerDetails._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY ," +
                    RegistrationSellerDetails.COLUMN_SELLER_NAME + " TEXT, " +
                    RegistrationSellerDetails.COLUMN_SELLER_PHONE + " TEXT, " +
                    RegistrationSellerDetails.COLUMN_SELLER_PHONE_VAL_FLAG + " INTEGER DEFAULT 0, " +
                    RegistrationSellerDetails.COLUMN_PASSWORD + " TEXT, " +
                    RegistrationSellerDetails.COLUMN_SHOP_NAME + " TEXT, " +
                    RegistrationSellerDetails.COLUMN_SHOP_ADDRESS + " TEXT, " +
                    RegistrationSellerDetails.COLUMN_SHOP_CITY + " INTEGER, " +
                    RegistrationSellerDetails.COLUMN_SHOP_AREAS + " INTEGER, " +
                    RegistrationSellerDetails.COLUMN_SHOP_TYPE + " INTEGER, " +
                    RegistrationSellerDetails.COLUMN_SHOP_OPEN_TIME + " INTEGER DEFAULT 540, " +
                    RegistrationSellerDetails.COLUMN_SHOP_CLOSE_TIME + " INTEGER DEFAULT 1260, " +
                    RegistrationSellerDetails.COLUMN_DELIVERY_TIME + " INTEGER DEFAULT 90, " +
                    RegistrationSellerDetails.COLUMN_MIN_DELIVERY_AMT + " INTEGER DEFAULT 200, " +
                    RegistrationSellerDetails.COLUMN_SHOP_OPEN_BITS + " INTEGER DEFAULT 0, " +
                    RegistrationSellerDetails.COLUMN_INVENTORY_BY_US + " INTEGER DEFAULT 0 " +
                    " );");
        }

        if (5 > oldVersion && 5 <= newVersion) {
            db.execSQL("CREATE TABLE IF NOT EXISTS " + SearchResult.TABLE_NAME + " (" +
                    SearchResult._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY ," +
                    SearchResult.COLUMN_ITEM_ID + " TEXT, " +
                    SearchResult.COLUMN_ITEM_CODE + " TEXT, " +
                    SearchResult.COLUMN_ITEM_NAME + " TEXT, " +
                    SearchResult.COLUMN_ITEM_IMG_FLAG + " TEXT, " +
                    SearchResult.COLUMN_ITEM_IMG_ADD + " TEXT, " +
                    SearchResult.COLUMN_ITEM_UNIT + " TEXT, " +
                    SearchResult.COLUMN_ITEM_COST + " DOUBLE, " +
                    SearchResult.COLUMN_ITEM_SHORT_DESC + " TEXT, " +
                    SearchResult.COLUMN_ITEM_DESC + " TEXT, " +
                    SearchResult.COLUMN_ITEM_STATUS + " INTEGER, " +
                    SearchResult.COLUMN_ITEM_TYPE + " TEXT, " +
                    SearchResult.COLUMN_ITEM_QUANTITY_SELECTED + " TEXT, " +
                    SearchResult.COLUMN_SC_CODE + " TEXT " +
                    " );");

            db.execSQL("DROP TABLE " + RegistrationSellerDetails.TABLE_NAME);

            db.execSQL("CREATE TABLE IF NOT EXISTS " + RegistrationSellerDetails.TABLE_NAME + " (" +
                    RegistrationSellerDetails._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY ," +
                    RegistrationSellerDetails.COLUMN_SELLER_NAME + " TEXT, " +
                    RegistrationSellerDetails.COLUMN_SELLER_PHONE + " TEXT, " +
                    RegistrationSellerDetails.COLUMN_SELLER_PHONE_VAL_FLAG + " INTEGER DEFAULT 0, " +
                    RegistrationSellerDetails.COLUMN_PASSWORD + " TEXT, " +
                    RegistrationSellerDetails.COLUMN_SHOP_NAME + " TEXT, " +
                    RegistrationSellerDetails.COLUMN_SHOP_ADDRESS + " TEXT, " +
                    RegistrationSellerDetails.COLUMN_SHOP_CITY + " INTEGER, " +
                    RegistrationSellerDetails.COLUMN_SHOP_AREAS + " INTEGER, " +
                    RegistrationSellerDetails.COLUMN_SHOP_TYPE + " INTEGER, " +
                    RegistrationSellerDetails.COLUMN_SHOP_OPEN_TIME + " INTEGER DEFAULT 540, " +
                    RegistrationSellerDetails.COLUMN_SHOP_CLOSE_TIME + " INTEGER DEFAULT 1260, " +
                    RegistrationSellerDetails.COLUMN_DELIVERY_TIME + " INTEGER DEFAULT 90, " +
                    RegistrationSellerDetails.COLUMN_MIN_DELIVERY_AMT + " INTEGER DEFAULT 200, " +
                    RegistrationSellerDetails.COLUMN_SHOP_OPEN_BITS + " INTEGER DEFAULT 0, " +
                    RegistrationSellerDetails.COLUMN_INVENTORY_BY_US + " INTEGER DEFAULT 0, " +
                    RegistrationSellerDetails.COLUMN_LOGISTICS_SERVICES + " INTEGER DEFAULT 0, " +
                    RegistrationSellerDetails.COLUMN_SODEXO_COUPONS + " INTEGER DEFAULT 0, " +
                    RegistrationSellerDetails.COLUMN_MIDNIGHT_DELIVERY + " INTEGER DEFAULT 0 " +
                    " );");
        }

        if (6 > oldVersion && 6 <= newVersion) {
            db.execSQL("DROP TABLE " + RegistrationSellerDetails.TABLE_NAME);
            db.execSQL("CREATE TABLE IF NOT EXISTS " + RegistrationSellerDetails.TABLE_NAME + " (" +
                    RegistrationSellerDetails._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY ," +
                    RegistrationSellerDetails.COLUMN_SELLER_NAME + " TEXT, " +
                    RegistrationSellerDetails.COLUMN_SELLER_PHONE + " TEXT, " +
                    RegistrationSellerDetails.COLUMN_SELLER_PHONE_VAL_FLAG + " INTEGER DEFAULT 0, " +
                    RegistrationSellerDetails.COLUMN_PASSWORD + " TEXT, " +
                    RegistrationSellerDetails.COLUMN_SHOP_NAME + " TEXT, " +
                    RegistrationSellerDetails.COLUMN_SHOP_ADDRESS + " TEXT, " +
                    RegistrationSellerDetails.COLUMN_SHOP_CITY + " INTEGER, " +
                    RegistrationSellerDetails.COLUMN_SHOP_AREAS + " INTEGER, " +
                    RegistrationSellerDetails.COLUMN_SHOP_TYPE + " INTEGER, " +
                    RegistrationSellerDetails.COLUMN_SHOP_OPEN_TIME + " INTEGER DEFAULT 540, " +
                    RegistrationSellerDetails.COLUMN_SHOP_CLOSE_TIME + " INTEGER DEFAULT 1260, " +
                    RegistrationSellerDetails.COLUMN_DELIVERY_TIME + " INTEGER DEFAULT 90, " +
                    RegistrationSellerDetails.COLUMN_MIN_DELIVERY_AMT + " INTEGER DEFAULT 200, " +
                    RegistrationSellerDetails.COLUMN_SHOP_OPEN_BITS + " INTEGER DEFAULT 0, " +
                    RegistrationSellerDetails.COLUMN_INVENTORY_BY_US + " INTEGER DEFAULT 0, " +
                    RegistrationSellerDetails.COLUMN_LOGISTICS_SERVICES + " INTEGER DEFAULT 0, " +
                    RegistrationSellerDetails.COLUMN_SODEXO_COUPONS + " INTEGER DEFAULT 0, " +
                    RegistrationSellerDetails.COLUMN_MIDNIGHT_DELIVERY + " INTEGER DEFAULT 0, " +
                    RegistrationSellerDetails.COLUMN_REFERRAL + " TEXT " +
                    " );");
        }

        if (7 > oldVersion && 7 <= newVersion) {
            db.execSQL("ALTER TABLE " + RegistrationSellerDetails.TABLE_NAME + " ADD COLUMN " + RegistrationSellerDetails.COLUMN_VERIFICATION_SOURCE + " TEXT ;");
        }
    }
}
