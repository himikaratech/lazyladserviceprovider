package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

/**
 * Created by Sakshi Gupta on 23-06-2015.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DbHelperForDelivered {

    public static final String COLUMN_NAME = "code";
    public static final String COLUMN_NAME1 = "address";
    public static final String COLUMN_NAME2 = "time";
    public static final String COLUMN_NAME3 = "amount";
    public static final String COLUMN_NAME4 = "name";
    public static final String COLUMN_NAME5 = "number";
    public static final String KEY_SEARCH = "searchData";
    private static final String TAG = "App";
    private DatabaseHelper myDbHelper;
    private SQLiteDatabase myDb;

    private static final String DATABASE_NAME = "Data";
    private static final String FTS_VIRTUAL_TABLE = "Info";
    private static final int DATABASE_VERSION = 7;

    //Create a Virtual Table for fast searches
    private static final String DATABASE_CREATE =
            "CREATE VIRTUAL TABLE " + FTS_VIRTUAL_TABLE + " USING fts3("
                    + COLUMN_NAME + ","
                    + COLUMN_NAME1 + ","
                    + COLUMN_NAME2 + ","
                    + COLUMN_NAME3 + ","
                    + COLUMN_NAME4 + ","
                    + COLUMN_NAME5 + ","
                    + KEY_SEARCH + ");";

    private final Context context;

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }


        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.w(TAG, DATABASE_CREATE);
            db.execSQL(DATABASE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS " + FTS_VIRTUAL_TABLE);
            onCreate(db);
        }
    }

    public DbHelperForDelivered(Context ctx) {
        this.context = ctx;
    }

    public DbHelperForDelivered open() throws SQLException {
        myDbHelper = new DatabaseHelper(context);
        myDb = myDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        if (myDbHelper != null) {
            myDbHelper.close();
        }
    }


    public long insertRows(String code, String address, String time, Double amount, String name, String number) {

        ContentValues initialValues = new ContentValues();

        String searchValue = code + " " +
                name + " " +
                number;
        initialValues.put(COLUMN_NAME, code);
        initialValues.put(COLUMN_NAME1, address);
        initialValues.put(COLUMN_NAME2, time);
        initialValues.put(COLUMN_NAME3, amount);
        initialValues.put(COLUMN_NAME4, name);
        initialValues.put(COLUMN_NAME5, number);
        initialValues.put(KEY_SEARCH, searchValue);

        return myDb.insert(FTS_VIRTUAL_TABLE, null, initialValues);
    }

    public Cursor searchByInputText(String inputText) throws SQLException {

        String query = "SELECT code, address, time, amount, name, number" +
                " from " + FTS_VIRTUAL_TABLE +
                " where " + KEY_SEARCH + " MATCH '" + inputText + "';";

        Cursor mCursor = myDb.rawQuery(query, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public boolean deleteAllNames() {
        int doneDelete = myDb.delete(FTS_VIRTUAL_TABLE, null, null);
        return doneDelete > 0;
    }
}
