package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;


import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.UserDetails;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiRequestModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiResponseModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiSuggestionService;

/**
 * Created by sakshigupta on 25/08/15.
 */
public class DeliveredOrderDetails extends ActionBarActivity {

    private String m_servProvId;
    private String m_deliveredOrderCode;

    private ArrayList<DeliveredOrderDetailsSnippet> m_deliveredOrderDetailsList;
    private ListView m_deliveredOrderListView;
    private DeliveredOrderDetailsAdapter m_deliveredOrderDetailsAdapter;

    private static String[] dummy = new String[]{"a", "b"};
    private static Uri selectUri;

    Toolbar toolbar;

    static {
        DeliveredOrderDetails.selectUri = Uri.parse((String) ("content://technologies.angular.lazyladserviceprovider.lazyladserviceprovider.provider/SELECT"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.delivered_order_details);

        if (!Utils.isNetworkAvailable(this)) {
            Toast.makeText(this, "Please Check Your Network Connectivity", Toast.LENGTH_SHORT).show();
        }

        m_deliveredOrderListView = (ListView) findViewById(R.id.delivered_order_details_listview);

        Intent intent = getIntent();
        String data[] = intent.getStringArrayExtra("data_send");
        m_deliveredOrderCode = data[0];

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Delivered Order:" + " " + m_deliveredOrderCode);

        Cursor m_cursor = getContentResolver().query(
                UserDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    m_servProvId = m_cursor.getString(1);
                    m_cursor.moveToNext();
                }
            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }

        LoadDeliveredOrderDetails();
        setTracker();
    }

    private void LoadDeliveredOrderDetails() {

        final ProgressDialog pDialog;

        pDialog = new ProgressDialog(DeliveredOrderDetails.this);
        pDialog.setMessage("Loading Delivered Order Details. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Utils.RestApiPathUrl).build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
        ApiRequestModel requestModel = new ApiRequestModel();
        ApiRequestModel.InputForDeliveredOrders delivered = requestModel.new InputForDeliveredOrders(m_servProvId, m_deliveredOrderCode);
        apiSuggestionsService.deliveredOrdersDetails(delivered, new Callback<ApiResponseModel.DeliveredOrderDetailsModel>() {

            @Override
            public void success(ApiResponseModel.DeliveredOrderDetailsModel responseDeliveredOrders, Response response) {
                int success = responseDeliveredOrders.error;
                if (success == 1) {
                    if (null != responseDeliveredOrders.deliveredOrderDetailsList && !responseDeliveredOrders.deliveredOrderDetailsList.isEmpty()) {

                        m_deliveredOrderDetailsList = responseDeliveredOrders.deliveredOrderDetailsList;
                        m_deliveredOrderDetailsAdapter = new DeliveredOrderDetailsAdapter(DeliveredOrderDetails.this, m_deliveredOrderDetailsList);
                        m_deliveredOrderListView.setAdapter((ListAdapter) m_deliveredOrderDetailsAdapter);
                    }
                } else {

                }
                pDialog.dismiss();
            }

            @Override
            public void failure(final RetrofitError error) {
                pDialog.dismiss();
            }
        });
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(this.getClass().getCanonicalName());

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }
}
