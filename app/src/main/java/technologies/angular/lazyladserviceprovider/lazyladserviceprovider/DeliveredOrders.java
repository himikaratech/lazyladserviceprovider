package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.support.v7.widget.SearchView.OnQueryTextListener;

import java.util.ArrayList;
import java.text.SimpleDateFormat;

import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;

import java.util.Calendar;

import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.UserDetails;

import android.app.DatePickerDialog;
import android.widget.DatePicker;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.SearchView;
import android.app.SearchManager;
import android.content.Context;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiRequestModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiResponseModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiSuggestionService;

public class DeliveredOrders extends ActionBarActivity {

    private static final String TAG = "DeliveredOrders";

    private String m_servProvId;
    private ArrayList<DeliveredOrdersSnippet> m_deliveredOrdersList;
    private DeliveredOrdersAdapter m_deliveredOrdersAdapter;
    private ListView m_deliveredOrdersDetails;
    private String[] time;
    private SpinnerWithOnClick spinner;
    private Toolbar toolbar;

    String selectedDay;
    String selectedDate;
    private MenuItem searchMenuItem;

    private String order_code = null, address = null, timeOrderPlaced = null, userName = null, userPhoneNum = null;
    private Double amount = 0.0d;
    private DbHelperForDelivered mDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.delivered_orders);

        mDbHelper = new DbHelperForDelivered(this);
        mDbHelper.open();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        time = getResources().getStringArray(R.array.date);
        spinner = (SpinnerWithOnClick) findViewById(R.id.date_spinner);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, time);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinner.setAdapter(dataAdapter);

        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                startImplementation(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        spinner.setSelection(0);
        setTracker();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mDbHelper != null) {
            mDbHelper.close();
        }
    }

    @Override
    public void onBackPressed() {
        m_deliveredOrdersDetails.setAdapter(m_deliveredOrdersAdapter);
        super.onBackPressed();
    }

    private void startImplementation(final int position) {
        if (Utils.isNetworkAvailable(this)) {
            m_deliveredOrdersDetails = (ListView) findViewById(R.id.delivered_orders_list_view);

            mDbHelper.deleteAllNames();
            m_deliveredOrdersList = null;

            Cursor m_cursor = getContentResolver().query(
                    UserDetails.CONTENT_URI,
                    null,
                    null,
                    null,
                    null);

            if (m_cursor != null && m_cursor.moveToFirst()) {
                if (m_cursor.getCount() != 0) {
                    for (int i = 0; i < m_cursor.getCount(); i++) {
                        m_servProvId = m_cursor.getString(1);
                        m_cursor.moveToNext();
                    }
                } else {
                    //TODO Some UI Element asking to add address
                }
            } else {
                //TODO Error Handling
            }
            if (position == 0 || position == 1) {
                selectedDate = "";
                selectedDay = String.valueOf(position);
                LoadDeliveredOrders(selectedDate, selectedDay);
            } else if (position == 2) {

                final Calendar myCalendar = Calendar.getInstance();
                final DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String myFormat = "yyyy/MM/dd"; // In which you need put here
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
                        selectedDate = sdf.format(myCalendar.getTime());
                        selectedDay = String.valueOf(-1);
                        LoadDeliveredOrders(selectedDate, selectedDay);
                    }

                };
                new DatePickerDialog(DeliveredOrders.this, dateSetListener, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        } else {
            setContentView(R.layout.network_problem);
            Button retryButton = (Button) findViewById(R.id.retry_button);

            retryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startImplementation(position);
                }
            });
        }
    }

    public MenuItem getSearchMenuItem() {
        return searchMenuItem;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_delivered_orders, menu);

        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchMenuItem = menu.findItem(R.id.search);
        SearchView searchView =
                (SearchView) searchMenuItem.getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                MenuItem searchMenuItem = getSearchMenuItem();
                if (searchMenuItem != null) {
                    SearchView sv = (SearchView) searchMenuItem.getActionView();
                    sv.onActionViewCollapsed();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    private void LoadDeliveredOrders(String date, String day) {

        selectedDate = date;
        selectedDay = day;

        final ProgressDialog pDialog;
        pDialog = new ProgressDialog(DeliveredOrders.this);
        pDialog.setMessage("Loading Delivered Orders. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Utils.RestApiPathUrl).build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
        ApiRequestModel requestModel = new ApiRequestModel();

        ApiRequestModel.Delivered delivered = requestModel.new Delivered(m_servProvId, selectedDate, selectedDay);
        apiSuggestionsService.deliveredOrders(delivered, new Callback<ApiResponseModel.DeliveredOrdersModel>() {

            @Override
            public void success(ApiResponseModel.DeliveredOrdersModel responseDeliveredOrders, Response response) {
                int success = responseDeliveredOrders.error;
                if (success == 1) {
                    for (int i = 0; i < responseDeliveredOrders.deliveredOrdersList.size(); i++) {
                        order_code = responseDeliveredOrders.deliveredOrdersList.get(i).orderCode;
                        address = responseDeliveredOrders.deliveredOrdersList.get(i).address;
                        timeOrderPlaced = responseDeliveredOrders.deliveredOrdersList.get(i).timeOrderPlaced;
                        amount = responseDeliveredOrders.deliveredOrdersList.get(i).amount;
                        userName = responseDeliveredOrders.deliveredOrdersList.get(i).userName;
                        userPhoneNum = responseDeliveredOrders.deliveredOrdersList.get(i).userName;
                        mDbHelper.open();
                        mDbHelper.insertRows(order_code, address, timeOrderPlaced, amount, userName, userPhoneNum);
                    }
                    m_deliveredOrdersList = responseDeliveredOrders.deliveredOrdersList;
                    m_deliveredOrdersAdapter = new DeliveredOrdersAdapter(DeliveredOrders.this, m_deliveredOrdersList);
                    m_deliveredOrdersDetails.setAdapter((ListAdapter) m_deliveredOrdersAdapter);
                } else {
                    //do nothing
                }
                pDialog.dismiss();
            }

            @Override
            public void failure(final RetrofitError error) {
                Log.i(TAG + "Failure", error.toString());
                pDialog.dismiss();
            }
        });
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(this.getClass().getCanonicalName());
        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }
}
