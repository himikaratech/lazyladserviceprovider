package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Saurabh on 30/01/15.
 */
public class DeliveredOrdersSnippet {

    public int deliveredOdersId;
    @SerializedName("delivered_order_code")
    public String orderCode;
    @SerializedName("delivery_address")
    public String address;
    @SerializedName("order_time")
    public String timeOrderPlaced;
    @SerializedName("total_amount")
    public double amount;
    @SerializedName("user_name")
    public String userName;
    @SerializedName("user_phone_number")
    public String userPhone;

    public DeliveredOrdersSnippet() {
    }

    public DeliveredOrdersSnippet(int id, String order_code, String customer_address, String time_order_placed, double total_amount, String user_name, String user_phone_num) {
        deliveredOdersId = id;
        orderCode = order_code;
        address = customer_address;
        timeOrderPlaced = time_order_placed;
        amount = total_amount;
        userName = user_name;
        userPhone = user_phone_num;
    }
}
