package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Picasso;

import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.RegistrationSellerDetails;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.RegistrationSellerAreas;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.lang.String;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiRequestModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiResponseModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiSuggestionService;

public class DeliveryAreas extends ActionBarActivity {

    private static final String TAG = "DeliveryAreas";

    private ListView m_deliveryAreas;
    ImageView cityImageView;
    TextView deliveryAreas;
    TextView cityName;
    private SessionManager m_sessionManager;

    private String phone = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        m_sessionManager = new SessionManager(this);
        startImplementation();
        setTracker();
    }

    private void startImplementation() {
        if (Utils.isNetworkAvailable(this)) {
            setContentView(R.layout.activity_delivery_areas);

            m_deliveryAreas = (ListView) findViewById(R.id.delivery_areas);
            cityImageView = (ImageView) findViewById(R.id.imageForCity);
            deliveryAreas = (TextView) findViewById(R.id.text_delivery_areas);
            cityName = (TextView) findViewById(R.id.city_name_textview);
            cityImageView.setBackgroundResource(R.drawable.cityimage);
            cityName.setText("Gurgaon");

            Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/Montserrat_Regular.ttf");
            deliveryAreas.setTypeface(typeFace);

            loadServiceAreas();
            (findViewById(R.id.confirm_delivery_areas_button)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DeliveryAreasAdapter adapter = (DeliveryAreasAdapter) m_deliveryAreas.getAdapter();
                    Boolean atleastOneArea = adapter.cblist.contains(true);

                    if (atleastOneArea) {
                        saveServiceAreas();
                        submitRegistrationDetails();
                    } else
                        Toast.makeText(getApplicationContext(), "Please select your Delivery Areas.", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            setContentView(R.layout.network_problem);
            Button retryButton = (Button) findViewById(R.id.retry_button);

            retryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startImplementation();
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_delievery_areas, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void loadServiceAreas() {

        String[]projection={RegistrationSellerDetails.COLUMN_SHOP_CITY, RegistrationSellerDetails.COLUMN_SHOP_AREAS};

        Cursor cursor = getContentResolver().query(
                RegistrationSellerDetails.CONTENT_URI,
                projection,
                null,
                null,
                null);

        int shopCityCode = 0;
        int shopAreaCode = 0;
        if (cursor != null && cursor.moveToFirst()) {
            if (cursor.getCount() != 0) {
                shopCityCode = cursor.getInt(0);
                shopAreaCode = cursor.getInt(1);

            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }

        String cityStr = Integer.toString(shopCityCode);
        final String areaStr = Integer.toString(shopAreaCode);

        final ProgressDialog pDialog;
        pDialog = new ProgressDialog(DeliveryAreas.this);
        pDialog.setMessage("Loading. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

       RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Utils.RestApiPathUrl).build();
        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);

        apiSuggestionsService.areasForSeller(cityStr, areaStr, new Callback<AreaModel>() {
            @Override
            public void success(AreaModel areaModel, retrofit.client.Response response) {
                ArrayList<AreaSnippet> m_areasList = new ArrayList<AreaSnippet>();
                m_areasList = areaModel.areas;
                String imgAddress = areaModel.m_imgAddress;
                String cityNameText = areaModel.cityName;
                AreaSnippet areaitem = null;

                if (imgAddress == null || imgAddress == "") imgAddress = "\"\"";

                Picasso.with(DeliveryAreas.this)
                        .load(imgAddress)
                        .placeholder(R.drawable.cityimage)
                        .fit().centerCrop()
                        .error(R.drawable.cityimage)
                        .into(cityImageView);

                cityName.setText(cityNameText);

                ContentValues[] cvArray = new ContentValues[m_areasList.size()];

                int i = 0;
                for (; i < m_areasList.size(); i++) {

                    areaitem = m_areasList.get(i);

                    ContentValues values=new ContentValues();
                    values.put(RegistrationSellerAreas.COLUMN_AREA_CODE,Integer.parseInt(areaitem.m_areaCode));
                    values.put(RegistrationSellerAreas.COLUMN_AREA_NAME,StringUtils.replace(areaitem.m_areaName, "'", "''"));
                    cvArray[i] = values ;
                }

                if (i != 0)

                        getContentResolver().bulkInsert(RegistrationSellerAreas.CONTENT_URI, cvArray);


                DeliveryAreasAdapter m_servProvAdapter = new DeliveryAreasAdapter(DeliveryAreas.this, m_areasList);
                m_deliveryAreas.setAdapter((ListAdapter) m_servProvAdapter);
                pDialog.dismiss();
            }

            @Override
            public void failure(final RetrofitError error) {
                Log.e(TAG + "Failure", error +" retro");
                pDialog.dismiss();
                loadServiceAreas();
            }
        });
    }

    private void saveServiceAreas() {

        Boolean deliverable;

        DeliveryAreasAdapter adapter = (DeliveryAreasAdapter) m_deliveryAreas.getAdapter();
        int totalAreas = adapter.cblist.size();

        final StringBuilder builder = new StringBuilder();
        String[] selectionArgs = new String[0];

        ArrayList<String> areaCodeArgs = new ArrayList<String>();

        for (int i = 0, j = 1; i < totalAreas; i++) {
            deliverable = adapter.cblist.get(i);
            if (deliverable) {
               if (j == 0) {
                    builder.append(",");
                } else j = 0;

                   builder.append("?");

                areaCodeArgs.add(((AreaSnippet) (adapter.getItem(i))).m_areaCode);
                selectionArgs = areaCodeArgs.toArray(selectionArgs);
            }
        }
        String selectionClause = RegistrationSellerAreas.COLUMN_AREA_CODE + " IN ("+builder.toString()+")";

        ContentValues values=new ContentValues();
        values.put(RegistrationSellerAreas.COLUMN_DELIVERABLE, 1);

        getContentResolver().update(RegistrationSellerAreas.CONTENT_URI, values,
                selectionClause, selectionArgs);

    }

    private void submitRegistrationDetails() {
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        String name = "", validation = "", password = "", shopName = "", address = "", city = "", area = "", type = "", open = "", close = "", time = "", amount = "", bits = "", inventory = "", code = "", logistics = "", sodexo = "", midnight = "", referral = "" , verificationSource = "";

        Cursor cursor1 = getContentResolver().query(RegistrationSellerDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (cursor1 != null && cursor1.moveToFirst()) {
            if (cursor1.getCount() != 0) {
                name = cursor1.getString(1);
                phone = cursor1.getString(2);
                validation = Integer.toString(cursor1.getInt(3));
                password = cursor1.getString(4);
                shopName = cursor1.getString(5);
                address = cursor1.getString(6);
                city = Integer.toString(cursor1.getInt(7));
                area = Integer.toString(cursor1.getInt(8));
                type = Integer.toString(cursor1.getInt(9));
                open = Integer.toString(cursor1.getInt(10));
                close = Integer.toString(cursor1.getInt(11));
                time = Integer.toString(cursor1.getInt(12));
                amount = Integer.toString(cursor1.getInt(13));
                bits = Integer.toString(cursor1.getInt(14));
                inventory = Integer.toString(cursor1.getInt(15));
                logistics = Integer.toString(cursor1.getInt(16));
                sodexo = Integer.toString(cursor1.getInt(17));
                midnight = Integer.toString(cursor1.getInt(18));
                referral = cursor1.getString(19);
                verificationSource = cursor1.getString(20);
            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }
        if (cursor1 != null) cursor1.close();

        String[]projection={RegistrationSellerAreas.COLUMN_AREA_CODE};
        String selection= RegistrationSellerAreas.COLUMN_DELIVERABLE + " = ? ";
        String[] selectionArgs = {"1"};

        Cursor cursor2 = getContentResolver().query(RegistrationSellerAreas.CONTENT_URI,
                projection,
                selection,
                selectionArgs,
                null);

        JSONArray jsonObject_deliveryAreas = new JSONArray();
        if (cursor2 != null && cursor2.moveToFirst()) {
            if (cursor2.getCount() != 0) {
                for (int i = 0; i < cursor2.getCount(); i++) {
                    JSONObject smallJsonObject = new JSONObject();
                    try {
                        smallJsonObject.put("area_code", cursor2.getInt(0));
                        jsonObject_deliveryAreas.put(smallJsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cursor2.moveToNext();
                }

            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }
        if (cursor2 != null) cursor2.close();

        code = jsonObject_deliveryAreas.toString();
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Utils.RestApiPathUrl).build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
        ApiRequestModel requestModel = new ApiRequestModel();

        ApiRequestModel.Register register = requestModel.new Register(name, phone, validation, password, shopName, address, city, area, type, open, close, time, amount, bits, inventory, code, logistics, sodexo, midnight, referral, verificationSource);
        apiSuggestionsService.finalRegister(register, new Callback<ApiResponseModel.ResponseRegister>() {

            @Override
            public void success(ApiResponseModel.ResponseRegister responseData, retrofit.client.Response response) {
                Integer register_code = 0;
                int success = responseData.error;
                if (success == 1) {
                    register_code = responseData.code;
                } else {
                    Log.i("SQL_Message", responseData.sql_error);
                }
                pDialog.dismiss();
                Intent intent = new Intent(DeliveryAreas.this, RegisterConfirm.class);
                intent.putExtra("register_code", register_code);
                intent.putExtra("phone", phone);
                startActivity(intent);
            }

            @Override
            public void failure(final RetrofitError error) {
                Log.e(TAG + "Failure", error +" retro");
                pDialog.dismiss();
            }
        });
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(this.getClass().getCanonicalName());

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }
}
