package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by AngularTechnologies on 11/06/15.
 */
public class DeliveryAreasAdapter extends BaseAdapter {
    private Activity m_activity;
    private ArrayList<AreaSnippet> m_areasList;
    public ArrayList<Boolean> cblist;

    public DeliveryAreasAdapter(Activity activity, ArrayList<AreaSnippet> areasList) {
        m_activity = activity;
        m_areasList = areasList;
        cblist = new ArrayList<>(Collections.nCopies(m_areasList.size(), false));
    }

    private class ViewHolder {
        TextView areaNameView;
        CheckBox deliverCB;
        View separateLine;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (m_areasList != null) {
            count = m_areasList.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (m_areasList != null) {
            return m_areasList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_areasList != null) {
            return m_areasList.get(position).m_id;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (m_areasList != null) {
            final ViewHolder vh;
            if (null == convertView) {
                vh = new ViewHolder();
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.delivery_area_details_list, parent, false);

                vh.areaNameView = (TextView) (convertView.findViewById(R.id.area_name));
                vh.deliverCB = (CheckBox) (convertView.findViewById(R.id.area_checkbox));
                vh.separateLine = (View) (convertView.findViewById(R.id.view_for_delivery_areas));
                convertView.setTag(vh);
            } else
                vh = (ViewHolder) convertView.getTag();

            if (vh.deliverCB.isChecked() == true) {
                convertView.setBackgroundColor(Color.parseColor("#78bd1e"));
                vh.areaNameView.setTextColor(Color.parseColor("#ffffff"));
                vh.separateLine.setBackgroundColor(Color.parseColor("#ffffff"));
            } else {
                convertView.setBackgroundColor(Color.parseColor("#ffffff"));
                vh.areaNameView.setTextColor(Color.parseColor("#333333"));
                vh.separateLine.setBackgroundColor(Color.parseColor("#ADAEAF"));
            }

            Typeface typeFace = Typeface.createFromAsset(m_activity.getAssets(), "fonts/Roboto_Light.ttf");
            vh.areaNameView.setTypeface(typeFace);

            vh.areaNameView.setText(m_areasList.get(position).m_areaName);
            vh.deliverCB.setTag(position);
            final View convertViewFinal = convertView;
            vh.deliverCB.setChecked(cblist.get(position));
            vh.deliverCB.setOnCheckedChangeListener(
                    new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView,
                                                     boolean isChecked) {
                            // Get the position that set for the checkbox using setTag.
                            int getPosition = (Integer) buttonView.getTag();
                            // Set the value of checkbox to maintain its state.
                            cblist.set(getPosition, buttonView.isChecked());

                            if (vh.deliverCB.isChecked() == true) {
                                convertViewFinal.setBackgroundColor(Color.parseColor("#78bd1e"));
                                vh.areaNameView.setTextColor(Color.parseColor("#ffffff"));
                                vh.separateLine.setBackgroundColor(Color.parseColor("#ffffff"));
                            } else {
                                convertViewFinal.setBackgroundColor(Color.parseColor("#ffffff"));
                                vh.areaNameView.setTextColor(Color.parseColor("#333333"));
                                vh.separateLine.setBackgroundColor(Color.parseColor("#ADAEAF"));
                            }
                        }
                    });

            return convertView;
        }
        return null;
    }
}
