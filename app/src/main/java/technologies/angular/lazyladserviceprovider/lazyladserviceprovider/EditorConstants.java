package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

/**
 * Created by Amud on 09/11/15.
 */
public class EditorConstants {

    public static String CITY__NAME_SELECTED_CONSTANT = "CITY_NAME_SELECTED";
    public static String AREA__NAME_SELECTED_CONSTANT = "AREA_NAME_SELECTED";

    public static  String LATITUDE_SELECTED_CONSTANT = "LATITUDE_SELECTED" ;
    public static  String LONGITUDE_SELECTED_CONSTANT = "LONGITUDE_SELECTED" ;
    public static String DEFAULT_CITY_SELECTED_CONSTANT= "City";

    public static String CUSTOMER_CARE_CONSTANT= "SELLER_CUSTOMER_CARE";



    public static String DEFAULT_CUSTOMER_CARE_CONSTANT= "8011139511";


}
