package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Saurabh on 30/01/15.
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context m_context;
    private List<String> m_listDataHeader;
    private HashMap<String, List<String>> m_listDataChild;
    private int m_IconList[];

    public ExpandableListAdapter(Context context, List<String> listDataHeader,
                                 HashMap<String, List<String>> listChildData, int[] iconList) {
        this.m_context = context;
        this.m_listDataHeader = listDataHeader;
        this.m_listDataChild = listChildData;
        this.m_IconList = iconList;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this.m_listDataChild.get(this.m_listDataHeader.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (this.m_listDataChild.get(this.m_listDataHeader.get(groupPosition)) != null) {
            return this.m_listDataChild.get(this.m_listDataHeader.get(groupPosition)).size();
        } else
            return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.m_listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.m_listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        String headerTitle = (String) getGroup(groupPosition);
        int colorFlag = m_IconList[groupPosition];
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.m_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.drawer_items, (ViewGroup) null);
        }

        TextView lblListHeader = (TextView) convertView.findViewById(R.id.section_text_display);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    /**
     * We require the group positions in which child is selectable and need to change this function accordingly
     */
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }


}
