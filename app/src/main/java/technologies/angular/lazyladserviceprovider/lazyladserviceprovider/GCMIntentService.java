package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;

import static technologies.angular.lazyladserviceprovider.lazyladserviceprovider.CommonUtilities.displayMessage;

public class GCMIntentService extends GCMBaseIntentService {

    private static final String TAG = "GCMIntentService";
    private static String SENDER_ID = "495574458613";

    private static final int MESSAGE_TYPE_PRICE = 1;

    public GCMIntentService() {
        super(SENDER_ID);
    }

    /**
     * Method called on Receiving a new message
     */
    @Override
    protected void onMessage(Context context, Intent intent) {
        Log.i(TAG, "Received message");
        String message = intent.getExtras().getString("price");
        int messageType = MESSAGE_TYPE_PRICE;
        if (message != null && !message.isEmpty()) {
            messageType = MESSAGE_TYPE_PRICE;
        }
        displayMessage(context, message);
        // notifies user
        generateNotification(context, message, messageType);
    }


    /**
     * Method called on Error
     */
    @Override
    public void onError(Context context, String errorId) {
        Log.i(TAG, "Received error: " + errorId);
    }

    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        // log message
        Log.i(TAG, "Received recoverable error: " + errorId);
        return super.onRecoverableError(context, errorId);
    }

    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    private static void generateNotification(Context context, String message, int messageType) {
        Intent notificationIntent = null;
        if (messageType == MESSAGE_TYPE_PRICE) {
            notificationIntent = new Intent(context, CurrentOrders.class);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_SINGLE_TOP);
        }
        int icon = R.drawable.ic_launcher;
        long when = System.currentTimeMillis();
        PendingIntent pendingIntent =
                PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT
                        | PendingIntent.FLAG_ONE_SHOT);
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setContentText(message)
                .setContentTitle("LazySP")
                .setContentIntent(pendingIntent).setWhen(when).setSmallIcon(icon).setDefaults(Notification.DEFAULT_ALL);

        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle("LazyLad Service Provider");
        bigTextStyle.bigText(message);
        builder.setStyle(bigTextStyle);

        //notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "your_sound_file_name.mp3");

        notificationManager.notify(0, builder.build());
    }

    @Override
    protected void onRegistered(Context context, String arg1) {
        Log.i(TAG, "Device registered: regId = " + arg1);
        //displayMessage(context, "Your device registred with GCM");
    }

    @Override
    protected void onUnregistered(Context context, String arg1) {
        Log.i(TAG, "Device unregistered");
        //displayMessage(context, arg1);
    }

}
