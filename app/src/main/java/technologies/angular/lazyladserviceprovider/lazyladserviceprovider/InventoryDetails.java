package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class InventoryDetails extends ActionBarActivity {

    private ListView m_inventoryDetails;
    private SessionManager m_sessionManager;

    private static Cursor m_cursor;
    private static ContentResolver m_conRes = null;
    private static String[] dummy = new String[]{"a", "b"};
    private static Uri selectUri;
    private static Uri insertUri;
    private static Uri updateUri;
    private static Uri deleteUri;

    static {
        selectUri = Uri.parse((String) ("content://technologies.angular.lazyladserviceprovider.lazyladserviceprovider.provider/SELECT"));
        insertUri = Uri.parse((String) ("content://technologies.angular.lazyladserviceprovider.lazyladserviceprovider.provider/INSERT"));
        updateUri = Uri.parse((String) ("content://technologies.angular.lazyladserviceprovider.lazyladserviceprovider.provider/UPDATE"));
        deleteUri = Uri.parse((String) ("content://technologies.angular.lazyladserviceprovider.lazyladserviceprovider.provider/DELETE"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        m_conRes = getApplicationContext().getContentResolver();
        m_sessionManager = new SessionManager(this);
        startImplementation();
    }

    private void startImplementation() {
        if (Utils.isNetworkAvailable(this)) {
            setContentView(R.layout.activity_inventory_details);

            (findViewById(R.id.confirm_items_button)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    submitRegistrationDetails();
                }
            });
            m_inventoryDetails = (ListView) findViewById(R.id.inventory_details_list_view);
            loadInventory();
        } else {
            setContentView(R.layout.network_problem);
            Button retryButton = (Button) findViewById(R.id.retry_button);
            retryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startImplementation();
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_inventory_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadInventory() {

        m_conRes.query(deleteUri, dummy, "delete from registration_inventory_details", dummy, "");

        m_cursor = m_conRes.query(selectUri, dummy, "select shop_type from registration_seller_details", dummy, "");

        int shopTypeCode = 0;
        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                shopTypeCode = m_cursor.getInt(0);
            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }
        if (m_cursor != null) m_cursor.close();

        String url_items_serviceProvider = "http://www.angulartechnologies.com/task_manager/v1/getBasicInventoryforShopType";
        String param = Integer.toString(shopTypeCode);
        url_items_serviceProvider = url_items_serviceProvider + "/" + param;

        GsonRequest<InventoryModel> jsObjRequest = new GsonRequest<InventoryModel>(Request.Method.GET,
                url_items_serviceProvider, InventoryModel.class, null, new Response.Listener<InventoryModel>() {
            @Override
            public void onResponse(InventoryModel response) {
                ArrayList<InventorySnippet> itemsList = new ArrayList<InventorySnippet>();
                itemsList = response.items;

                final String baseQuery = "insert into registration_inventory_details(item_code,item_name, item_cost, item_desc," +
                        " item_img_flag, item_img_add) values ";
                StringBuilder builder = new StringBuilder(baseQuery);
                int i = 0;
                for (; i < itemsList.size(); i++) {
                    if (i % 500 != 0) {
                        builder.append(",");
                    }

                    InventorySnippet inv_item = itemsList.get(i);

                    builder.append(String.format("(%s, '%s',%s,'%s',%s,'%s')", inv_item.m_itemCode,
                            StringUtils.replace(inv_item.m_itemName, "'", "''"), inv_item.m_itemCost,
                            StringUtils.replace(inv_item.m_itemDesc, "'", "''"), inv_item.m_itemImgFlag,
                            StringUtils.replace(inv_item.m_itemImgAddress, "'", "''")));

                    if (((i + 1) % 500) == 0) {
                        Log.i("Insert Query", builder.toString());
                        m_conRes.query(insertUri, dummy, builder.toString(), dummy, "");
                        builder = new StringBuilder(baseQuery);
                    }
                }

                if ((i + 1) % 500 != 0) {
                    Log.i("Insert Query", builder.toString());
                    m_conRes.query(insertUri, dummy, builder.toString(), dummy, "");
                }

                InventoryDetailsAdapter m_invdetailsAdapter = new InventoryDetailsAdapter(InventoryDetails.this, itemsList);
                m_inventoryDetails.setAdapter(m_invdetailsAdapter);

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error even: ", error+" retro");
                loadInventory();
            }
        });
        m_sessionManager.addToRequestQueue(jsObjRequest);

    }

    private void submitRegistrationDetails() {

        HashMap<String, String> arrayList_RegistrationDetails = new HashMap<String, String>();

        String url_post_registration_details = "http://www.angulartechnologies.com/task_manager/v1/postCompleteRegistrationDetails";

        m_cursor = m_conRes.query(selectUri, dummy, "select * from registration_seller_details", dummy, "");

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                arrayList_RegistrationDetails.put("seller_name", m_cursor.getString(1));
                arrayList_RegistrationDetails.put("seller_phone", m_cursor.getString(2));
                arrayList_RegistrationDetails.put("password", m_cursor.getString(3));
                arrayList_RegistrationDetails.put("shop_name", m_cursor.getString(4));
                arrayList_RegistrationDetails.put("shop_address", m_cursor.getString(5));
                arrayList_RegistrationDetails.put("shop_city", Integer.toString(m_cursor.getInt(6)));
                arrayList_RegistrationDetails.put("shop_area", Integer.toString(m_cursor.getInt(7)));
                arrayList_RegistrationDetails.put("shop_type", Integer.toString(m_cursor.getInt(8)));
                arrayList_RegistrationDetails.put("shop_open_time", Integer.toString(m_cursor.getInt(9)));
                arrayList_RegistrationDetails.put("shop_close_time", Integer.toString(m_cursor.getInt(10)));
                arrayList_RegistrationDetails.put("delivery_time", Integer.toString(m_cursor.getInt(11)));
                arrayList_RegistrationDetails.put("min_delivery_amt", Integer.toString(m_cursor.getInt(12)));

            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }
        if (m_cursor != null) m_cursor.close();

        m_cursor = m_conRes.query(selectUri, dummy, "select area_code from registration_service_areas where delieverable = 1", dummy, "");
        JSONArray jsonObject_deliveryAreas = new JSONArray();
        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    JSONObject smallJsonObject = new JSONObject();
                    try {
                        smallJsonObject.put("area_code", m_cursor.getInt(0));
                        jsonObject_deliveryAreas.put(smallJsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    m_cursor.moveToNext();
                }

            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }
        if (m_cursor != null) m_cursor.close();

        m_cursor = m_conRes.query(selectUri, dummy, "select item_code from registration_inventory_details where available = 1", dummy, "");
        JSONArray jsonObject_inventoryDetails = new JSONArray();
        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    JSONObject smallJsonObject = new JSONObject();
                    try {
                        smallJsonObject.put("item_code", m_cursor.getInt(0));
                        jsonObject_inventoryDetails.put(smallJsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    m_cursor.moveToNext();
                }

            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }
        if (m_cursor != null) m_cursor.close();

        arrayList_RegistrationDetails.put("JsonServiceAreasArray", jsonObject_deliveryAreas.toString());
        arrayList_RegistrationDetails.put("JsonInventoryDetailsArray", jsonObject_inventoryDetails.toString());

        JsonObjectRequest sendOrderDetails = new JsonObjectRequest(url_post_registration_details, new JSONObject(arrayList_RegistrationDetails),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            VolleyLog.v("Response ", response.toString());
                            Integer register_code = 0;
                            int success = response.getInt("error");
                            if (success == 1) {
                                register_code = response.getInt("register_code");

                            } else {
                                Log.i("SQL_Message", response.getString("sql_error"));
                            }

                            Intent intent = new Intent(InventoryDetails.this, RegisterConfirm.class);
                            intent.putExtra("register_code", register_code);
                            //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error+" retro");
            }
        });
        SessionManager m_sessionManager;
        m_sessionManager = new SessionManager(this);
        m_sessionManager.addToRequestQueue(sendOrderDetails);

    }
}
