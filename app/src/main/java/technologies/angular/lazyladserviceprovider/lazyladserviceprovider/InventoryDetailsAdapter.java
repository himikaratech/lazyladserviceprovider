package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by AngularTechnologies on 11/06/15.
 */
public class InventoryDetailsAdapter extends BaseAdapter {
    private Activity m_activity;
    private ArrayList<InventorySnippet> m_itemsList;

    private class ViewHolder {
        TextView itemCostTextView;
        TextView itemNameTextView;
        TextView itemDescTextView;
        ImageView imageView;
    }

    public InventoryDetailsAdapter(Activity activity, ArrayList<InventorySnippet> itemsList) {
        m_activity = activity;
        m_itemsList = itemsList;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (m_itemsList != null) {
            count = m_itemsList.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (m_itemsList != null) {
            return m_itemsList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_itemsList != null) {
            return m_itemsList.get(position).m_itemId;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (m_itemsList != null) {

            String itemName = ((InventorySnippet) getItem(position)).m_itemName;
            double itemCost = ((InventorySnippet) getItem(position)).m_itemCost;
            String itemDesc = ((InventorySnippet) getItem(position)).m_itemDesc;
            String itemImageAdd = ((InventorySnippet) getItem(position)).m_itemImgAddress;
            ViewHolder vh;
            if (null == convertView) {
                vh = new ViewHolder();

                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.inventory_details_list, (ViewGroup) null);

                vh.itemCostTextView = (TextView) convertView.findViewById(R.id.available_item_mrp_textview);
                vh.itemNameTextView = (TextView) convertView.findViewById(R.id.available_item_name_textview);
                vh.itemDescTextView = (TextView) convertView.findViewById(R.id.available_item_desc_textview);
                vh.imageView = (ImageView) convertView.findViewById(R.id.available_item_image);
                convertView.setTag(vh);
            } else
                vh = (ViewHolder) convertView.getTag();

            if (itemImageAdd == null || itemImageAdd == "") itemImageAdd = "\"\"";
            Picasso.with(parent.getContext())
                    .load(itemImageAdd)
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.no_image)
                    .fit().centerInside()
                    .into(vh.imageView);

            vh.itemNameTextView.setText(itemName);
            vh.itemDescTextView.setText(itemDesc);
            vh.itemCostTextView.setText(Double.toString(itemCost));

            return convertView;
        }
        return null;
    }
}
