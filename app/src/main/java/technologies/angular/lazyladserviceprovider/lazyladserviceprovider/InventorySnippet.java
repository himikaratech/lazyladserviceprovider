package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Saurabh on 27/02/15.
 */
public class InventorySnippet {

    public int m_itemId;
    @SerializedName("item_code")
    public int m_itemCode;
    @SerializedName("item_name")
    public String m_itemName;
    @SerializedName("item_cost")
    public double m_itemCost;
    @SerializedName("item_desc")
    public String m_itemDesc;
    @SerializedName("item_img_flag")
    public int m_itemImgFlag;
    @SerializedName("item_img_add")
    public String m_itemImgAddress;

    public InventorySnippet() {

    }

    public InventorySnippet(int id, int itemCode, String itemName, double itemCost, String itemDesc, int itemImgFlag,
                            String itemImgAdd) {
        m_itemId = id;
        m_itemCode = itemCode;
        m_itemName = itemName;
        m_itemCost = itemCost;
        m_itemDesc = itemDesc;
        m_itemImgFlag = itemImgFlag;
        m_itemImgAddress = itemImgAdd;
    }
}
