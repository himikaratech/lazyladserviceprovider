package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Saurabh on 11/02/15.
 */
public class ItemSnippet implements Serializable {

    private static final long serialVersionUID = 1L;

    @SerializedName("item_id")
    public String m_itemId;
    @SerializedName("item_code")
    public String m_itemCode;
    @SerializedName("item_name")
    public String m_itemName;
    @SerializedName("item_img_flag")
    public String m_itemImgFlag;
    @SerializedName("item_img_add")
    public String m_itemImgAddress;
    @SerializedName("item_unit")
    public String m_itemUnit;
    @SerializedName("item_short_desc")
    public String m_itemShortDesc;   //Standard Weight
    @SerializedName("item_desc")
    public String m_itemDesc;
    @SerializedName("item_status")
    public int m_itemStatus;   //Available and Not Available
    @SerializedName("item_cost")
    public double m_itemCost;
    @SerializedName("item_type_code")
    public String m_itemTypeCode;
    @SerializedName("item_quantity")
    public String m_itemQuantity;

    public boolean m_priceChanged;

    public ItemSnippet() {
    }

  public ItemSnippet(String id, String itemCode, String itemName, String itemImgFlag,
                                    String itemImgAdd, String itemUnit, String itemShortDesc, String itemDesc,
                                    int itemStatus, double itemCost, String itemTypeCode, String itemQuantity){
        m_itemId=id;
        m_itemCode=itemCode;
        m_itemName=itemName;
        m_itemImgFlag=itemImgFlag;
        m_itemImgAddress=itemImgAdd;
        m_itemUnit=itemUnit;
        m_itemShortDesc=itemShortDesc;
        m_itemDesc=itemDesc;
        m_itemStatus=itemStatus;
        m_itemCost=itemCost;
        m_itemTypeCode=itemTypeCode;
        m_itemQuantity=itemQuantity;

        m_priceChanged = false;
    }
}
