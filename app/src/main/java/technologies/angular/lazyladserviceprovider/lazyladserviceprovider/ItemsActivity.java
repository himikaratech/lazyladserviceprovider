package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.SearchManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.ServiceTypeCategories;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.UserDetails;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.SearchResult;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiResponseModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiSuggestionService;

import com.astuetz.PagerSlidingTabStrip;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;


public class ItemsActivity extends ActionBarActivity {

    private static final String TAG = "ItemsActivity";
    private ViewPager m_viewPager;
    private Toolbar m_toolbar;
    private ItemsTabPagerAdapter m_itemsTabPagerAdapter;

    public static ArrayList<String> m_itemsCategoryNames;
    public static ArrayList<Integer> m_itemsCategoryCode;

    private MenuItem searchMenuItem;
    private String m_stCode;
    private String m_spCode;
    String value;
    private AsyncTask m_async5;
    // otherwise  Unknown
    // 1 for Available Items
    // 2 for Non Available Items
    // 3 for Out of Stock Items
    public Integer items_type;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_example);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            value = extras.getString("items_type");
            items_type = Integer.parseInt(value);
            Log.i(TAG, "Activity Created " + items_type);
        }

        Cursor m_cursor = getContentResolver().query(ServiceTypeCategories.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            m_itemsCategoryNames = new ArrayList<String>(m_cursor.getCount());
            m_itemsCategoryCode = new ArrayList<Integer>(m_cursor.getCount());
            for (int i = 0; i < m_cursor.getCount(); i++) {
                m_itemsCategoryNames.add(m_cursor.getString(3));
                m_itemsCategoryCode.add(Integer.parseInt(m_cursor.getString(2)));
                m_cursor.moveToNext();
            }
        }

        m_cursor = getContentResolver().query(UserDetails.CONTENT_URI,
                null,
                null,
                null,
                null);


        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    m_spCode = m_cursor.getString(1);
                    m_stCode = m_cursor.getString(4);
                    m_cursor.moveToNext();
                }
            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }
        if (m_cursor != null) m_cursor.close();

        m_viewPager = (ViewPager) findViewById(R.id.items_pager);
        m_toolbar = (Toolbar) findViewById(R.id.items_toolbar);
        m_itemsTabPagerAdapter = new ItemsTabPagerAdapter(getSupportFragmentManager(), m_itemsCategoryCode);

        m_viewPager.setAdapter(m_itemsTabPagerAdapter);

        setSupportActionBar(m_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (items_type == 1)
            getSupportActionBar().setTitle("Available Items");
        else if (items_type == 2)
            getSupportActionBar().setTitle("Non Available Items");
        else if (items_type == 3)
            getSupportActionBar().setTitle("Out of Stock Items");
        final PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);

        tabs.setViewPager(m_viewPager);
        m_viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // on changing the page
                // make respected tab selected
                tabs.setViewPager(m_viewPager);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });

        setTracker();
    }

    public String getServerDataUrl() {
        String dataUrl = "";
        switch (items_type) {
            default:
                break;
            case 1:
                dataUrl = "getAvailableItemsGSONLimited";
                break;
            case 2:
                dataUrl = "getNotAvailableItemsGSONLimited";
                break;
            case 3:
                dataUrl = "getOutofStockItemsGSONLimited";
                break;
        }
        return dataUrl;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //overridePendingTransition(R.anim.anim_zoom_out_back, R.anim.anim_slideup_back);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //AppEventsLogger.deactivateApp(this);
    }

    public MenuItem getSearchMenuItem() {
        return searchMenuItem;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if (items_type == 1 || items_type == 3) {
            getMenuInflater().inflate(R.menu.menu_search_results, menu);
            SearchManager searchManager =
                    (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            searchMenuItem = menu.findItem(R.id.search);
            SearchView searchView =
                    (SearchView) searchMenuItem.getActionView();
            searchView.setSearchableInfo(
                    searchManager.getSearchableInfo(getComponentName()));

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    MenuItem searchMenuItem = getSearchMenuItem();
                    if (searchMenuItem != null) {
                        SearchView sv = (SearchView) searchMenuItem.getActionView();
                        sv.onActionViewCollapsed();
                    }
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    getContentResolver().delete(
                            SearchResult.CONTENT_URI,
                            null,
                            null
                    );
                    if (s.length() > 2) {
                        loadData(s);
                    }
                    return true;
                }
            });
        } else if (items_type == 2) {
            getMenuInflater().inflate(R.menu.menu_non_available_items, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    private void loadData(final String searchText) {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Utils.RestApiPathUrl).build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);

        apiSuggestionsService.searchFeeds(m_spCode, m_stCode, items_type, searchText, "0", new Callback<ApiResponseModel.SearchModel>() {

            @Override
            public void success(ApiResponseModel.SearchModel itemResults, Response response) {
                int success = itemResults.error;
                if (success == 1) {

                    ArrayList<SearchResultsSnippet> itemsList = itemResults.items_service_providers;
                    m_async5 = new StoreItemsInSqllite().execute(itemsList);
                } else {
                    Toast.makeText(getApplicationContext(), "No result Found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e(TAG + "Failure", error.toString());
            }
        });
    }

    private class StoreItemsInSqllite extends AsyncTask<ArrayList, Void, Void> {
        @Override
        protected Void doInBackground(ArrayList... params) {
            ArrayList<SearchResultsSnippet> itemsList = params[0];

            if (itemsList == null)
                return null;

            for (int i = 0; i < itemsList.size(); i++) {
                if (isCancelled()) {
                    break;
                }
                String itemId = itemsList.get(i).m_itemId;
                String itemCode = itemsList.get(i).m_itemCode;
                String itemName = Utils.strForSqlite(itemsList.get(i).m_itemName);
                String itemUnit = Utils.strForSqlite(itemsList.get(i).m_itemUnit);
                String itemTypeCode = itemsList.get(i).m_itemTypeCode;
                String itemQuantity = itemsList.get(i).m_itemQuantity;
                double itemCost = itemsList.get(i).m_itemCost;
                String itemImgFlag = itemsList.get(i).m_itemImgFlag;
                String itemImgAddress = Utils.strForSqlite(itemsList.get(i).m_itemImgAddress);
                String itemShortDesc = Utils.strForSqlite(itemsList.get(i).m_itemShortDesc);
                String itemDesc = Utils.strForSqlite(itemsList.get(i).m_itemDesc);
                int itemStatus = itemsList.get(i).m_itemStatus;
                String scCode = itemsList.get(i).m_scCode;

                ContentValues values = new ContentValues();
                values.put(SearchResult.COLUMN_ITEM_ID, itemId);
                values.put(SearchResult.COLUMN_ITEM_CODE, itemCode);
                values.put(SearchResult.COLUMN_ITEM_NAME, itemName);
                values.put(SearchResult.COLUMN_ITEM_IMG_FLAG, itemImgFlag);
                values.put(SearchResult.COLUMN_ITEM_IMG_ADD, itemImgAddress);
                values.put(SearchResult.COLUMN_ITEM_UNIT, itemUnit);
                values.put(SearchResult.COLUMN_ITEM_COST, itemCost);
                values.put(SearchResult.COLUMN_ITEM_SHORT_DESC, itemShortDesc);
                values.put(SearchResult.COLUMN_ITEM_DESC, itemDesc);
                values.put(SearchResult.COLUMN_ITEM_STATUS, itemStatus);
                values.put(SearchResult.COLUMN_ITEM_TYPE, itemTypeCode);
                values.put(SearchResult.COLUMN_ITEM_QUANTITY_SELECTED, itemQuantity);
                values.put(SearchResult.COLUMN_SC_CODE, scCode);


                getContentResolver().insert(SearchResult.CONTENT_URI, values);
            }
            return null;
        }
    }

    @Override
    public void startActivity(Intent intent) {
        // check if search intent
        if (Intent.ACTION_SEARCH.equals(intent.getAction()) || Intent.ACTION_VIEW.equals(intent.getAction())) {
            intent.putExtra("items_type", value);
        }
        super.startActivity(intent);
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(this.getClass().getCanonicalName());

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }
}
