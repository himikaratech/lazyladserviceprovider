package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Saurabh on 13/04/15.
 */
public class ItemsCategoryAdapter extends BaseAdapter {

    private Activity m_activity;
    private ArrayList<ItemSnippet> m_itemDetails;

    private class ViewHolder {
        TextView itemNameTextView;
        TextView itemDescTextView;
        TextView itemMrpTextView;
        TextView itemPriceText;
        TextView itemCodeTextView;
        EditText itemCostEditText;
        ImageButton changePriceButton;
        ImageView itemImageView;
        RadioGroup itemAvailabilityStatus;
        RadioButton availableRadioButton;
        RadioButton nonAvailableRadioButton;
        RadioButton outOfStockRadioButton;
    }


    public ItemsCategoryAdapter(Activity activity, ArrayList<ItemSnippet> itemsDetails) {
        m_activity = activity;
        m_itemDetails = itemsDetails;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (m_itemDetails != null) {
            count = m_itemDetails.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (m_itemDetails != null) {
            return m_itemDetails.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_itemDetails != null) {
            return Long.parseLong(m_itemDetails.get(position).m_itemId);
        }
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (m_itemDetails != null) {

            final ViewHolder holder;

            String itemCode = ((ItemSnippet) getItem(position)).m_itemCode;
            String itemName = ((ItemSnippet) getItem(position)).m_itemName;
            String itemShortDesc = ((ItemSnippet) getItem(position)).m_itemShortDesc;
            String itemDesc = ((ItemSnippet) getItem(position)).m_itemDesc;
            double itemCost = ((ItemSnippet) getItem(position)).m_itemCost;
            String itemImageAdd = ((ItemSnippet) getItem(position)).m_itemImgAddress;

            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.items_list_view_details, (ViewGroup) null);
                holder = new ViewHolder();
                holder.itemNameTextView = (TextView) convertView.findViewById(R.id.item_name_textview);
                holder.itemDescTextView = (TextView) convertView.findViewById(R.id.item_desc_textview);
                holder.itemMrpTextView = (TextView) convertView.findViewById(R.id.item_mrp_textview);
                holder.itemCodeTextView = (TextView) convertView.findViewById(R.id.item_code_textview);
                holder.itemCostEditText = (EditText) convertView.findViewById(R.id.item_cost_editview);
                holder.itemAvailabilityStatus = (RadioGroup) convertView.findViewById(R.id.availability_status);
                holder.changePriceButton = (ImageButton) convertView.findViewById(R.id.change_price_button);
                holder.availableRadioButton = (RadioButton) convertView.findViewById(R.id.radio_available);
                holder.nonAvailableRadioButton = (RadioButton) convertView.findViewById(R.id.radio_nAvailable);
                holder.outOfStockRadioButton = (RadioButton) convertView.findViewById(R.id.radio_outOfStock);
                holder.itemPriceText = (TextView) convertView.findViewById(R.id.price_text);
                holder.itemImageView = (ImageView) convertView.findViewById(R.id.item_image);
                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
                holder.itemAvailabilityStatus.setOnCheckedChangeListener(null);
            }

            holder.availableRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
            holder.nonAvailableRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
            holder.outOfStockRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
            holder.availableRadioButton.setButtonDrawable(R.drawable.inactive);
            holder.nonAvailableRadioButton.setButtonDrawable(R.drawable.inactive);
            holder.outOfStockRadioButton.setButtonDrawable(R.drawable.inactive);

            switch (m_itemDetails.get(position).m_itemStatus) {
                default:
                case 0:
                    holder.itemAvailabilityStatus.clearCheck();
                    break;
                case 1:
                    holder.itemAvailabilityStatus.check(R.id.radio_available);
                    holder.availableRadioButton.setTextColor(Color.parseColor("#ffffff"));
                    holder.availableRadioButton.setButtonDrawable(R.drawable.active);
                    holder.nonAvailableRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
                    holder.outOfStockRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
                    holder.nonAvailableRadioButton.setButtonDrawable(R.drawable.inactive);
                    holder.outOfStockRadioButton.setButtonDrawable(R.drawable.inactive);

                    break;
                case 2:
                    holder.itemAvailabilityStatus.check(R.id.radio_nAvailable);
                    holder.nonAvailableRadioButton.setTextColor(Color.parseColor("#ffffff"));
                    holder.nonAvailableRadioButton.setButtonDrawable(R.drawable.active);
                    holder.availableRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
                    holder.outOfStockRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
                    holder.availableRadioButton.setButtonDrawable(R.drawable.inactive);
                    holder.outOfStockRadioButton.setButtonDrawable(R.drawable.inactive);

                    break;
                case 3:
                    holder.itemAvailabilityStatus.check(R.id.radio_outOfStock);
                    holder.outOfStockRadioButton.setTextColor(Color.parseColor("#ffffff"));
                    holder.outOfStockRadioButton.setButtonDrawable(R.drawable.active);
                    holder.availableRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
                    holder.nonAvailableRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
                    holder.availableRadioButton.setButtonDrawable(R.drawable.inactive);
                    holder.nonAvailableRadioButton.setButtonDrawable(R.drawable.inactive);
                    break;
            }

            Typeface typeFace = Typeface.createFromAsset(m_activity.getAssets(), "fonts/Roboto_Regular.ttf");
            holder.itemNameTextView.setTypeface(typeFace);
            holder.itemMrpTextView.setTypeface(typeFace);
            holder.itemPriceText.setTypeface(typeFace);
            holder.itemCostEditText.setTypeface(typeFace);

            holder.itemNameTextView.setText(itemName);
            holder.itemDescTextView.setText(itemDesc);
            holder.itemMrpTextView.setText(Double.toString(itemCost));
            holder.itemCodeTextView.setText(itemCode);

            if (itemImageAdd == null || itemImageAdd == "") itemImageAdd = "\"\"";
            Picasso.with(parent.getContext())
                    .load(itemImageAdd)
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.no_image)
                    .fit().centerInside()
                    .into(holder.itemImageView);

            holder.itemAvailabilityStatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    switch (checkedId) {
                        default:
                            m_itemDetails.get(position).m_itemStatus = 0;
                            break;
                        case R.id.radio_available:
                            m_itemDetails.get(position).m_itemStatus = 1;
                            holder.availableRadioButton.setTextColor(Color.parseColor("#ffffff"));
                            holder.availableRadioButton.setButtonDrawable(R.drawable.active);
                            holder.nonAvailableRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
                            holder.outOfStockRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
                            holder.nonAvailableRadioButton.setButtonDrawable(R.drawable.inactive);
                            holder.outOfStockRadioButton.setButtonDrawable(R.drawable.inactive);

                            break;
                        case R.id.radio_nAvailable:
                            m_itemDetails.get(position).m_itemStatus = 2;
                            holder.nonAvailableRadioButton.setTextColor(Color.parseColor("#ffffff"));
                            holder.nonAvailableRadioButton.setButtonDrawable(R.drawable.active);
                            holder.availableRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
                            holder.outOfStockRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
                            holder.availableRadioButton.setButtonDrawable(R.drawable.inactive);
                            holder.outOfStockRadioButton.setButtonDrawable(R.drawable.inactive);

                            break;
                        case R.id.radio_outOfStock:
                            m_itemDetails.get(position).m_itemStatus = 3;
                            holder.outOfStockRadioButton.setTextColor(Color.parseColor("#ffffff"));
                            holder.outOfStockRadioButton.setButtonDrawable(R.drawable.active);
                            holder.availableRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
                            holder.nonAvailableRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
                            holder.availableRadioButton.setButtonDrawable(R.drawable.inactive);
                            holder.nonAvailableRadioButton.setButtonDrawable(R.drawable.inactive);

                            break;
                    }
                }
            });

            holder.changePriceButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    double cost;
                    String mystring = holder.itemCostEditText.getText().toString();
                    if (mystring.matches("^[0-9]*\\.?[0-9]*$") && !mystring.isEmpty()) {
                        cost = Double.parseDouble(mystring);
                        m_itemDetails.get(position).m_itemCost = cost;
                        holder.itemMrpTextView.setText(Double.toString(cost));
                        m_itemDetails.get(position).m_priceChanged = true;
                    }
                }
            });
        }
        return convertView;
    }
}
