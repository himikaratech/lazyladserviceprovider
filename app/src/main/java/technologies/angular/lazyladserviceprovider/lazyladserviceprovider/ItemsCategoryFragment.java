package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiRequestModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiResponseModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiSuggestionService;

import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.UserDetails;


/**
 * Created by Saurabh on 31/03/15.
 */
public class ItemsCategoryFragment extends Fragment {

    private static final String TAG = "ItemsCategoryFragment";
    private String m_stCode;
    private String m_scCode;
    private String m_spCode;
    private String JsonDataArray;

    private ListView m_itemDetailsListView;
    private ArrayList<ItemSnippet> m_itemsList;
    private ItemsCategoryAdapter m_itemsDetailAdapter;

    private ProgressDialog progressDialog;
    private Button m_saveSelectedItemsButton;
    private Boolean dataLoading = false;
    private Boolean shallLoad = true;

    public static ItemsCategoryFragment newInstance(int scCode) {
        ItemsCategoryFragment fragmentFirst = new ItemsCategoryFragment();
        Bundle args = new Bundle();
        args.putString("scCode", Integer.toString(scCode));
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        m_scCode = getArguments().getString("scCode");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.items_fragment, container, false);

        if (!Utils.isNetworkAvailable(getActivity())) {
            Toast.makeText(getActivity(), "Please Check Your Network Connectivity", Toast.LENGTH_SHORT).show();
        }

        Cursor m_cursor = getActivity().getContentResolver().query(UserDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    m_spCode = m_cursor.getString(1);
                    m_stCode = m_cursor.getString(4);
                    m_cursor.moveToNext();
                }
            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }
        if (m_cursor != null) m_cursor.close();
        m_itemDetailsListView = (ListView) rootView.findViewById(R.id.items_fg_details_list_view);

        if (m_itemsDetailAdapter != null) {
            m_itemDetailsListView.setAdapter(m_itemsDetailAdapter);
        }

        m_saveSelectedItemsButton = (Button) rootView.findViewById(R.id.confirm_items_aval_button);
        m_saveSelectedItemsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveUpdatedItemDetails();
            }
        });

        m_itemDetailsListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) { // TODO Auto-generated method stub
                if (null == m_itemDetailsListView)
                    return;

                int threshold = 1;
                int count = m_itemDetailsListView.getCount();

                if (scrollState == SCROLL_STATE_IDLE) {
                    if (shallLoad && !dataLoading && m_itemDetailsListView.getLastVisiblePosition() >= count - threshold) {
                        dataLoading = true;
                        getItemsSnippetListReady();
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                //leave this empty
                if (totalItemCount == 0)
                    onScrollStateChanged(view, SCROLL_STATE_IDLE);
            }
        });
        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    private void saveUpdatedItemDetails() {
        JSONArray jsonObject = new JSONArray();
        for (int i = 0; i < m_itemsList.size(); i++) {
            JSONObject smallJsonObject = new JSONObject();
            if (m_itemsList.get(i).m_itemStatus == 1 || m_itemsList.get(i).m_itemStatus == 2
                    || m_itemsList.get(i).m_itemStatus == 3 || m_itemsList.get(i).m_priceChanged) {
                String m_itemId = m_itemsList.get(i).m_itemId;
                String m_itemCode = m_itemsList.get(i).m_itemCode;
                String m_itemName = m_itemsList.get(i).m_itemName;
                String m_itemImgFlag = m_itemsList.get(i).m_itemImgFlag;
                String m_itemImgAddress = m_itemsList.get(i).m_itemImgAddress;
                String m_itemUnit = m_itemsList.get(i).m_itemUnit;
                String m_itemShortDesc = m_itemsList.get(i).m_itemShortDesc;
                String m_itemDesc = m_itemsList.get(i).m_itemDesc;
                int m_itemStatus = m_itemsList.get(i).m_itemStatus;
                if (m_itemStatus == 0) m_itemStatus = ((ItemsActivity) getActivity()).items_type;
                double m_itemCost = m_itemsList.get(i).m_itemCost;
                String m_itemTypeCode = m_itemsList.get(i).m_itemTypeCode;
                String m_itemQuantity = m_itemsList.get(i).m_itemQuantity;
                try {
                    smallJsonObject.put("item_id", m_itemId);
                    smallJsonObject.put("item_code", m_itemCode);
                    smallJsonObject.put("item_name", m_itemName);
                    smallJsonObject.put("item_img_flag", m_itemImgFlag);
                    smallJsonObject.put("item_img_add", m_itemImgAddress);
                    smallJsonObject.put("item_unit", m_itemUnit);
                    smallJsonObject.put("item_short_desc", m_itemShortDesc);
                    smallJsonObject.put("item_desc", m_itemDesc);
                    smallJsonObject.put("item_status", m_itemStatus);
                    smallJsonObject.put("item_cost", m_itemCost);
                    smallJsonObject.put("item_type_code", m_itemTypeCode);
                    smallJsonObject.put("item_qunatity", m_itemQuantity);
                    jsonObject.put(smallJsonObject);
                    JsonDataArray = jsonObject.toString();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Updating. Please wait...");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Utils.RestApiPathUrl).build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
        ApiRequestModel requestModel = new ApiRequestModel();

        ApiRequestModel.save save1 = requestModel.new save(JsonDataArray.toString(), m_spCode, m_scCode);
        apiSuggestionsService.save(save1, new Callback<ApiResponseModel.ResponseSave>() {
            @Override
            public void success(ApiResponseModel.ResponseSave responseData, retrofit.client.Response response) {
                int success = responseData.error;
                if (success == 1) {
                    progressDialog.dismiss();
                    m_itemsList = null;
                    getItemsSnippetListReady();
                    Toast.makeText(getActivity(), "Updated Successfully", Toast.LENGTH_SHORT).show();
                } else {
                }
                progressDialog.dismiss();
            }

            @Override
            public void failure(final RetrofitError error) {
                Log.e(TAG, error.toString());
                progressDialog.dismiss();
            }
        });
    }

    private void getItemsSnippetListReady() {

        String url_items_serviceProvider = ((ItemsActivity) getActivity()).getServerDataUrl();

        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();

        String previous_item_code;
        if (m_itemsList == null || m_itemsList.isEmpty()) {
            previous_item_code = "0";
        } else {
            previous_item_code = m_itemsList.get(m_itemsList.size() - 1).m_itemCode;
        }

        RestAdapter restAdapter = new RestAdapter.Builder()

                .setEndpoint(Utils.Base_Url).build();

        ApiSuggestionService api = restAdapter.create(ApiSuggestionService.class);
        api.avail(url_items_serviceProvider, m_spCode, m_scCode, previous_item_code, new Callback<ItemModel>() {

            @Override
            public void success(ItemModel responseItems, retrofit.client.Response response) {
                if (responseItems.error == 1) {
                    if (m_itemsList == null) {

                        m_itemsList = responseItems.items_service_providers;
                        m_itemsDetailAdapter = new ItemsCategoryAdapter(getActivity(), m_itemsList);
                        m_itemDetailsListView.setAdapter((ListAdapter) m_itemsDetailAdapter);
                    } else {
                        m_itemsList.addAll(responseItems.items_service_providers);
                        m_itemsDetailAdapter.notifyDataSetChanged();
                    }
                }

                if (null == responseItems.items_service_providers || responseItems.items_service_providers.isEmpty())
                    shallLoad = false;
                progressDialog.dismiss();
                dataLoading = false;
            }

            @Override
            public void failure(final RetrofitError error) {
                Log.e(TAG, error.toString());
                progressDialog.dismiss();
                dataLoading = false;
            }
        });
    }
}
