package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by Saurabh on 07/03/15.
 */
public class ItemsTabPagerAdapter extends FragmentPagerAdapter {

    ArrayList<Integer> m_itemsCategoryCode;

    public ItemsTabPagerAdapter(FragmentManager fragmentManager, ArrayList<Integer> itemsCategoryCode) {
        super(fragmentManager);
        m_itemsCategoryCode = itemsCategoryCode;
    }

    @Override
    public Fragment getItem(int position) {
        int sc_code = m_itemsCategoryCode.get(position);
        switch (position) {
            case 0: {
                return ItemsCategoryFragment.newInstance(sc_code);
            }
            case 1: {
                return ItemsCategoryFragment.newInstance(sc_code);
            }
            case 2: {
                return ItemsCategoryFragment.newInstance(sc_code);
            }
            case 3: {
                return ItemsCategoryFragment.newInstance(sc_code);
            }
            case 4: {
                return ItemsCategoryFragment.newInstance(sc_code);
            }
            case 5: {
                return ItemsCategoryFragment.newInstance(sc_code);
            }
            case 6: {
                return ItemsCategoryFragment.newInstance(sc_code);
            }
            case 7: {
                return ItemsCategoryFragment.newInstance(sc_code);
            }
            case 8: {
                return ItemsCategoryFragment.newInstance(sc_code);
            }
        }
        return null;
    }

    @Override
    public int getCount() {
        if (ItemsActivity.m_itemsCategoryNames != null)
            return ItemsActivity.m_itemsCategoryNames.size();

        return 0;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return ItemsActivity.m_itemsCategoryNames.get(position);
    }
}