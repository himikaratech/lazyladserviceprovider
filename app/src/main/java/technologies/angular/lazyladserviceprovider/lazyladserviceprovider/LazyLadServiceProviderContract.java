package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by sakshigupta on 14/09/15.
 */
public class LazyLadServiceProviderContract {

    // The "Content authority" is a name for the entire content provider, similar to the
    // relationship between a domain name and its website.  A convenient string to use for the
    // content authority is the package name for the app, which is guaranteed to be unique on the
    // device.
    public static final String CONTENT_AUTHORITY = "technologies.angular.lazyladserviceprovider.lazyladserviceprovider.provider";

    // Use CONTENT_AUTHORITY to create the base of all URI's which apps will use to contact
    // the content provider.
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_USER_DETAILS = "service_providers";
    public static final String PATH_ITEMS_NOT_AVAILABLE = "items_not_available";
    public static final String PATH_ITEMS_AVAILABLE = "items_available";
    public static final String PATH_ITEMS_OUT_OF_STOCK = "items_out_of_stock";
    public static final String PATH_SERVICE_TYPE_CATEGORY = "service_type_categories";
    public static final String PATH_SEARCH_RESULT = "search_result";
    public static final String PATH_REGISTRATION_SELLER_DETAILS = "registration_seller_details";
    public static final String PATH_REGISTRATION_SERVICE_AREAS = "registration_service_areas";
    public static final String PATH_REGISTRATION_INVENTORY_DETAILS = "registration_inventory_details";

    /* Inner class that defines the table contents of the userDetails table */
    public static final class UserDetails implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_USER_DETAILS).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_USER_DETAILS;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_USER_DETAILS;

        // Table name
        public static final String TABLE_NAME = "service_providers";

        public static final String COLUMN_SP_CODE = "sp_code";
        public static final String COLUMN_SP_EMAIL = "sp_email";
        public static final String COLUMN_SP_PASSWORD = "sp_password";
        public static final String COLUMN_ST_CODE = "m_stCode";

        public static Uri buildUserDetailsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    /* Inner class that defines the table contents of the nonAvailableItems table */
    public static final class NonAvailableItems implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_ITEMS_NOT_AVAILABLE).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_ITEMS_NOT_AVAILABLE;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_ITEMS_NOT_AVAILABLE;

        // Table name
        public static final String TABLE_NAME = "items_not_available";

        public static final String COLUMN_ITEM_CODE = "item_code";
        public static final String COLUMN_ITEM_NAME = "item_name";
        public static final String COLUMN_ITEM_IMG_FLAG = "item_img_flag";
        public static final String COLUMN_ITEM_IMG_ADD = "item_img_add";
        public static final String COLUMN_ITEM_UNIT = "item_unit";
        public static final String COLUMN_ITEM_SHORT_DESC = "item_short_desc";
        public static final String COLUMN_ITEM_DESC = "item_desc";
        public static final String COLUMN_ITEM_STATUS = "item_status";
        public static final String COLUMN_ITEM_COST = "item_cost";
        public static final String COLUMN_ITEM_TYPE_CODE = "item_type_code";
        public static final String COLUMN_ITEM_QUANTITY = "item_quantity";

        public static Uri buildNonAvailableItemsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    /* Inner class that defines the table contents of the outOfStockItems table */
    public static final class OutOfStockItems implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_ITEMS_OUT_OF_STOCK).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_ITEMS_OUT_OF_STOCK;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_ITEMS_OUT_OF_STOCK;

        // Table name
        public static final String TABLE_NAME = "items_out_of_stock";

        public static final String COLUMN_ITEM_CODE = "item_code";
        public static final String COLUMN_ITEM_NAME = "item_name";
        public static final String COLUMN_ITEM_IMG_FLAG = "item_img_flag";
        public static final String COLUMN_ITEM_IMG_ADD = "item_img_add";
        public static final String COLUMN_ITEM_UNIT = "item_unit";
        public static final String COLUMN_ITEM_SHORT_DESC = "item_short_desc";
        public static final String COLUMN_ITEM_DESC = "item_desc";
        public static final String COLUMN_ITEM_STATUS = "item_status";
        public static final String COLUMN_ITEM_COST = "item_cost";
        public static final String COLUMN_ITEM_TYPE_CODE = "item_type_code";
        public static final String COLUMN_ITEM_QUANTITY = "item_quantity";

        public static Uri buildOutOfStockItemsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    /* Inner class that defines the table contents of the availableItems table */
    public static final class AvailableItems implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_ITEMS_AVAILABLE).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_ITEMS_AVAILABLE;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_ITEMS_AVAILABLE;

        // Table name
        public static final String TABLE_NAME = "items_available";

        public static final String COLUMN_ITEM_CODE = "item_code";
        public static final String COLUMN_ITEM_NAME = "item_name";
        public static final String COLUMN_ITEM_IMG_FLAG = "item_img_flag";
        public static final String COLUMN_ITEM_IMG_ADD = "item_img_add";
        public static final String COLUMN_ITEM_UNIT = "item_unit";
        public static final String COLUMN_ITEM_SHORT_DESC = "item_short_desc";
        public static final String COLUMN_ITEM_DESC = "item_desc";
        public static final String COLUMN_ITEM_STATUS = "item_status";
        public static final String COLUMN_ITEM_COST = "item_cost";
        public static final String COLUMN_ITEM_TYPE_CODE = "item_type_code";
        public static final String COLUMN_ITEM_QUANTITY = "item_quantity";

        public static Uri buildAvailableItemsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    /* Inner class that defines the table contents of the serviceTypeCategories table */
    public static final class ServiceTypeCategories implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_SERVICE_TYPE_CATEGORY).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_SERVICE_TYPE_CATEGORY;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_SERVICE_TYPE_CATEGORY;

        // Table name
        public static final String TABLE_NAME = "service_type_categories";

        public static final String COLUMN_ST_CODE = "st_code";
        public static final String COLUMN_SC_CODE = "sc_code";
        public static final String COLUMN_SC_NAME = "sc_name";

        public static Uri buildServiceTypeCategoriesUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    /* Inner class that defines the table contents of the searchResult table */
    public static final class SearchResult implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_SEARCH_RESULT).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_SEARCH_RESULT;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_SEARCH_RESULT;

        // Table name
        public static final String TABLE_NAME = "search_result";

        public static final String COLUMN_ITEM_ID = "item_id";
        public static final String COLUMN_ITEM_CODE = "item_code";
        public static final String COLUMN_ITEM_NAME = "item_name";
        public static final String COLUMN_ITEM_IMG_FLAG = "item_img_flag";
        public static final String COLUMN_ITEM_IMG_ADD = "item_img_address";
        public static final String COLUMN_ITEM_UNIT = "item_unit";
        public static final String COLUMN_ITEM_COST = "item_cost";
        public static final String COLUMN_ITEM_SHORT_DESC = "item_short_desc";
        public static final String COLUMN_ITEM_DESC = "item_desc";
        public static final String COLUMN_ITEM_STATUS = "item_status";
        public static final String COLUMN_ITEM_TYPE = "item_type";
        public static final String COLUMN_ITEM_QUANTITY_SELECTED = "item_quantity_selected";
        public static final String COLUMN_SC_CODE = "sc_code";

        public static Uri buildSearchResultUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    /* Inner class that defines the table contents of the registrationSellerDetails table */
    public static final class RegistrationSellerDetails implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_REGISTRATION_SELLER_DETAILS).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_REGISTRATION_SELLER_DETAILS;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_REGISTRATION_SELLER_DETAILS;

        // Table name
        public static final String TABLE_NAME = "registration_seller_details";

        public static final String COLUMN_SELLER_NAME = "seller_name";
        public static final String COLUMN_SELLER_PHONE = "seller_phone";
        public static final String COLUMN_SELLER_PHONE_VAL_FLAG = "seller_phone_val_flag";
        public static final String COLUMN_PASSWORD = "password";
        public static final String COLUMN_SHOP_NAME = "shop_name";
        public static final String COLUMN_SHOP_ADDRESS = "shop_address";
        public static final String COLUMN_SHOP_CITY = "shop_city";
        public static final String COLUMN_SHOP_AREAS = "shop_area";
        public static final String COLUMN_SHOP_TYPE = "shop_type";
        public static final String COLUMN_SHOP_OPEN_TIME = "shop_open_time";
        public static final String COLUMN_SHOP_CLOSE_TIME = "shop_close_time";
        public static final String COLUMN_DELIVERY_TIME = "delivery_time";
        public static final String COLUMN_MIN_DELIVERY_AMT = "min_delivery_amt";
        public static final String COLUMN_SHOP_OPEN_BITS = "shop_open_bits";
        public static final String COLUMN_INVENTORY_BY_US = "inventory_by_us";
        public static final String COLUMN_LOGISTICS_SERVICES = "logistics_services";
        public static final String COLUMN_SODEXO_COUPONS = "sodexo_coupons";
        public static final String COLUMN_MIDNIGHT_DELIVERY = "midnight_delivery";
        public static final String COLUMN_REFERRAL = "referral";
        public static final String COLUMN_VERIFICATION_SOURCE = "verification_source";

        public static Uri buildRegistrationSellerDetailsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    /* Inner class that defines the table contents of the registrationSellerAreas table */
    public static final class RegistrationSellerAreas implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_REGISTRATION_SERVICE_AREAS).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_REGISTRATION_SERVICE_AREAS;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_REGISTRATION_SERVICE_AREAS;

        // Table name
        public static final String TABLE_NAME = "registration_service_areas";

        public static final String COLUMN_AREA_CODE = "area_code";
        public static final String COLUMN_AREA_NAME = "area_name";
        public static final String COLUMN_DELIVERABLE = "delieverable";

        public static Uri buildRegistrationSellerAreasUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    /* Inner class that defines the table contents of the registrationInventoryDetails table */
    public static final class RegistrationInventoryDetails implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_REGISTRATION_INVENTORY_DETAILS).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_REGISTRATION_INVENTORY_DETAILS;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_REGISTRATION_INVENTORY_DETAILS;

        // Table name
        public static final String TABLE_NAME = "registration_inventory_details";

        public static final String COLUMN_ITEM_CODE = "item_code";
        public static final String COLUMN_ITEM_NAME = "item_name";
        public static final String COLUMN_ITEM_COST = "item_cost";
        public static final String COLUMN_ITEM_DESC = "item_desc";
        public static final String COLUMN_ITEM_IMG_FLAG = "item_img_flag";
        public static final String COLUMN_ITEM_IMG_ADD = "item_img_add";
        public static final String COLUMN_AVAILABLE = "available";

        public static Uri buildRegistrationInventoryDetailsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }


}
