package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.UserDetails;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiRequestModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiResponseModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiSuggestionService;

public class LoginActivity extends Activity {

    private EditText m_emailEditView;
    private EditText m_passwordEditView;

    private String m_email;
    private String m_password;

    private Button m_signInButton;

    private TextView m_title;
    private TextView m_details;
    private TextView m_endNote;

    private String m_servProvId;
    private String m_servProvType;

    private String regid;
    int count = 0;
    static final String TAG = "GCMDemo";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private static String SENDER_ID = "495574458613";
    private GoogleCloudMessaging gcm;
    private AtomicInteger msgId = new AtomicInteger();

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        LoginActivity.this.getWindow().
                setBackgroundDrawableResource(R.drawable.login);
        final int[] drawablearray = new int[]{R.drawable.login1, R.drawable.login2};
        gettingBackground(drawablearray);

        m_title = (TextView) findViewById(R.id.Signin);
        m_endNote = (TextView) findViewById(R.id.endNote);
        m_details = (TextView) findViewById(R.id.enterDetails);
        m_emailEditView = (EditText) findViewById(R.id.email_editview);
        m_passwordEditView = (EditText) findViewById(R.id.password_editview);
        m_signInButton = (Button) findViewById(R.id.sign_in_button);
        m_servProvId = "0";
        m_servProvType = "0";
        context = getApplicationContext();

        Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto_Regular.ttf");
        m_title.setTypeface(typeFace);
        m_details.setTypeface(typeFace);
        m_endNote.setTypeface(typeFace);
        m_emailEditView.setTypeface(typeFace);
        m_passwordEditView.setTypeface(typeFace);
        m_signInButton.setTypeface(typeFace);

        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this);
            regid = getRegistrationId(context);
            Log.i("Registration Id", regid);
            if (regid.isEmpty()) {
                registerInBackground();
            }
        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
        }

        m_signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_email = m_emailEditView.getText().toString();
                m_password = m_passwordEditView.getText().toString();
                if (m_email.isEmpty() || m_email == null) {
                    Toast.makeText(getApplicationContext(), "Please enter your username", Toast.LENGTH_SHORT).show();
                } else if (m_password.isEmpty() || m_password == null) {
                    Toast.makeText(getApplicationContext(), "Please enter your password", Toast.LENGTH_SHORT).show();
                } else {
                    SignInServProv();
                }
            }
        });

        setTracker();
    }

    private void gettingBackground(final int[] drawablearray) {
        // TODO Auto-generated method stub
        new Handler().postDelayed(new Runnable() {
            public void run() {
                if (count < drawablearray.length) {
                    LoginActivity.this.getWindow().
                            setBackgroundDrawableResource(drawablearray[count]);
                    count++;
                    gettingBackground(drawablearray);
                } else {
                    count = 0;
                    LoginActivity.this.getWindow().
                            setBackgroundDrawableResource(R.drawable.login);
                    gettingBackground(drawablearray);
                }
            }
        }, 10000);
    }

    private void SignInServProv() {
        final ProgressDialog pDialog;

        pDialog = new ProgressDialog(LoginActivity.this);
        pDialog.setMessage("Signing In. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Utils.RestApiPathUrl).build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
        ApiRequestModel requestModel = new ApiRequestModel();
        ApiRequestModel.User user = requestModel.new User(m_email, m_password, regid);
        apiSuggestionsService.login(user, new Callback<ApiResponseModel.ResponseDataLogin>() {

            @Override
            public void success(ApiResponseModel.ResponseDataLogin responseLogin, Response response) {
                int success = responseLogin.error;
                if (success == 1) {
                    m_servProvId = responseLogin.code;
                    m_servProvType = responseLogin.type;
                    ContentValues values = new ContentValues();
                    values.put(UserDetails.COLUMN_SP_CODE, m_servProvId);
                    values.put(UserDetails.COLUMN_SP_EMAIL, m_email);
                    values.put(UserDetails.COLUMN_SP_PASSWORD, m_password);
                    values.put(UserDetails.COLUMN_ST_CODE, m_servProvType);


                    Uri insertUri = getContentResolver().insert(UserDetails.CONTENT_URI, values);

                } else {
                    Toast.makeText(LoginActivity.this, "Invalid Login Details", Toast.LENGTH_SHORT).show();
                }

                if (Integer.parseInt(m_servProvId) != 0) {
                    startMainActivity();
                }
                pDialog.dismiss();
            }

            @Override
            public void failure(final RetrofitError error) {
                pDialog.dismiss();
            }
        });
    }

    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(CurrentOrders.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPlayServices();
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p/>
     * Stores the registration ID and app versionCode in the application's
     * shared preferences.
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    private void registerInBackground() {
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    regid = gcm.register(SENDER_ID);
                    msg = "Device registered, registration ID=" + regid;

                    // For this demo: we don't need to send it because the device
                    // will send upstream messages to a server that echo back the
                    // message using the 'from' address in the message.

                    // Persist the regID - no need to register again.
                    storeRegistrationId(context, regid);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(Object msg) {
            }
        }.execute(null, null, null);

    }

    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    private void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void registeration_begin(View v) {
        Intent intent = new Intent(this, Registration.class);
        startActivity(intent);
    }

    public void call_us(View v) {
        final SharedPreferences prefs = getSharedPreferences(
                "technologies.angular.lazyladserviceprovider.lazyladserviceprovider", Context.MODE_PRIVATE);

        String phone_number=prefs.getString(EditorConstants.CUSTOMER_CARE_CONSTANT,EditorConstants.DEFAULT_CUSTOMER_CARE_CONSTANT);


        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:+91-"+phone_number));
        startActivity(callIntent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(this.getClass().getCanonicalName());

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
