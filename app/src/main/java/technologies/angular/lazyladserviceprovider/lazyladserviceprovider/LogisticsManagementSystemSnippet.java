package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

/**
 * Created by Saurabh on 30/01/15.
 */
public class LogisticsManagementSystemSnippet {

    public int m_delGuyId;
    public String m_delGuyCode;
    public String m_delGuyName;

    public LogisticsManagementSystemSnippet() {
    }

    public LogisticsManagementSystemSnippet(int delGuyId, String delGuyCode, String delGuyName) {
        m_delGuyId = delGuyId;
        m_delGuyCode = delGuyCode;
        m_delGuyName = delGuyName;
    }
}
