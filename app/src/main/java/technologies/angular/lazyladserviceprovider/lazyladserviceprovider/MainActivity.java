package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.UserDetails;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.ServiceTypeCategories;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiRequestModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiResponseModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiSuggestionService;
public class MainActivity extends ActionBarActivity {

    private static final String TAG = "MainActivity";
    private Toolbar m_toolbar;
    private String m_servProvId;
    private TextView m_availableItemsButton;
    private TextView m_outofStockItemsButton;
    private TextView m_nonAvailableItemsButton;
    private TextView m_currentOrdersButton;
    private TextView m_deliveredOrdersButton;

    String TITLES[] = {"Home", "Profile", "Orders", "Share & Earn", "Lazy Wallet","","Rate Us", "Feedback", "Call Us"};
    int ICONS[] = {R.drawable.home, R.drawable.about, R.drawable.orders, R.drawable.share, R.drawable.wallet, R.drawable.ic_launcher, R.drawable.rate, R.drawable.feedback, R.drawable.call_nav};

    RecyclerView mRecyclerView;                           // Declaring RecyclerView
    RecyclerView.Adapter mAdapter;                        // Declaring Adapter For Recycler View
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    DrawerLayout Drawer;                                  // Declaring DrawerLayout
    String Name;
    public static String mAddressOutput = "LazyLad Seller";
    public ActionBarDrawerToggle mDrawerToggle;
    ImageButton m_callUs;

    private View frame;

    private static String[] dummy = new String[]{"a", "b"};
    private static Uri selectUri;
    private static Uri insertUri;
    private static Uri deleteUri;

    static {
        MainActivity.selectUri = Uri.parse((String) ("content://technologies.angular.lazyladserviceprovider.lazyladserviceprovider.provider/SELECT"));
    }

    static {
        MainActivity.insertUri = Uri.parse((String) ("content://technologies.angular.lazyladserviceprovider.lazyladserviceprovider.provider/INSERT"));
    }

    static {
        MainActivity.deleteUri = Uri.parse((String) ("content://technologies.angular.lazyladserviceprovider.lazyladserviceprovider.provider/DELETE"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        setTitle("Home");
        m_toolbar = (Toolbar) findViewById(R.id.items_toolbar);
        m_callUs=(ImageButton)findViewById(R.id.call_us);
        setSupportActionBar(m_toolbar);

        Cursor m_cursor = getContentResolver().query(UserDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    m_servProvId = m_cursor.getString(1);
                    m_cursor.moveToNext();
                }
            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }

        m_cursor = getContentResolver().query(ServiceTypeCategories.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() > 0) {
                getContentResolver().delete(ServiceTypeCategories.CONTENT_URI,
                        null,
                        null
                );
            }
        }
        if (null != m_cursor) m_cursor.close();

        mRecyclerView = (RecyclerView) findViewById(R.id.nav_drawer_RecyclerView); // Assigning the RecyclerView Object to the xml View
       // mRecyclerView.setHasFixedSize(true);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int displayViewWidth_ = size.x;

        frame = (View) findViewById(R.id.content_frame);

        mAdapter = new MyAdapter(TITLES, ICONS, Name, mAddressOutput, MainActivity.this, 1);// Creating the Adapter of MyAdapter class(which we are going to see in a bit)
        mRecyclerView.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(this);

        mRecyclerView.setLayoutManager(mLayoutManager);
        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout); // Drawer object Assigned to the view

        ViewGroup.LayoutParams params = mRecyclerView.getLayoutParams();
        params.width = 9 * (displayViewWidth_ / 10);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutParams(params);

        mDrawerToggle = new ActionBarDrawerToggle(this, Drawer, m_toolbar, R.string.opendrawer, R.string.closedrawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        }; // Drawer Toggle Object Made
        Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
        mDrawerToggle.syncState(); // Finally we set the drawer toggle sync State

        loadServiceTypesCategories();

        m_availableItemsButton = (TextView) findViewById(R.id.available_items_button);
        m_outofStockItemsButton = (TextView) findViewById(R.id.out_of_stock_items_button);
        m_nonAvailableItemsButton = (TextView) findViewById(R.id.non_available_items_button);
        m_currentOrdersButton = (TextView) findViewById(R.id.current_orders_button);
        m_deliveredOrdersButton = (TextView) findViewById(R.id.delivered_orders_button);

        CardView availableCardView = (CardView) findViewById(R.id.availableCardView);
        CardView nAvailableCardView = (CardView) findViewById(R.id.nAvailableCardView);
        CardView outOfStockCardView = (CardView) findViewById(R.id.outOfStockCardView);
        CardView currentCardView = (CardView) findViewById(R.id.currentOrdersCardView);
        CardView deliveredCardView = (CardView) findViewById(R.id.deliveredOrdersCardView);

        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/Roboto_Light.ttf");
        m_availableItemsButton.setTypeface(typeFace);
        m_outofStockItemsButton.setTypeface(typeFace);
        m_nonAvailableItemsButton.setTypeface(typeFace);
        m_currentOrdersButton.setTypeface(typeFace);
        m_deliveredOrdersButton.setTypeface(typeFace);

        availableCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAvailableItemsActivity();
            }
        });

        nAvailableCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startOutofStockItemsActivity();
            }
        });

        outOfStockCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNonAvailableItemsActivity();
            }
        });

        currentCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startCurrentOrdersActivity();
            }
        });

        deliveredCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDeliveredOrdersActivity();
            }
        });


        m_callUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callUs();
            }
        });
        setTracker();
    }

    private void loadServiceTypesCategories() {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Utils.RestApiPathUrl).build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
        ApiRequestModel requestModel = new ApiRequestModel();
        ApiRequestModel.InputForMain main = requestModel.new InputForMain(m_servProvId);
        apiSuggestionsService.mainActivity(main, new Callback<ApiResponseModel.Main>() {

            @Override
            public void success(ApiResponseModel.Main responseResult, retrofit.client.Response response) {
                int success = responseResult.error;
                if (success == 1) {
                    int numberOfServiceCategory = responseResult.mainActivtyList.size();
                    for (int i = 0; i < numberOfServiceCategory; i++) {
                        String stcode = responseResult.mainActivtyList.get(i).m_srvTypeCode;
                        String sccode = responseResult.mainActivtyList.get(i).m_srvCatCode;
                        String scname = responseResult.mainActivtyList.get(i).m_srvCatName;
                        ContentValues values = new ContentValues();
                        values.put(ServiceTypeCategories.COLUMN_ST_CODE, stcode);
                        values.put(ServiceTypeCategories.COLUMN_SC_CODE, sccode);
                        values.put(ServiceTypeCategories.COLUMN_SC_NAME, scname);

                        getContentResolver().insert(ServiceTypeCategories.CONTENT_URI, values);

                    }
                }
            }

            @Override
            public void failure(final RetrofitError error) {
                Log.e(TAG + "Failure", error+" retro");
            }
        });
    }

    private void startAvailableItemsActivity() {
        Intent intent = new Intent(this, ItemsActivity.class);
        intent.putExtra("items_type", "1");
        startActivity(intent);
    }

    private void startOutofStockItemsActivity() {
        Intent intent = new Intent(this, ItemsActivity.class);
        intent.putExtra("items_type", "3");
        startActivity(intent);
    }

    private void startNonAvailableItemsActivity() {
        Intent intent = new Intent(this, ItemsActivity.class);
        intent.putExtra("items_type", "2");
        startActivity(intent);
    }

    private void startCurrentOrdersActivity() {
        Intent intent = new Intent(this, TabViewForOrders.class);
        startActivity(intent);
    }

    private void startDeliveredOrdersActivity() {
        Intent intent = new Intent(this, DeliveredOrders.class);
        startActivity(intent);
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(this.getClass().getCanonicalName());

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    private void callUs()
    {

        final SharedPreferences prefs = getSharedPreferences(
                "technologies.angular.lazyladserviceprovider.lazyladserviceprovider", Context.MODE_PRIVATE);

        String phone_number=prefs.getString(EditorConstants.CUSTOMER_CARE_CONSTANT,EditorConstants.DEFAULT_CUSTOMER_CARE_CONSTANT);
        try {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:+91-"+phone_number));
            startActivity(callIntent);
        } catch (ActivityNotFoundException e) {
            Log.e("helloandroid dialing", "Call failed", e);
        }
    }
}
