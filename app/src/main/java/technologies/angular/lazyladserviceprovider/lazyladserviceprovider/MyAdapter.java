package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Sakshi Gupta .
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private static final int TYPE_HEADER = 0;  // Declaring Variable to Understand which View is being worked on
    // IF the view under inflation and population is header or Item
    private static final int TYPE_ITEM = 1;

    private static final int TYPE_LINE = 2;

    private String mNavTitles[]; // String Array to store the passed titles Value from MainActivity.java
    private int mIcons[];       // Int Array to store the passed icons resource value from MainActivity.java

    private String name;        //String Resource for header View Name
    //private int profile;        //int Resource for header view profile picture
    private String email;       //String Resource for header view email

    private static Activity m_activity;
    private static int m_flag;
    private static int displayViewWidth;
    // Creating a ViewHolder which extends the RecyclerView View Holder
    // ViewHolder are used to to store the inflated views in order to recycle them

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        int Holderid;

        TextView textView;
        ImageView imageView;
        TextView Name;
        TextView email;
        View line;
        View itemLayout;

        public ViewHolder(View itemView, int ViewType) {                 // Creating ViewHolder Constructor with View and viewType As a parameter
            super(itemView);
            itemView.setOnClickListener(this);

            // Here we set the appropriate view in accordance with the the view type as passed when the holder object is created

            if (ViewType == TYPE_ITEM) {
                textView = (TextView) itemView.findViewById(R.id.rowText); // Creating TextView object with the id of textView from item_row.xml
                imageView = (ImageView) itemView.findViewById(R.id.rowIcon);// Creating ImageView object with the id of ImageView from item_row.xml
                itemLayout=(View)itemView.findViewById(R.id.itemLayout);
                Display display = m_activity.getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int displayViewWidth = size.x;
                LinearLayout.LayoutParams layoutParams1 = (LinearLayout.LayoutParams) imageView.getLayoutParams();
                layoutParams1.setMargins(displayViewWidth / 10, 0, 0, 0);
                LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) textView.getLayoutParams();
                layoutParams2.setMargins(displayViewWidth / 15, 0, 0, 0);
                Typeface tf = Typeface.createFromAsset(m_activity.getAssets(), "fonts/Montserrat_Regular.ttf");
                textView.setTypeface(tf);
                Holderid = 1;                                               // setting holder id as 1 as the object being populated are of type item row
            } else if (ViewType == TYPE_LINE) {
                Log.e("line", "working2");
                line = (View) itemView.findViewById(R.id.line);
                FrameLayout.LayoutParams layoutParams1 = (FrameLayout.LayoutParams) line.getLayoutParams();
                layoutParams1.setMargins(displayViewWidth / 10, 0, displayViewWidth / 10, 0);
                Holderid = 5;
            }
            else if (ViewType == TYPE_HEADER) {
                Name = (TextView) itemView.findViewById(R.id.name);         // Creating Text View object from header.xml for name
                email = (TextView) itemView.findViewById(R.id.email);       // Creating Text View object from header.xml for email
                Typeface tf = Typeface.createFromAsset(m_activity.getAssets(), "fonts/Montserrat_Regular.ttf");
                Name.setTypeface(tf);
                email.setTypeface(tf);
                Name.setVisibility(View.GONE);
                LinearLayout.LayoutParams layoutParams1 = (LinearLayout.LayoutParams) Name.getLayoutParams();
                layoutParams1.setMargins(displayViewWidth / 10, 0, 0, 0);
                LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) email.getLayoutParams();
                layoutParams2.setMargins(displayViewWidth / 10, 0, 0, 0);// Creating Text View object from header.xml for email
                Holderid = 0;                                                // Setting holder id = 0 as the object being populated are of type header view
            }
        }

        @Override
        public void onClick(View view) {

            if (getPosition() == 1) {
                if (m_flag != 1) {
                    Intent intent = new Intent(m_activity, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    m_activity.startActivity(intent);
                }
            }
            else if (getPosition() == 2) {
                if (m_flag != 2) {
                    Intent intent = new Intent(m_activity, ProfileActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    m_activity.startActivity(intent);
                }
            }
            else if (getPosition() == 3) {
                if (m_flag != 3) {
                    Intent intent = new Intent(m_activity, TabViewForOrders.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    m_activity.startActivity(intent);
                }
            }
            /*else if (getPosition() == 3) {
                if (m_flag != 3) {
                    Intent intent = new Intent(m_activity, TabViewForOrders.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    m_activity.startActivity(intent);
                }
            } */
            else if (getPosition() == 4) {

                Intent intent = new Intent(m_activity, ShareVia.class);
                m_activity.startActivity(intent);
               /* Intent shareSMS = new Intent(Intent.ACTION_SEND);
                shareSMS.setType("text/plain");
                shareSMS.putExtra(Intent.EXTRA_SUBJECT, "Try this app, its great");
                shareSMS.putExtra(Intent.EXTRA_TEXT, "Free your evenings and weekends of daily needs shopping with LazyLad." + "\n" + "Download it from play store :" + "\n" +
                        "https://play.google.com/store/apps/" + "details?id=technologies.angular.lazylad");

                Intent chooser = new Intent(Intent.ACTION_CHOOSER);
                chooser.putExtra(Intent.EXTRA_INTENT, shareSMS);
                chooser.putExtra(Intent.EXTRA_TITLE, "Share via");
                //          chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, mailIntents.toArray(new Parcelable[] { }));

                m_activity.startActivity(chooser);
        */    }else if (getPosition() == 5) {
                if (m_flag != 5) {
                    Intent intent = new Intent(m_activity, AccountDetails.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    m_activity.startActivity(intent);
                }
            }
            else if (getPosition() == 7) {
                Uri uri = Uri.parse("market://details?id=" + m_activity.getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    m_activity.startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    m_activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + m_activity.getPackageName())));
                }
            } else if (getPosition() == 8) {
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", "contact@angulartechnologies.com", null));
                intent.putExtra(Intent.EXTRA_SUBJECT, "[Customer]Feedback");
                //intent.putExtra(Intent.EXTRA_TEXT, message);
                m_activity.startActivity(Intent.createChooser(intent, "Choose an Email client :"));
            } else if (getPosition() == 9){
                try {

                    final SharedPreferences prefs = m_activity.getSharedPreferences(
                            "technologies.angular.lazyladserviceprovider.lazyladserviceprovider", Context.MODE_PRIVATE);

                    String phone_number=prefs.getString(EditorConstants.CUSTOMER_CARE_CONSTANT,EditorConstants.DEFAULT_CUSTOMER_CARE_CONSTANT);

                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:+91-"+phone_number));
                    m_activity.startActivity(callIntent);
                } catch (ActivityNotFoundException e) {
                    Log.e("helloandroid dialing", "Call failed", e);
                }
            }

        }

    }

    MyAdapter(String Titles[], int Icons[], String Name, String Email, Activity activity, int flagWhichActivity) { // MyAdapter Constructor with titles and icons parameter
        // titles, icons, name, email, profile pic are passed from the main activity as we
        mNavTitles = Titles;                //have seen earlier
        mIcons = Icons;
        name = Name;
        email = Email;
        // profile = Profile;                     //here we assign those passed values to the values we declared here
        //in adapter
        m_activity = activity;
        m_flag = flagWhichActivity;

        Display display = m_activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        displayViewWidth = size.x;
    }

    //Below first we ovverride the method onCreateViewHolder which is called when the ViewHolder is
    //Created, In this method we inflate the item_row.xml layout if the viewType is Type_ITEM or else we inflate header.xml
    // if the viewType is TYPE_HEADER
    // and pass it to the view holder

    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.navigation_drawer_child_items, parent, false); //Inflating the layout

            ViewHolder vhItem = new ViewHolder(v, viewType); //Creating ViewHolder and passing the object of type view

            return vhItem; // Returning the created object

            //inflate your layout and pass it to view holder

        } else if (viewType == TYPE_HEADER) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_drawer, parent, false); //Inflating the layout

            ViewHolder vhHeader = new ViewHolder(v, viewType); //Creating ViewHolder and passing the object of type view

            return vhHeader; //returning the object created


        }else if (viewType == TYPE_LINE) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_drawer, parent, false);
            //Inflating the layout

            ViewHolder vhHeader = new ViewHolder(v, viewType); //Creating ViewHolder and passing the object of type view

            return vhHeader; //returning the object created


        }

        return null;

    }

    //Next we override a method which is called when the item in a row is needed to be displayed, here the int position
    // Tells us item at which position is being constructed to be displayed and the holder id of the holder object tell us
    // which view type is being created 1 for item row
    @Override
    public void onBindViewHolder(MyAdapter.ViewHolder holder, int position) {

        if (holder.Holderid == 1) {                              // as the list view is going to be called after the header view so we decrement the
            // position by 1 and pass it to the holder while setting the text and image
            holder.textView.setText(mNavTitles[position - 1]); // Setting the Text with the array of our Titles
            holder.imageView.setImageResource(mIcons[position - 1]);// Setting the image with array of our icons
            if(position==m_flag)
            {
                holder.itemLayout.setBackgroundColor(m_activity.getResources().getColor(R.color.navigation_selected));
                holder.textView.setBackgroundColor(m_activity.getResources().getColor(R.color.navigation_selected));
            }
        } else if (holder.Holderid == 0) {

            //holder.profile.setImageResource(profile);           // Similarly we set the resources for header view
            holder.Name.setText(name);
            holder.email.setText(email);
        }

        if (holder.Holderid == 6) {

            //holder.profile.setImageResource(profile);           // Similarly we set the resources for header view


        }
    }

    // This method returns the number of items present in the list
    @Override
    public int getItemCount() {
        return mNavTitles.length + 1; // the number of items in the list will be +1 the titles including the header view.
    }


    // Witht the following method we check what type of view is being passed
    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;
        if (position == 6) {
            Log.e("line", "woking1");
            return TYPE_LINE;
        }
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }
}
