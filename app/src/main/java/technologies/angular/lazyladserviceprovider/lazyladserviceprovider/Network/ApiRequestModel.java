package technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sakshigupta on 09/02/16.
 */
public class ApiRequestModel {

    public class User {
        @SerializedName("servProv_email")
        public String email;
        @SerializedName("servProv_password")
        public String password;
        @SerializedName("servProv_regId")
        public String regId;

        public User(String email, String password, String regId) {
            this.email = email;
            this.password = password;
            this.regId = regId;
        }
    }

    public class InputForMain {
        @SerializedName("sp_code")
        public String spCode;

        public InputForMain(String spCode) {
            this.spCode = spCode;
        }
    }

    public class InputForCurrentOrders {
        @SerializedName("servProv_id")
        public String servId;
        @SerializedName("currentOrder_code")
        public String orderCode;

        public InputForCurrentOrders(String servProvId, String orderCode) {
            this.servId = servProvId;
            this.orderCode = orderCode;
        }
    }

    public class InputForConfirmCurrentOrders {
        @SerializedName("JsonDataArray")
        public String data;
        @SerializedName("current_order_code")
        public String orderCode;
        @SerializedName("user_code")
        public String userCode;
        @SerializedName("serv_prov_id")
        public String servId;

        public InputForConfirmCurrentOrders(String data, String orderCode, String userCode, String servProvId) {
            this.data = data;
            this.orderCode = orderCode;
            this.userCode = userCode;
            this.servId = servProvId;
        }
    }

    public class InputForPendingOrders {
        @SerializedName("servProv_id")
        public String servId;
        @SerializedName("pendingOrder_code")
        public String orderCode;

        public InputForPendingOrders(String servProvId, String orderCode) {
            this.servId = servProvId;
            this.orderCode = orderCode;
        }
    }

    public class InputForConfirmPendingOrders {
        @SerializedName("pending_order_code")
        public String orderCode;
        @SerializedName("user_code")
        public String userCode;

        public InputForConfirmPendingOrders(String orderCode, String userCode) {
            this.orderCode = orderCode;
            this.userCode = userCode;
        }
    }

    public class InputForOrders {
        @SerializedName("servProv_id")
        public String servId;
        @SerializedName("before_order_code")
        public String beforeCode;

        public InputForOrders(String servProvId, String beforeOrderCode) {
            this.servId = servProvId;
            this.beforeCode = beforeOrderCode;
        }
    }

    public class InputForDeliveredOrders {
        @SerializedName("servProv_id")
        public String servId;
        @SerializedName("deliveredOrder_code")
        public String orderCode;

        public InputForDeliveredOrders(String servProvId, String orderCode) {
            this.servId = servProvId;
            this.orderCode = orderCode;
        }
    }

    public class save {
        @SerializedName("JsonDataArray")
        public String jsonData;
        @SerializedName("sp_code")
        public String spCode;
        @SerializedName("sc_code")
        public String scCode;

        public save(String JsonData, String spCode, String scCode) {
            this.jsonData = JsonData;
            this.spCode = spCode;
            this.scCode = scCode;
        }
    }

    public class AccountsInput {
        @SerializedName("user_code")
        public String user_code;
        @SerializedName("owner_type")
        public int owner_type;
        @SerializedName("previous_tid")
        public String previous_tid;
    }

    public class DetailsInput {
        @SerializedName("account_name")
        public String accountName;
        @SerializedName("account_number")
        public String accountNo;
        @SerializedName("ifsc_code")
        public String ifscCode;
        @SerializedName("branch_details")
        public String branchAdd;
        @SerializedName("seller_id")
        public String spCode;
        public ShopLocation shop_location;

        public DetailsInput(String accountName, String accountNo, String ifscCode, String branchAdd, String spCode, Double latitude, Double longitude, String city_code) {
            this.accountName = accountName;
            this.accountNo = accountNo;
            this.ifscCode = ifscCode;
            this.branchAdd = branchAdd;
            this.spCode = spCode;
            shop_location = new ShopLocation(latitude, longitude, city_code);
        }

        public class ShopLocation {
            public String lat;
            @SerializedName("long")
            public String longi;
            public String city_code;

            public ShopLocation(Double latitude, Double longitide, String city_code) {
                this.lat = String.valueOf(latitude);
                this.longi = String.valueOf(longitide);
                this.city_code = city_code;
            }
        }
    }

    public class Delivered {
        @SerializedName("servProv_id")
        public String servId;
        @SerializedName("date")
        public String date;
        @SerializedName("day")
        public String day;

        public Delivered(String servProvId, String date, String day) {
            this.servId = servProvId;
            this.date = date;
            this.day = day;
        }
    }

    public class Rider {
        @SerializedName("order_code")
        public String orderCode;

        public Rider(String orderCode) {
            this.orderCode = orderCode;
        }
    }

    public class ReferralInput {
        @SerializedName("user_code")
        public String user_code;
        @SerializedName("owner_type")
        public String owner_type;

        public ReferralInput(String userCode, String ownerType) {
            this.user_code = userCode;
            this.owner_type = ownerType;
        }
    }

    public class Suggestion {
        @SerializedName("seller_phone")
        public String number;
        @SerializedName("comment_text")
        public String comment;

        public Suggestion(String number, String comment) {
            this.number = number;
            this.comment = comment;
        }
    }

    public class Input {
        @SerializedName("seller_phone")
        public String number;

        public Input(String param) {
            this.number = param;
        }
    }

    public class Register {
        @SerializedName("seller_name")
        public String name;
        @SerializedName("seller_phone")
        public String phone;
        @SerializedName("seller_phone_validated")
        public String validation;
        @SerializedName("password")
        public String password;
        @SerializedName("shop_name")
        public String shopName;
        @SerializedName("shop_address")
        public String address;
        @SerializedName("shop_city")
        public String city;
        @SerializedName("shop_area")
        public String area;
        @SerializedName("shop_type")
        public String type;
        @SerializedName("shop_open_time")
        public String open;
        @SerializedName("shop_close_time")
        public String close;
        @SerializedName("delivery_time")
        public String time;
        @SerializedName("min_delivery_amt")
        public String amount;
        @SerializedName("shop_open_bits")
        public String bits;
        @SerializedName("inventory_by_us")
        public String inventory;
        @SerializedName("JsonServiceAreasArray")
        public String code;
        @SerializedName("logistics_services")
        public String logistics;
        @SerializedName("sodexo_coupons")
        public String sodexo;
        @SerializedName("midnight_delivery")
        public String midnight;
        @SerializedName("referral_code")
        public String referral;
        public String verification_source;

        public Register(String name, String phone, String validation, String password, String shopName, String address, String city, String area, String type, String open, String close, String time, String amount, String bits, String inventory, String code, String logistics, String sodexo, String midnight, String referral, String verificationSource) {
            this.name = name;
            this.phone = phone;
            this.validation = validation;
            this.password = password;
            this.shopName = shopName;
            this.address = address;
            this.city = city;
            this.area = area;
            this.type = type;
            this.open = open;
            this.close = close;
            this.time = time;
            this.amount = amount;
            this.bits = bits;
            this.inventory = inventory;
            this.code = code;
            this.logistics = logistics;
            this.sodexo = sodexo;
            this.midnight = midnight;
            this.referral = referral;
            this.verification_source = verificationSource;
        }
    }

    public class saveInSearch {
        @SerializedName("JsonDataArray")
        public String jsonData;
        @SerializedName("sp_code")
        public String spCode;

        public saveInSearch(String JsonData, String spCode) {
            this.jsonData = JsonData;
            this.spCode = spCode;
        }
    }

    public class settleInput {
        @SerializedName("t_type")
        public String t_type;
        @SerializedName("user_code")
        public String user_code;
        @SerializedName("amount")
        public double amount;

        public settleInput(String tID, String userCode, Double amt) {
            this.user_code = userCode;
            this.amount = amt;
            this.t_type = tID;
        }
    }

    public class WalletInput {
        @SerializedName("user_code")
        public String user_code;
        @SerializedName("owner_type")
        public String owner_type;

        public WalletInput(String userCode, String ownerType) {
            this.user_code = userCode;
            this.owner_type = ownerType;
        }
    }

    public class RequestBalanceInput {
        @SerializedName("sp_code")
        public String sp_code;
        @SerializedName("wallet")
        public int wallet;
        @SerializedName("coupons")
        public int coupons;
        @SerializedName("pricediff")
        public int pricediff;
        public RequestBalanceInput(String sp_code, int wallet ,int coupons ,int pricediff ) {
            this.sp_code = sp_code;
            this.wallet = wallet;
            this.coupons=coupons;
            this.pricediff=pricediff;
        }
    }

    public class RequestPaymentInput {
        @SerializedName("sp_code")
        public String sp_code;
        @SerializedName("wallet")
        public int wallet;
        @SerializedName("coupons")
        public int coupons;
        @SerializedName("pricediff")
        public int pricediff;
        public RequestPaymentInput(String sp_code, int wallet ,int coupons ,int pricediff ) {
            this.sp_code = sp_code;
            this.wallet = wallet;
            this.coupons=coupons;
            this.pricediff=pricediff;
        }
    }


    public class CallUsInput {
        @SerializedName("sp_code")
        public String sp_code;

        public CallUsInput(String sp_code) {
            this.sp_code = sp_code;
        }
    }

    public class LoadOrderRequestModel{
        public String order_code;
        public String user_code;
    }

}
