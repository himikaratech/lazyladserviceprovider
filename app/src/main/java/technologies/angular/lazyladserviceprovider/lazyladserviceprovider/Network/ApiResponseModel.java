package technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.AccountSnippet;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.CurrentOrderDetailsSnippet;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.CurrentOrdersSnippet;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.DeliveredOrderDetailsSnippet;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.DeliveredOrdersSnippet;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.PendingOrderDetailsSnippet;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.PendingOrdersSnippet;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.ScheduledOrdersSnippet;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.SearchResultsSnippet;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.ServiceCategoryListSnippet;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Snippets.TransactionDetails;

/**
 * Created by sakshigupta on 09/02/16.
 */
public class ApiResponseModel {

    public class AccountsOutput {
        public boolean success;
        public String message;
        @SerializedName("transactions")
        public ArrayList<AccountSnippet> transactionsList = new ArrayList<AccountSnippet>();
    }

    public class ProfileModel {
        public String error;
        public boolean success;
        public String seller_name;
        public String seller_number;
        @SerializedName("account_details")
        public ProfileDetails profile;

        public class ProfileDetails {
            public String account_name;
            public String account_number;
            public String ifsc_code;
            public String branch_details;
        }
    }

    public class DetailsOutput {
        public boolean success;
        public String message;
    }

    public class ResponseSave {
        public int error;
    }

    public class Main {
        public int error;
        @SerializedName("service_categories")
        public ArrayList<ServiceCategoryListSnippet> mainActivtyList = new ArrayList<ServiceCategoryListSnippet>();
    }

    public class CurrentOrderDetailsModel {
        public int error;
        @SerializedName("current_order_details_service_providers")
        public ArrayList<CurrentOrderDetailsSnippet> currentOrderDetailsList = new ArrayList<CurrentOrderDetailsSnippet>();
    }

    public class PendingOrderDetailsModel {
        public int error;
        @SerializedName("pending_order_details_service_providers")
        public ArrayList<PendingOrderDetailsSnippet> pendingOrderDetailsList = new ArrayList<PendingOrderDetailsSnippet>();
    }

    public class DeliveredOrderDetailsModel {
        public int error;
        @SerializedName("delivered_order_details_service_providers")
        public ArrayList<DeliveredOrderDetailsSnippet> deliveredOrderDetailsList = new ArrayList<DeliveredOrderDetailsSnippet>();
    }

    public class ConfirmCurrentOrderDetailsModel {
        public int error;
    }

    public class ConfirmPendingOrderDetailsModel {
        public int error;
    }

    public class PendingOrdersModel {
        public int error;
        @SerializedName("pending_orders_service_providers")
        public ArrayList<PendingOrdersSnippet> pendingOrdersList = new ArrayList<PendingOrdersSnippet>();
    }

    public class CurrentOrdersModel {
        public int error;
        @SerializedName("current_orders_service_providers")
        public ArrayList<CurrentOrdersSnippet> currentOrdersList = new ArrayList<CurrentOrdersSnippet>();
    }

    public class ScheduledOrdersModel {
        public int error;
        @SerializedName("scheduled_orders_service_providers")
        public ArrayList<ScheduledOrdersSnippet> scheduledOrdersList = new ArrayList<ScheduledOrdersSnippet>();
    }

    public class DeliveredOrdersModel {
        public int error;
        @SerializedName("delivered_orders_service_providers")
        public ArrayList<DeliveredOrdersSnippet> deliveredOrdersList = new ArrayList<DeliveredOrdersSnippet>();
    }

    public class RiderResponse {
        public boolean success;
    }

    public class ReferralOutput {
        public boolean success;
        public int error_code;
        public String message;
        public String referral_code;
    }

    public class ResponseSuggestion {
        public int error;
    }

    public class ResponseAvailability {
        @SerializedName("availability_code")
        public int code;
        public int error;
    }

    public class ResponseRegister {
        @SerializedName("register_code")
        public int code;
        public int error;
        @SerializedName("sql_error")
        public String sql_error;
    }

    public class SearchModel {
        public int error;
        public ArrayList<SearchResultsSnippet> items_service_providers;
    }

    public class ResponseSaveInSearch {
        public int error;
    }

    public class settleOutput {
        public boolean success;
        public int error_code;
        public String message;
        public String t_id;
        public double amount;
    }

    public class WalletOutput {
        @SerializedName("success")
        public boolean success;
        @SerializedName("error_code")
        public int error_code;
        @SerializedName("message")
        public String message;
        @SerializedName("wallet")
        public WalletDetail wallet;

        public class WalletDetail {
            public int w_id;
            public String owner_id;
            public int owner_type;
            @SerializedName("balance")
            public Double balance;
        }
    }

    public class ResponseDataLogin {
        public int error;
        @SerializedName("servProv_code")
        public String code;
        @SerializedName("servProv_type")
        public String type;
    }

    public class ResponseDataBalance {
        public int error;
        public boolean success;
        public double pricediff_amount;
        public double total_amount;
        public double wallet_amount;
        public double coupons_amount;


        public int charge_order_nos;
        public double charge_amount;
        public double charge_tax_amount;
        public double total_charge_amount;
        public String charge_last_settled;
    }



    public class ResponseDataPayment {
        public int error;
        public boolean success;
    }

    public class ResponseDataCallUs {
        public int error;
        public boolean success;
        public String customer_care;
    }

    public class OrderFinished {
        public boolean success;
        public TransactionDetails transaction_details ;


    }

    public class LoadOrderResponseModel{
        public int error;
        public OrderPaymentDetails order_payment_details;


    }
    public class OrderPaymentDetails {
        public String to_be_paid;
        public String paid;

    }
}
