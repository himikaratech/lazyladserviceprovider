package technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.AreaModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.CityModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.InventoryModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.ItemModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.ServiceModel;

/**
 * Created by sakshigupta on 09/02/16.
 */
public interface ApiSuggestionService {

    @FormUrlEncoded
    @POST("/newserver/account/getAllTransactions")
    void accountTransactions(@Field("user_code") String user_code, @Field("owner_type") int owner_type,
                             @Field("previous_tid") String previous_tid, Callback<ApiResponseModel.AccountsOutput> cb);

    @GET("/newserver/seller/seller_profile/")
    void getProfileDetails(@Query("seller_id") String seller_id, Callback<ApiResponseModel.ProfileModel> callback);

    @POST("/newserver/seller/seller_profile")
    void bankDetails(@Body ApiRequestModel.DetailsInput input, Callback<ApiResponseModel.DetailsOutput> callback);

    @GET("/task_manager/v1/{url}/{sp_code}/{sc_code}")
    void avail(@Path("url") String url, @Path("sp_code") String m_spCode, @Path("sc_code") String m_scCode,
               @Query("previous_item_code") String previousItemCode,
               Callback<ItemModel> callback);

    @POST("/confirmItemsServiceProviderGSON")
    void save(@Body ApiRequestModel.save saveAndUpdate, Callback<ApiResponseModel.ResponseSave> callback);

    @POST("/signInServProvAndGetType")
    void login(@Body ApiRequestModel.User user, Callback<ApiResponseModel.ResponseDataLogin> callback);

    @POST("/serviceCategoriesOfferedBySP")
    void mainActivity(@Body ApiRequestModel.InputForMain main, Callback<ApiResponseModel.Main> callback);

    @POST("/currentOrdersDetails")
    void currentOrdersDetails(@Body ApiRequestModel.InputForCurrentOrders currentDetails, Callback<ApiResponseModel.CurrentOrderDetailsModel> callback);

    @POST("/pendingOrdersDetails")
    void pendingOrdersDetails(@Body ApiRequestModel.InputForPendingOrders pendingDetails, Callback<ApiResponseModel.PendingOrderDetailsModel> callback);

    @POST("/deliveredOrdersDetails")
    void deliveredOrdersDetails(@Body ApiRequestModel.InputForDeliveredOrders deliveredDetails, Callback<ApiResponseModel.DeliveredOrderDetailsModel> callback);

    @POST("/confirmCurrentOrderItemDetails_v2")
    void confirmCurrentOrdersDetails(@Body ApiRequestModel.InputForConfirmCurrentOrders confirmCurrentDetails, Callback<ApiResponseModel.ConfirmCurrentOrderDetailsModel> callback);

    @POST("/confirmPendingOrders")
    void confirmPendingOrdersDetails(@Body ApiRequestModel.InputForConfirmPendingOrders confirmPendingDetails, Callback<ApiResponseModel.ConfirmPendingOrderDetailsModel> callback);

    @POST("/currentOrdersLimited_v2")
    void currentOrders(@Body ApiRequestModel.InputForOrders current, Callback<ApiResponseModel.CurrentOrdersModel> callback);

    @POST("/pendingOrdersLimited_v2")
    void pendingOrders(@Body ApiRequestModel.InputForOrders pending, Callback<ApiResponseModel.PendingOrdersModel> callback);

    @POST("/scheduledOrdersLimited_v2")
    void scheduledOrders(@Body ApiRequestModel.InputForOrders scheduled, Callback<ApiResponseModel.ScheduledOrdersModel> callback);

    @POST("/DeliveredOrdersforDayDate")
    void deliveredOrders(@Body ApiRequestModel.Delivered delivered, Callback<ApiResponseModel.DeliveredOrdersModel> callback);

    @POST("/newserver/logistics/create_order")
    void riderRequest(@Body ApiRequestModel.Rider rider, Callback<ApiResponseModel.RiderResponse> callback);

    @POST("/getReferralCodeForOwner")
    void referralCode(@Body ApiRequestModel.ReferralInput input, Callback<ApiResponseModel.ReferralOutput> callback);

    @GET("/getCitiesforSeller")
    void cities(Callback<CityModel> callback);

    @GET("/getServiceTypesforSeller")
    void serviceTypes(Callback<ServiceModel> callback);

    @GET("/getAreasSeller/{cityCode}")
    void areas(@Path("cityCode") String cityCode, Callback<AreaModel> callback);

    @GET("/getBasicInventoryforShopType/{shopTypeCode}")
    void shopType(@Path("shopTypeCode") String shopTypeCode, Callback<InventoryModel> callback);

    @POST("/checkAvailabilityServProv")
    void availability(@Body ApiRequestModel.Input input, Callback<ApiResponseModel.ResponseAvailability> callback);

    @GET("/getAreasforSeller/{cityStr}/{areaStr}")
    void areasForSeller(@Path("cityStr") String cityStr, @Path("areaStr") String areaStr,
                        Callback<AreaModel> callback);

    @POST("/postCompleteRegistrationDetails_v5")
    void finalRegister(@Body ApiRequestModel.Register register, Callback<ApiResponseModel.ResponseRegister> callback);

    @POST("/commentOnSellerRegister")
    void suggestion(@Body ApiRequestModel.Suggestion suggestion, Callback<ApiResponseModel.ResponseSuggestion> callback);

    @GET("/searchItemsSrvProvLimited/{serv_prov_code}/{serv_type_code}/{item_type}")
    void searchFeeds(@Path("serv_prov_code") String serv_prov_code, @Path("serv_type_code") String serv_type_code,
                     @Path("item_type") int item_type,
                     @Query("search_string") String searchString, @Query("previous_item_code") String previousItemCode,
                     Callback<ApiResponseModel.SearchModel> cb);

    @POST("/confirmItemsServiceProviderInSearchGSON")
    void save(@Body ApiRequestModel.saveInSearch saveAndUpdate, Callback<ApiResponseModel.ResponseSaveInSearch> callback);

    @POST("/makeTransaction")
    void settle(@Body ApiRequestModel.settleInput input, Callback<ApiResponseModel.settleOutput> callback);

    @POST("/getWalletDetails")
    void walletDetails(@Body ApiRequestModel.WalletInput input, Callback<ApiResponseModel.WalletOutput> callback);

    @POST("/newserver/revenue/RequestBalance")
    void requestBalance(@Body ApiRequestModel.RequestBalanceInput input, Callback<ApiResponseModel.ResponseDataBalance> callback);

    @POST("/newserver/revenue/RequestSettlement")
    void requestPayment(@Body ApiRequestModel.RequestPaymentInput input, Callback<ApiResponseModel.ResponseDataPayment> callback);

    @POST("/newserver/seller/seller_call_us")
    void callUs(@Body ApiRequestModel.CallUsInput input, Callback<ApiResponseModel.ResponseDataCallUs> callback);

    @GET("/newserver/account2/order_finish_seller_payment_details")
    void orderFinish(@Query("t_id") String t_id, Callback<ApiResponseModel.OrderFinished> callback);

    @POST("/task_manager/v1/getOrderDetailsUser")
    void getOrderDetails(@Body ApiRequestModel.LoadOrderRequestModel requestModel, Callback<ApiResponseModel.LoadOrderResponseModel> cb);


}
