package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiResponseModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiSuggestionService;

/**
 * Created by Amud on 08/03/16.
 */


public class OrderFinishedDetails extends AppCompatActivity {



    TextView m_lazyladChargesTextView;
    TextView m_discountTextView;
    TextView m_deliveryChargesTextView;
    TextView m_priceDifferenceTextView;

    TextView m_walletTextView;
    String m_tId;






    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_finished_details_layout);
        m_tId=getIntent().getStringExtra(LazyCosntant.T_ID);
        Log.d("m_tId",m_tId);




        m_lazyladChargesTextView = (TextView)findViewById(R.id.lazylad_charges_textView);
        m_discountTextView = (TextView)findViewById(R.id.discount_textView);
        m_deliveryChargesTextView = (TextView)findViewById(R.id.delivery_charges_textView);
        m_priceDifferenceTextView = (TextView)findViewById(R.id.price_difference_textView);
        m_walletTextView = (TextView)findViewById(R.id.wallet_textView);


        getPaymentDetails();

    }

    private void getPaymentDetails() {



        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(OrderFinishedDetails.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://www.angulartechnologies.com").setLogLevel(RestAdapter.LogLevel.FULL).build();
        ApiSuggestionService api = restAdapter.create(ApiSuggestionService.class);
        String param = m_tId;
        api.orderFinish(param, new Callback<ApiResponseModel.OrderFinished>() {
            @Override
            public void success(ApiResponseModel.OrderFinished orderFinished, retrofit.client.Response response) {
               if(orderFinished.success)
               {
                    m_lazyladChargesTextView.setText(String.valueOf(orderFinished.transaction_details.lazylad_charges));
                    m_discountTextView.setText(String.valueOf(orderFinished.transaction_details.discount));
                    m_deliveryChargesTextView.setText(String.valueOf(orderFinished.transaction_details.delivery_charges));
                    m_priceDifferenceTextView.setText(String.valueOf(orderFinished.transaction_details.price_difference));
                   m_walletTextView.setText(String.valueOf(orderFinished.transaction_details.wallet));
               }

                progressDialog.dismiss();
            }

            @Override
            public void failure(final RetrofitError error) {
                progressDialog.dismiss();
            }
        });


    }


    @Override
    public void onBackPressed()
    {

        super.onBackPressed();  // optional depending on your needs
        this.finish();
    }


}
