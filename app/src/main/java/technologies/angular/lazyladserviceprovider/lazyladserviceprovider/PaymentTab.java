package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiRequestModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiResponseModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiSuggestionService;

/**
 * Created by Amud on 17/02/16.
 */

public class PaymentTab extends Fragment {

    CheckBox m_priceCheckBox;
    CheckBox m_walletCheckBox;

    CheckBox m_discountCheckBox;
    Button m_getPayment;


    TextView m_priceDiffTV;
    TextView m_discountAmountTV;
    TextView m_walletAmountTV;
    TextView m_totalTV;
    private String m_spCode;
    int m_priceDiffChecked;
    int m_discountChecked;
    int m_walletChecked;

    double m_totalGetPaymentAmount;


    Double m_couponAmount;
    Double m_priceDiffAmount;
    Double m_walletAmount;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTracker();
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.payment_layout, container, false);

        if (!Utils.isNetworkAvailable(getActivity())) {
            Toast.makeText(getActivity(), "Please Check Your Network Connectivity", Toast.LENGTH_SHORT).show();
        }
        m_priceDiffChecked = 0;
        m_discountChecked = 0;
        m_walletChecked = 0;

        m_couponAmount = 0.0;
        m_priceDiffAmount = 0.0;
        m_walletAmount = 0.0;

        m_totalGetPaymentAmount = 0.0;

        m_priceCheckBox = (CheckBox) v.findViewById(R.id.price_checkbox);
        m_discountCheckBox = (CheckBox) v.findViewById(R.id.discount_checkbox);
        m_walletCheckBox = (CheckBox) v.findViewById(R.id.wallet_checkbox);
        m_getPayment = (Button) v.findViewById(R.id.get_payment);
        m_totalTV=(TextView)v.findViewById(R.id.total);

        m_priceDiffTV = (TextView) v.findViewById(R.id.price_diff_amount);
        m_walletAmountTV = (TextView) v.findViewById(R.id.wallet_amount);
        m_discountAmountTV = (TextView) v.findViewById(R.id.discount_amount);

        m_priceCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    m_totalGetPaymentAmount = m_totalGetPaymentAmount + m_priceDiffAmount;
                    m_priceDiffChecked = 1;
                } else {
                    m_totalGetPaymentAmount = m_totalGetPaymentAmount - m_priceDiffAmount;
                    m_priceDiffChecked = 0;
                }

            }
        });

        m_walletCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    m_totalGetPaymentAmount = m_totalGetPaymentAmount + m_walletAmount;
                    m_walletChecked = 1;
                } else {
                    m_totalGetPaymentAmount = m_totalGetPaymentAmount - m_walletAmount;
                    m_walletChecked = 0;
                }

            }
        });


        m_discountCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    m_totalGetPaymentAmount = m_totalGetPaymentAmount + m_couponAmount;
                    m_discountChecked = 1;
                } else {
                    m_totalGetPaymentAmount = m_totalGetPaymentAmount - m_couponAmount;
                    m_discountChecked = 0;
                }

            }
        });


        getPaymentDetails();


        m_getPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), RequestPaymentClass.class);


                Log.d("m_discountChecked1",m_discountChecked+"");
                Log.d("m_priceDiffChecked1",m_priceDiffChecked+"");
                Log.d("m_walletChecked1",m_walletChecked+"");
                Log.d("m_discountChecked1",m_discountChecked+"");




                intent.putExtra(LazyCosntant.DISCOUNT_CONSTANT, m_discountChecked);
                intent.putExtra(LazyCosntant.PRICE_DIFFERENCE_CONSTANT, m_priceDiffChecked);
                intent.putExtra(LazyCosntant.WALLET_PAYMENT_CONSTANT, m_walletChecked);
                intent.putExtra(LazyCosntant.REQUESTED_AMOUNT_CONSTANT,m_totalGetPaymentAmount);

                getActivity().startActivity(intent);

            }
        });
        return v;
    }

    private void getPaymentDetails() {


        Cursor m_cursor = getActivity().getContentResolver().query(
                LazyLadServiceProviderContract.UserDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    m_spCode = m_cursor.getString(1);
                    m_cursor.moveToNext();
                }
            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }
        if (m_cursor != null) m_cursor.close();

        final ProgressDialog pDialog;
        pDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
        pDialog.setMessage("Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        pDialog.show();

        String user_code = m_spCode;
        String owner_type = "1";


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://www.angulartechnologies.com").build();


        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
        ApiRequestModel requestModel = new ApiRequestModel();
        ApiRequestModel.RequestBalanceInput input = requestModel.new RequestBalanceInput(m_spCode, 1, 1, 1);
        apiSuggestionsService.requestBalance(input, new Callback<ApiResponseModel.ResponseDataBalance>() {

            @Override
            public void success(ApiResponseModel.ResponseDataBalance output, retrofit.client.Response response) {
                boolean success = output.success;
                if (success == true) {


                    m_couponAmount = output.coupons_amount;
                    m_priceDiffAmount = output.pricediff_amount;
                    m_walletAmount = output.wallet_amount;

                    m_priceDiffTV.setText("₹ " + String.valueOf(m_priceDiffAmount));
                    m_discountAmountTV.setText("₹ " + String.valueOf(m_couponAmount));
                    m_walletAmountTV.setText("₹ " + String.valueOf(m_walletAmount));
                    m_totalTV.setText("₹ " + String.valueOf(m_priceDiffAmount+m_couponAmount+m_walletAmount));
                }
                if (pDialog.isShowing())
                    pDialog.dismiss();
            }

            @Override
            public void failure(final RetrofitError error) {
                Log.e("Failure", error + " retro");
                if (pDialog.isShowing())
                    pDialog.dismiss();
            }
        });
    }


    @Override
    public void onPause() {
         m_priceCheckBox.setChecked(false);
         m_walletCheckBox.setChecked(false);
         m_discountCheckBox.setChecked(false);

        super.onPause();  // Always call the superclass method first

    }

    private void setTracker() {
        Tracker t = ((SessionManager) getActivity().getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(this.getClass().getCanonicalName());

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }

}
