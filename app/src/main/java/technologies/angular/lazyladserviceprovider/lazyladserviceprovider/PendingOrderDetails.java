package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.UserDetails;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiRequestModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiResponseModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiSuggestionService;

public class PendingOrderDetails extends AppCompatActivity {

    private static final String TAG = PendingOrderDetails.class.getSimpleName();
    private String m_servProvId;
    private String m_pendingOrderCode;
    private String m_userCode;
    Toolbar toolbar;

    private TextView m_to_be_paid;
    private TextView m_already_paid_online;

    private ArrayList<PendingOrderDetailsSnippet> m_pendingOrderDetailsList;
    private ListView m_pendingOrderListView;
    private PendingOrderDetailsAdapter m_pendingOrderDetailsAdapter;

    private Button m_confirmButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pending_order_details);

        if (!Utils.isNetworkAvailable(this)) {
            Toast.makeText(this, "Please Check Your Network Connectivity", Toast.LENGTH_SHORT).show();
        }

        m_pendingOrderListView = (ListView) findViewById(R.id.pending_order_details_listview);
        m_confirmButton = (Button) findViewById(R.id.confirm_pending_order_items_button);

        m_confirmButton.setVisibility(View.INVISIBLE);

        Intent intent = getIntent();
        String data[] = intent.getStringArrayExtra("data_send_pending_details");
        m_pendingOrderCode = data[0];
        m_userCode = data[1];

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Confirmed Order:" + " " + m_pendingOrderCode);

        Cursor m_cursor = getContentResolver().query(UserDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    m_servProvId = m_cursor.getString(1);
                    m_cursor.moveToNext();
                }
            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }

        LoadPendingOrderDetails();

        m_already_paid_online = (TextView) findViewById(R.id.already_paid_online);
        m_to_be_paid = (TextView) findViewById(R.id.to_be_paid);

        loadOrderDetails();

        m_confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfirmPendingOrder();
            }
        });
        setTracker();
    }

    private void ConfirmPendingOrder() {

        final ProgressDialog pDialog;

        pDialog = new ProgressDialog(PendingOrderDetails.this);
        pDialog.setMessage("Loading Pending Order Details. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Utils.RestApiPathUrl).build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
        ApiRequestModel requestModel = new ApiRequestModel();
        ApiRequestModel.InputForConfirmPendingOrders confirmPending = requestModel.new InputForConfirmPendingOrders(m_pendingOrderCode, m_userCode);
        apiSuggestionsService.confirmPendingOrdersDetails(confirmPending, new Callback<ApiResponseModel.ConfirmPendingOrderDetailsModel>() {
            @Override
            public void failure(final RetrofitError error) {
                pDialog.dismiss();
            }

            @Override
            public void success(ApiResponseModel.ConfirmPendingOrderDetailsModel responseConfirmPendingOrders, Response response) {
                pDialog.dismiss();
                onBackPressed();
            }
        });
    }

    private void LoadPendingOrderDetails() {

        final ProgressDialog pDialog;

        pDialog = new ProgressDialog(PendingOrderDetails.this);
        pDialog.setMessage("Loading Pending Order Details. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Utils.RestApiPathUrl).build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
        ApiRequestModel requestModel = new ApiRequestModel();
        ApiRequestModel.InputForPendingOrders pending = requestModel.new InputForPendingOrders(m_servProvId, m_pendingOrderCode);
        apiSuggestionsService.pendingOrdersDetails(pending, new Callback<ApiResponseModel.PendingOrderDetailsModel>() {

            @Override
            public void success(ApiResponseModel.PendingOrderDetailsModel responsePendingOrders, Response response) {
                int success = responsePendingOrders.error;
                if (success == 1) {
                    if (null != responsePendingOrders.pendingOrderDetailsList && !responsePendingOrders.pendingOrderDetailsList.isEmpty()) {

                        m_pendingOrderDetailsList = responsePendingOrders.pendingOrderDetailsList;
                        m_pendingOrderDetailsAdapter = new PendingOrderDetailsAdapter(PendingOrderDetails.this, m_pendingOrderDetailsList);
                        m_pendingOrderListView.setAdapter((ListAdapter) m_pendingOrderDetailsAdapter);
                        m_confirmButton.setVisibility(View.VISIBLE);

                    }
                } else {

                }
                pDialog.dismiss();
            }

            @Override
            public void failure(final RetrofitError error) {
                pDialog.dismiss();
            }
        });
    }


    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(this.getClass().getCanonicalName());

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    private void loadOrderDetails() {
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading Orders Details. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        RestAdapter restAdapter = Utils.providesRestAdapter("http://www.angulartechnologies.com");
        ApiRequestModel requestModel = new ApiRequestModel();
        final ApiRequestModel.LoadOrderRequestModel loadOrderRequestModel = requestModel.new LoadOrderRequestModel();
        loadOrderRequestModel.user_code = m_userCode;
        loadOrderRequestModel.order_code = m_pendingOrderCode;
        ApiSuggestionService apiSuggestionService = restAdapter.create(ApiSuggestionService.class);
        apiSuggestionService.getOrderDetails(loadOrderRequestModel, new Callback<ApiResponseModel.LoadOrderResponseModel>() {
            @Override
            public void success(ApiResponseModel.LoadOrderResponseModel loadOrderResponseModel, Response response) {
                int status = loadOrderResponseModel.error;
                ApiResponseModel.OrderPaymentDetails paymentDetails = loadOrderResponseModel.order_payment_details;
                if (status == 1) {
                    m_to_be_paid.setText(paymentDetails.to_be_paid);
                    m_already_paid_online.setText(paymentDetails.paid);
                    pDialog.dismiss();
                } else {
                    pDialog.dismiss();
                    Toast.makeText(PendingOrderDetails.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                pDialog.dismiss();
                Log.e(TAG, error.getMessage());
                Toast.makeText(PendingOrderDetails.this, getString(R.string.network_error), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
