package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Saurabh on 16/03/15.
 */
public class PendingOrderDetailsSnippet {

    public int m_itemId;
    @SerializedName("item_code")
    public String m_itemCode;
    @SerializedName("item_name")
    public String m_itemName;
    @SerializedName("item_img_flag")
    public int m_itemImgFlag;
    @SerializedName("item_img_address")
    public String m_itemImgAdd;
    @SerializedName("item_short_desc")
    public String m_itemShortDesc; //Weight like 20 gm wali dabi
    @SerializedName("item_quantity")
    public int m_itemQuantity;
    @SerializedName("item_cost")
    public double m_itemCost;   //total cost i.e. cost*quantity
    @SerializedName("item_desc")
    public String m_itemDesc;
    public int m_itemConfirmed;   //0 for not available (by default); 1 for confirming

    public PendingOrderDetailsSnippet() {
    }

    public PendingOrderDetailsSnippet(int itemId, String itemCode, String itemName, int itemImgFlag, String itemImgAdd,
                                      String itemShortDesc, int itemQuantity, double itemCost, String itemDesc) {
        m_itemId = itemId;
        m_itemCode = itemCode;
        m_itemName = itemName;
        m_itemImgFlag = itemImgFlag;
        m_itemImgAdd = itemImgAdd;
        m_itemShortDesc = itemShortDesc;
        m_itemQuantity = itemQuantity;
        m_itemCost = itemCost;
        m_itemDesc = itemDesc;
        m_itemConfirmed = 0;
    }
}
