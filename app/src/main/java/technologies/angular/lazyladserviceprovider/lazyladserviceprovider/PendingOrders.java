
package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiRequestModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiResponseModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiSuggestionService;

import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.UserDetails;

public class PendingOrders extends android.support.v4.app.Fragment {

    private Boolean dataLoading = false;

    private String m_servProvId;
    private ArrayList<PendingOrdersSnippet> m_pendingOrdersList = null;
    private PendingOrdersAdapter m_pendingOrdersAdapter = null;
    private ListView m_pendingOrdersDetails;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTracker();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.pending_orders, container, false);

        if (!Utils.isNetworkAvailable(getActivity())) {
            Toast.makeText(getActivity(), "Please Check Your Network Connectivity", Toast.LENGTH_SHORT).show();
        }
        Cursor m_cursor = getActivity().getContentResolver().query(UserDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    m_servProvId = m_cursor.getString(1);
                    m_cursor.moveToNext();
                }
            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }

        m_pendingOrdersDetails = (ListView) rootView.findViewById(R.id.pending_orders_list_view);

        m_pendingOrdersDetails.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) { // TODO Auto-generated method stub
                if (null == m_pendingOrdersDetails)
                    return;

                int threshold = 1;
                int count = m_pendingOrdersDetails.getCount();

                if (scrollState == SCROLL_STATE_IDLE) {
                    if (!dataLoading && m_pendingOrdersDetails.getLastVisiblePosition() >= count - threshold) {
                        dataLoading = true;
                        // Execute LoadMoreDataTask AsyncTask
                        LoadPendingOrders();
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                //leave this empty
            }
        });
        return rootView;
    }

    public void onStart() {
        super.onStart();
        m_pendingOrdersList = null;
        LoadPendingOrders();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void LoadPendingOrders() {
        final ProgressDialog pDialog;
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Loading Orders. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        String orderId;
        if (m_pendingOrdersList == null) {
            orderId = "0";
        } else {
            orderId = m_pendingOrdersList.get(m_pendingOrdersList.size() - 1).m_pendingOrderCode;
        }
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Utils.RestApiPathUrl).build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
        ApiRequestModel requestModel = new ApiRequestModel();
        ApiRequestModel.InputForOrders pending = requestModel.new InputForOrders(m_servProvId, orderId);
        apiSuggestionsService.pendingOrders(pending, new Callback<ApiResponseModel.PendingOrdersModel>() {

            @Override
            public void success(ApiResponseModel.PendingOrdersModel responsePendingOrders, Response response) {
                int success = responsePendingOrders.error;
                if (success == 1) {

                    if (null != responsePendingOrders.pendingOrdersList && !responsePendingOrders.pendingOrdersList.isEmpty()) {

                        if (m_pendingOrdersList == null) {

                            m_pendingOrdersList = responsePendingOrders.pendingOrdersList;
                            m_pendingOrdersAdapter = new PendingOrdersAdapter(getActivity(), m_pendingOrdersList);
                            m_pendingOrdersDetails.setAdapter((ListAdapter) m_pendingOrdersAdapter);
                        } else {
                            m_pendingOrdersList.addAll(responsePendingOrders.pendingOrdersList);
                            m_pendingOrdersAdapter.notifyDataSetChanged();
                        }
                    }
                } else {
                }
                pDialog.dismiss();
                dataLoading = false;
            }

            @Override
            public void failure(final RetrofitError error) {
                pDialog.dismiss();
                dataLoading = false;
            }
        });
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getActivity().getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(this.getClass().getCanonicalName());

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }
}
