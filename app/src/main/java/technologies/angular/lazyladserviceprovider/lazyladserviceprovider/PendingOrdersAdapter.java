package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiRequestModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiResponseModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiSuggestionService;

/**
 * Created by Saurabh on 26/02/15.
 */
public class PendingOrdersAdapter extends BaseAdapter {

    private Activity m_activity;
    private ArrayList<PendingOrdersSnippet> m_pendingOrdersSnippetArrayList;

    public class ViewHolder {
        TextView pendingOrderCodeTextView;
        TextView pendingOrderAddressTextView;
        TextView pendingOrderAmountTextView;
        TextView pendingOrderTimePlacedTextView;
        TextView pendingOrderUserNameTextView;
        TextView pendingOrderUserPhoneTextView;
        LinearLayout pendingOrderDetailView;
        LinearLayout numberLayoutView;
        LinearLayout riderLayoutView;
    }

    public PendingOrdersAdapter(Activity activity, ArrayList<PendingOrdersSnippet> pendingOrdersSnippetArrayList) {
        m_activity = activity;
        m_pendingOrdersSnippetArrayList = pendingOrdersSnippetArrayList;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (m_pendingOrdersSnippetArrayList != null) {
            count = m_pendingOrdersSnippetArrayList.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (m_pendingOrdersSnippetArrayList != null) {
            return m_pendingOrdersSnippetArrayList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_pendingOrdersSnippetArrayList != null) {
            return m_pendingOrdersSnippetArrayList.get(position).pendingOrdersId;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (m_pendingOrdersSnippetArrayList != null) {

            final ViewHolder holder;

            final String pendingOrderCode = ((PendingOrdersSnippet) getItem(position)).m_pendingOrderCode;
            String pendingOrderAddress = ((PendingOrdersSnippet) getItem(position)).m_address;
            int pendingOrderAmount = ((PendingOrdersSnippet) getItem(position)).m_amount;
            String pendingOrderTimePlaced = ((PendingOrdersSnippet) getItem(position)).m_timeOrderPlaced;
            String pendingOrderUserName = ((PendingOrdersSnippet) getItem(position)).m_userName;
            final String pendingOrderPhoneNumber = ((PendingOrdersSnippet) getItem(position)).m_userPhoneNum;
            final int riderRequested = ((PendingOrdersSnippet) getItem(position)).requestedRiderOrNot;
            final String userCode = ((PendingOrdersSnippet) getItem(position)).m_userCode;

            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.pending_orders_display_list, (ViewGroup) null);
                holder = new ViewHolder();
                holder.pendingOrderCodeTextView = (TextView) convertView.findViewById(R.id.pending_order_code_textview);
                holder.pendingOrderAddressTextView = (TextView) convertView.findViewById(R.id.pending_order_address_textview);
                holder.pendingOrderAmountTextView = (TextView) convertView.findViewById(R.id.amount_pending_order_textview);
                holder.pendingOrderTimePlacedTextView = (TextView) convertView.findViewById(R.id.pending_order_del_textview);
                holder.pendingOrderUserNameTextView = (TextView) convertView.findViewById(R.id.pending_order_username_textview);
                holder.pendingOrderUserPhoneTextView = (TextView) convertView.findViewById(R.id.pending_order_number_textview);
                holder.pendingOrderDetailView = (LinearLayout) convertView.findViewById(R.id.detail_layout);
                holder.numberLayoutView = (LinearLayout) convertView.findViewById(R.id.number_layout);
                holder.riderLayoutView = (LinearLayout) convertView.findViewById(R.id.rider_layout);
                convertView.setTag(holder);
            }
            else {
                holder = (ViewHolder) convertView.getTag();
            }
                if (riderRequested == 0) {
                    holder.riderLayoutView.setVisibility(View.VISIBLE);
                } else
                    holder.riderLayoutView.setVisibility(View.GONE);

                TextView pendingOrderNumber = (TextView) convertView.findViewById(R.id.order_number);
                TextView viewDetail = (TextView) convertView.findViewById(R.id.view_detail_textview);

                Typeface typeFace = Typeface.createFromAsset(m_activity.getAssets(), "fonts/Roboto_Medium.ttf");
                Typeface typeFace1 = Typeface.createFromAsset(m_activity.getAssets(), "fonts/Roboto_Regular.ttf");
                Typeface typeFace2 = Typeface.createFromAsset(m_activity.getAssets(), "fonts/Roboto_Light.ttf");

                pendingOrderNumber.setTypeface(typeFace);
            holder.pendingOrderCodeTextView.setTypeface(typeFace);
            holder.pendingOrderAmountTextView.setTypeface(typeFace1);
            holder.pendingOrderTimePlacedTextView.setTypeface(typeFace1);
            holder.pendingOrderUserNameTextView.setTypeface(typeFace);
            holder.pendingOrderAddressTextView.setTypeface(typeFace1);
            holder.pendingOrderUserPhoneTextView.setTypeface(typeFace2);
                viewDetail.setTypeface(typeFace2);

            holder.pendingOrderCodeTextView.setText(pendingOrderCode);
            holder.pendingOrderAddressTextView.setText(pendingOrderAddress);
            holder.pendingOrderAmountTextView.setText("₹ " + Integer.toString(pendingOrderAmount));
            holder.pendingOrderTimePlacedTextView.setText(pendingOrderTimePlaced);
            holder.pendingOrderUserNameTextView.setText(pendingOrderUserName);
            holder.pendingOrderUserPhoneTextView.setText(pendingOrderPhoneNumber);

            holder.numberLayoutView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent callIntent = new Intent(Intent.ACTION_DIAL);
                        callIntent.setData(Uri.parse("tel: " + pendingOrderPhoneNumber));
                        m_activity.startActivity(callIntent);
                    }
                });
            holder.pendingOrderDetailView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startPendingOrderDetilsActivity(pendingOrderCode, userCode);
                    }
                });

            holder.riderLayoutView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        RestAdapter restAdapter = new RestAdapter.Builder()
                                .setEndpoint("http://www.angulartechnologies.com").build();
                        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
                        ApiRequestModel requestModel = new ApiRequestModel();
                        ApiRequestModel.Rider rider = requestModel.new Rider(pendingOrderCode);
                        apiSuggestionsService.riderRequest(rider, new Callback<ApiResponseModel.RiderResponse>() {

                            @Override
                            public void success(ApiResponseModel.RiderResponse responseLogistics, Response response) {
                                boolean success = responseLogistics.success;
                                Log.i("Success", success + "");
                                if (success == true) {
                                    Toast.makeText(m_activity, "Request Sent", Toast.LENGTH_SHORT).show();
                                    holder.riderLayoutView.setVisibility(View.GONE);
                                    //do nothing
                                }
                            }
                            @Override
                            public void failure(final RetrofitError error) {
                                Log.i("Failure", error.getMessage());
                            }
                        });
                    }
                });
            }
            return convertView;
        }

    private void startPendingOrderDetilsActivity(String pendingOrderCode, String userCode) {
        Intent intent = new Intent(m_activity, PendingOrderDetails.class);
        String data[] = {pendingOrderCode, userCode};
        intent.putExtra("data_send_pending_details", data);
        m_activity.startActivity(intent);
    }
}
