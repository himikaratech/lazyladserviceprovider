package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Saurabh on 26/02/15.
 */
public class PendingOrdersSnippet {

    public int pendingOrdersId;
    @SerializedName("pending_order_code")
    public String m_pendingOrderCode;
    @SerializedName("delivery_address")
    public String m_address;
    @SerializedName("user_code")
    public String m_userCode;
    @SerializedName("order_time")
    public String m_timeOrderPlaced;
    @SerializedName("total_amount")
    public int m_amount;
    @SerializedName("user_name")
    public String m_userName;
    @SerializedName("user_phone_number")
    public String m_userPhoneNum;
    @SerializedName("logistics_used")
    public int requestedRiderOrNot;

    public PendingOrdersSnippet() {
    }

    public PendingOrdersSnippet(int id, String pendingOrderCode, String customer_address, String user_code, String time_order_placed, int total_amount, String userName, String userPhoneNum, int rider_requested) {
        pendingOrdersId = id;
        m_pendingOrderCode = pendingOrderCode;
        m_address = customer_address;
        m_userCode = user_code;
        m_timeOrderPlaced = time_order_placed;
        m_amount = total_amount;
        m_userName = userName;
        m_userPhoneNum = userPhoneNum;
        requestedRiderOrNot = rider_requested;
    }
}
