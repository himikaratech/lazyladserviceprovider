package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by user on 20-07-2015.
 */
public class PriceDifferenceActivity extends ActionBarActivity{

    private String m_spCode;
    private PriceDifferenceAdapter m_priceDifferenceAdapter;
    private ProgressDialog pDialog;
    private ListView m_priceDifference;
    ArrayList<PriceDifferenceSnippet> m_price;

    private Boolean dataLoading = false;
    private int orderID;
    private String transactionID;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.price_difference);

        Intent in = new Intent();
        orderID= in.getExtras().getInt("data_orderID");
        transactionID= in.getExtras().getString("data_transactionID");

        if(!Utils.isNetworkAvailable(this)){
            Toast.makeText(this, "Please Check Your Network Connectivity", Toast.LENGTH_SHORT).show();
        }

        Cursor m_cursor = getContentResolver().query(
                LazyLadServiceProviderContract.UserDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if(m_cursor!=null && m_cursor.moveToFirst()){
            if(m_cursor.getCount()!=0){
                for(int i=0; i<m_cursor.getCount(); i++){
                    m_spCode=m_cursor.getString(1);
                    m_cursor.moveToNext();
                }
                //m_addressTextView.setText(m_userAddress);
            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }
       
        if(m_cursor != null)m_cursor.close();

        m_priceDifference = (ListView)findViewById(R.id.price_difference_list_view);
        getReadyPriceDifferenceList();
    }

    private void getReadyPriceDifferenceList() {
        SessionManager m_sessionManager;
        m_sessionManager=new SessionManager(PriceDifferenceActivity.this);

        final ProgressDialog pDialog;

        pDialog = new ProgressDialog(PriceDifferenceActivity.this);
        pDialog.setMessage("Signing In. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        String url_items_serviceProvider = "http://www.angulartechnologies.com/task_manager/v1/priceDifference";
        String param=m_spCode;
        url_items_serviceProvider=url_items_serviceProvider + "/" + param + "/" + orderID + "/" + transactionID;

        GsonRequest<PriceDifferenceModel> jsObjRequest = new GsonRequest<PriceDifferenceModel>(Request.Method.POST,
                url_items_serviceProvider, PriceDifferenceModel.class, null, new Response.Listener<PriceDifferenceModel>() {
            @Override
            public void onResponse(PriceDifferenceModel response) {
                m_price=new ArrayList<PriceDifferenceSnippet>();
                int success =response.error;
                if(success == 1) {
                    m_price = response.price_difference;
                    m_priceDifferenceAdapter = new PriceDifferenceAdapter(PriceDifferenceActivity.this, m_price);
                    m_priceDifference.setAdapter((ListAdapter) m_priceDifferenceAdapter);
                    pDialog.dismiss();
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error even: ", error+" retro");
                pDialog.dismiss();

            }
        });
        m_sessionManager.addToRequestQueue(jsObjRequest);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (pDialog != null) {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
        }
    }
}