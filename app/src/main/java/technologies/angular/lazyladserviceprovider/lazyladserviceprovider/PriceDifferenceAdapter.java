package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by user on 20-07-2015.
 */
public class PriceDifferenceAdapter extends BaseAdapter {

    private ArrayList<PriceDifferenceSnippet> m_priceDiffDetails;
    private Activity m_activity;

    public PriceDifferenceAdapter(Activity activity, ArrayList<PriceDifferenceSnippet> itemsDetails) {
        m_activity = activity;
        m_priceDiffDetails = itemsDetails;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (m_priceDiffDetails != null) {
            count = m_priceDiffDetails.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (m_priceDiffDetails != null) {
            return m_priceDiffDetails.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_priceDiffDetails != null) {
            return m_priceDiffDetails.get(position).m_itemCode;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (m_priceDiffDetails != null) {
            final String itemImageAdd = ((PriceDifferenceSnippet) getItem(position)).m_itemImgAddress;
            final String itemName = ((PriceDifferenceSnippet) getItem(position)).m_itemName;
            final double itemCost = ((PriceDifferenceSnippet) getItem(position)).m_itemCost;
            final double itemCostNew = ((PriceDifferenceSnippet) getItem(position)).m_itemCostNew;
            int itemQuantitySelected = ((PriceDifferenceSnippet) getItem(position)).m_itemQuantitySelected;
            final String itemDesc = ((PriceDifferenceSnippet) getItem(position)).m_itemDesc;

            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.price_difference_display_list_view, (ViewGroup) null);
            }
            TextView itemNameTextView = (TextView) convertView.findViewById(R.id.price_difference_item_name);
            final TextView itemQuantitySelectedTextView = (TextView) convertView.findViewById(R.id.price_difference_item_quantity_selected);
            TextView price = (TextView) convertView.findViewById(R.id.price_difference_item_price);
            TextView priceNew = (TextView) convertView.findViewById(R.id.price_difference_new_price);
            TextView Description = (TextView) convertView.findViewById(R.id.price_difference_item_description);
            TextView netPrice = (TextView)convertView.findViewById(R.id.price_difference_total_price);

            Double netCost = (itemCost - itemCostNew);
            netPrice.setText(" ? " + netCost);
            Description.setText(itemDesc);
            ImageView imageView = (ImageView) convertView.findViewById(R.id.price_difference_item_image);
            Picasso.with(parent.getContext())
                    .load(itemImageAdd)
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.no_image)
                    .into(imageView);


            price.setText("? " + itemCost);
            priceNew.setText("? " + itemCostNew);
            itemNameTextView.setText(itemName);
            itemQuantitySelectedTextView.setText(Integer.toString(itemQuantitySelected));
        }
        return convertView;
    }
}