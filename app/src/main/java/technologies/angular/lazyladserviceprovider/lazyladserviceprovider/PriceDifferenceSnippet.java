package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 20-07-2015.
 */
public class PriceDifferenceSnippet {

    @SerializedName("order_id")
    public int m_itemCode;
    @SerializedName("item_img_address")
    public String m_itemImgAddress;
    @SerializedName("item_name")
    public String m_itemName;
    @SerializedName("item_desc")
    public String m_itemDesc;
    @SerializedName("item_quantity_selected")
    public int m_itemQuantitySelected;
    @SerializedName("item_cost")
    public double m_itemCost;
    @SerializedName("item_new_cost")
    public double m_itemCostNew;   //Standard Weight

    public PriceDifferenceSnippet()
    {
    }

    public PriceDifferenceSnippet(int orderID, String itemName, String itemImgAdd, String itemDesc, double itemCost, double itemCostNew, int itemQuantitySelected)
    {
        m_itemCode=orderID;
        m_itemName=itemName;
        m_itemImgAddress=itemImgAdd;
        m_itemCost=itemCost;
        m_itemDesc=itemDesc;
        m_itemCostNew=itemCostNew;
        m_itemQuantitySelected=itemQuantitySelected;
    }
}