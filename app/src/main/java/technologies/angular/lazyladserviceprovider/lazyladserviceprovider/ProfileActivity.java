package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiRequestModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiResponseModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiSuggestionService;

/**
 * Created by sakshigupta on 04/10/15.
 */
public class ProfileActivity extends ActionBarActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, ResultCallback<LocationSettingsResult> {

    private Toolbar toolbar;
    private EditText accountName, accountNo, ifscCode, branchAddress;
    private TextView sellerName, sellerPhone;
    private String ifsc_code = "", branch_add = "";
    private String account_no = "", account_name = "";
    private Button submitBankDetailsButton;
    private String m_servProvId;

    protected GoogleApiClient mGoogleApiClient;
    private GeoPlaceAutocompleteAdapter mAdapter;
    private AutoCompleteTextView mAutocompleteView;
    private View mCurrentLocation;
    private Location mLastLocation;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private static final LatLngBounds BOUNDS_GREATER_SYDNEY = new LatLngBounds(
            new LatLng(6.7535159, 68.1623859), new LatLng(35.5087008, 97.395561));
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    protected LocationSettingsRequest mLocationSettingsRequest;
    protected String TAG = "locationSetting";
    private static final String TAG2 = "LocationAddress";

    protected LocationRequest mLocationRequest;
    private SharedPreferences prefs_location;
    private ProgressDialog progressDialog;
    private double mLatitude;
    private double mLongitude;
    private ArrayList<CitySnippet> m_cityList;
    Map<String, String> mCityMap = new HashMap<String, String>();
    String mCityName;
    String mCityCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);

        toolbar = (Toolbar) findViewById(R.id.items_toolbar);
        setSupportActionBar(toolbar);

        Cursor m_cursor = getContentResolver().query(
                LazyLadServiceProviderContract.UserDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    m_servProvId = m_cursor.getString(1);
                    m_cursor.moveToNext();
                }
            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }

        accountName = (EditText) findViewById(R.id.account_name_editText);
        accountNo = (EditText) findViewById(R.id.account_no_editText);
        ifscCode = (EditText) findViewById(R.id.ifsc_code_editText);
        branchAddress = (EditText) findViewById(R.id.branch_add_editText);
        sellerName = (TextView) findViewById(R.id.seller_name);
        sellerPhone = (TextView) findViewById(R.id.seller_phone);
        submitBankDetailsButton = (Button) findViewById(R.id.submitProfileDetails);


        if (checkPlayServices()) {
            buildGoogleApiClient();
        }
        prefs_location = getSharedPreferences(
                "technologies.angular.lazyladserviceprovider.lazyladserviceprovider", Context.MODE_PRIVATE);

        progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);

        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        buildLocationSettingsRequest();


        mAutocompleteView = (AutoCompleteTextView) findViewById(R.id.autocomplete_places);
        mAutocompleteView.setOnItemClickListener(mAutocompleteClickListener);

        mCurrentLocation = findViewById(R.id.gps_view);


        mAdapter = new GeoPlaceAutocompleteAdapter(this, mGoogleApiClient, BOUNDS_GREATER_SYDNEY,
                null);
        mAutocompleteView.setAdapter(mAdapter);


        mCurrentLocation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mAutocompleteView.setText("");
                if (!progressDialog.isShowing())
                    progressDialog.show();
                checkLocationSettings();

            }
        });

        loadCities();

        submitBankDetailsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                account_name = accountName.getText().toString();
                account_no = accountNo.getText().toString();
                ifsc_code = ifscCode.getText().toString();
                branch_add = branchAddress.getText().toString();
                if (account_no.isEmpty() || account_no == null) {
                    Toast.makeText(getApplicationContext(), "Please enter your account number", Toast.LENGTH_SHORT).show();
                } else if (account_name.isEmpty() || account_name == null) {
                    Toast.makeText(getApplicationContext(), "Please enter holder's name", Toast.LENGTH_SHORT).show();
                } else if (ifsc_code.isEmpty() || ifsc_code == null) {
                    Toast.makeText(getApplicationContext(), "Please enter ifsc code", Toast.LENGTH_SHORT).show();
                } else if (branch_add.isEmpty() || branch_add == null) {
                    Toast.makeText(getApplicationContext(), "Please enter branch address", Toast.LENGTH_SHORT).show();
                } else {
                    SubmitBankDetails();
                }
            }
        });

    }

    private void loadCities() {

        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(ProfileActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Utils.RestApiPathUrl).
                        setLogLevel(RestAdapter.LogLevel.FULL).build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
        apiSuggestionsService.cities(new Callback<CityModel>() {
            @Override
            public void success(CityModel cityModel, retrofit.client.Response response) {
                m_cityList = new ArrayList<CitySnippet>();
                m_cityList = cityModel.cities;
                if (!m_cityList.isEmpty()) {
                    for (int i = 0; i < m_cityList.size(); i++) {
                        mCityMap.put(m_cityList.get(i).m_cityName, m_cityList.get(i).m_cityCode);
                    }

                    progressDialog.dismiss();
                    getSellerProfileDetails();

                }
            }

            @Override
            public void failure(final RetrofitError error) {
                progressDialog.dismiss();
                loadCities();
            }
        });
    }


    private void displayLocation() {

        new Handler().postDelayed((Runnable) (new Runnable() {
            public void run() {
                mLastLocation = LocationServices.FusedLocationApi
                        .getLastLocation(mGoogleApiClient);
                if (mLastLocation != null) {
                    mLatitude = mLastLocation.getLatitude();
                    mLongitude = mLastLocation.getLongitude();

                    SharedPreferences.Editor editor = prefs_location.edit();
                    editor.putString(EditorConstants.LATITUDE_SELECTED_CONSTANT, String.valueOf(mLatitude));
                    editor.putString(EditorConstants.LONGITUDE_SELECTED_CONSTANT, String.valueOf(mLongitude));
                    editor.commit();
                    getAddressFromLocation(mLatitude, mLongitude,
                            getApplicationContext(), new GeocoderHandler());

                } else {
                    displayLocation();

                }
            }
        }), 1200);
    }


    public void getAddressFromLocation(final double latitude, final double longitude,
                                       final Context context, final Handler handler) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                if (geocoder.isPresent()) {

                    try {
                        List<Address> addressList = geocoder.getFromLocation(
                                latitude, longitude, 1);
                        if (addressList != null && addressList.size() > 0) {
                            Address address = addressList.get(0);

                            StringBuilder sb = new StringBuilder();
                            for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                                sb.append(address.getAddressLine(i)).append(" ");
                            }


                            if (address.getLocality() != null)
                                mCityName = address.getLocality().toString().trim();

                        }
                    } catch (IOException e) {
                        Log.e(TAG2, "Unable connect to Geocoder", e);
                    } finally {
                        Message message = Message.obtain();
                        message.setTarget(handler);
                        if (!(mCityName.equals(EditorConstants.DEFAULT_CITY_SELECTED_CONSTANT))) {
                            message.what = 1;
                            Bundle bundle = new Bundle();
                            bundle.putString("mCityName", mCityName);
                            message.setData(bundle);
                        } else {
                            message.what = 0;
                            Bundle bundle = new Bundle();
                            bundle.putString("mCityName", mCityName);
                            message.setData(bundle);
                        }
                        message.sendToTarget();
                    }
                } else {
                    Message message = Message.obtain();
                    message.setTarget(handler);
                    message.what = 0;
                    Bundle bundle = new Bundle();
                    bundle.putString("mCityName", mCityName);
                    message.setData(bundle);

                }
            }
        };
        thread.start();
    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationCity;
            String locationArea;
            if (progressDialog.isShowing())
                progressDialog.dismiss();
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationCity = bundle.getString("mCityName");
                    locationArea = bundle.getString("mAreaName");
                    getCityCode(mCityName);

                    break;
                default:
                    locationCity = EditorConstants.DEFAULT_CITY_SELECTED_CONSTANT;
                    requestCityName(mLatitude, mLongitude);
            }


        }
    }


    private void getCityCode(String city_name) {
        if (mCityMap.containsKey(city_name)) {
            mCityCode = (mCityMap.get(city_name)).trim();
            Log.e("city_code", mCityCode);

        } else {
            Toast.makeText(this, "Service not available", Toast.LENGTH_LONG).show();
        }


    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0 /* clientId */, this)
                .addApi(Places.GEO_DATA_API)
                .addConnectionCallbacks(ProfileActivity.this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (!progressDialog.isShowing())
                progressDialog.show();
            final AutocompletePrediction item = mAdapter.getItem(position);
            final String placeId = item.getPlaceId();

            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);


        }
    };

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                places.release();
                return;
            }
            final Place place = places.get(0);


            mLatitude = place.getLatLng().latitude;
            mLongitude = place.getLatLng().longitude;

            SharedPreferences.Editor editor = prefs_location.edit();
            editor.putString(EditorConstants.LATITUDE_SELECTED_CONSTANT, String.valueOf(mLatitude));
            editor.putString(EditorConstants.LONGITUDE_SELECTED_CONSTANT, String.valueOf(mLongitude));
            editor.commit();

            getAddressFromLocation(mLatitude, mLongitude,
                    getApplicationContext(), new GeocoderHandler());

            places.release();
        }
    };

    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    protected void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );

        result.setResultCallback(this);
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e("error", "onConnectionFailed: ConnectionResult.getErrorCode() = "
                + connectionResult.getErrorCode());

        // TODO(Developer): Check error code and notify the user of error state and resolution.
        Toast.makeText(this,
                "Could not connect to Google API Client: Error " + connectionResult.getErrorCode(),
                Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        checkPlayServices();
    }

    @Override
    public void onConnected(Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.i(TAG, "All location settings are satisfied.");
                displayLocation();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(ProfileActivity.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.i(TAG, "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i(TAG, "User agreed to make required location settings changes.");
                        displayLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i(TAG, "User chose not to make required location settings changes.");
                        break;
                }
                break;
        }
    }


    protected void requestCityName(final double latitude, final double longitude) {
        final String key = "AIzaSyAevWz0y25b6b1kkdBfHl1ncDZGQU-8Qa4";


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + "," + longitude + "&key=AIzaSyChaF8L3iN0Duz8Lf_H2ifnpQxhXFZcLRs&result_type=locality")
                .build();


    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }


    private void getSellerProfileDetails() {
        final ProgressDialog pDialog;

        pDialog = new ProgressDialog(ProfileActivity.this);
        pDialog.setMessage("Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://www.angulartechnologies.com").build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
        String seller_id = m_servProvId;
        Log.i("SpCode", seller_id + "");
        apiSuggestionsService.getProfileDetails(seller_id, new Callback<ApiResponseModel.ProfileModel>() {

            @Override
            public void success(ApiResponseModel.ProfileModel responseProfileDetails, Response response) {
                boolean success = responseProfileDetails.success;
                Log.i("Success", responseProfileDetails.success + "");
                if (success == true) {

                    sellerName.setText(responseProfileDetails.seller_name);
                    sellerPhone.setText(responseProfileDetails.seller_number);
                    if (null != responseProfileDetails.profile) {
                        accountName.setText(responseProfileDetails.profile.account_name);
                        accountNo.setText(responseProfileDetails.profile.account_number);
                        ifscCode.setText(responseProfileDetails.profile.ifsc_code);
                        branchAddress.setText(responseProfileDetails.profile.branch_details);
                    }

                    accountName.setEnabled(true);
                    accountNo.setEnabled(true);
                    ifscCode.setEnabled(true);
                    branchAddress.setEnabled(true);
                }
                pDialog.dismiss();
            }

            @Override
            public void failure(final RetrofitError error) {
                Log.e("Retrofit error", error.toString());
                pDialog.dismiss();
            }
        });
    }

    private void SubmitBankDetails() {

        final ProgressDialog pDialog;

        pDialog = new ProgressDialog(ProfileActivity.this);
        pDialog.setMessage("Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://www.angulartechnologies.com").build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
        ApiRequestModel requestModel = new ApiRequestModel();
        ApiRequestModel.DetailsInput user = requestModel.new DetailsInput(account_name, account_no, ifsc_code, branch_add, m_servProvId, mLatitude, mLongitude, mCityCode);
        apiSuggestionsService.bankDetails(user, new Callback<ApiResponseModel.DetailsOutput>() {

            @Override
            public void success(ApiResponseModel.DetailsOutput responseDetails, Response response) {
                boolean success = responseDetails.success;
                String message = responseDetails.message;
                Log.i("Success on post", success + "");
                Log.i("meesage", message);
                if (success == true) {
                    accountName.setEnabled(false);
                    accountNo.setEnabled(false);
                    ifscCode.setEnabled(false);
                    branchAddress.setEnabled(false);
                    submitBankDetailsButton.setEnabled(false);
                    mAutocompleteView.setEnabled(false);
                    Toast.makeText(ProfileActivity.this, "Successfully Submitted", Toast.LENGTH_SHORT).show();

                } else {
                    accountName.setEnabled(true);
                    accountNo.setEnabled(true);
                    ifscCode.setEnabled(true);
                    branchAddress.setEnabled(true);
                    submitBankDetailsButton.setEnabled(true);
                    Toast.makeText(ProfileActivity.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                }

                pDialog.dismiss();
            }

            @Override
            public void failure(final RetrofitError error) {
                Log.i("retrofit error post", error.toString());
                pDialog.dismiss();
            }
        });
    }
}