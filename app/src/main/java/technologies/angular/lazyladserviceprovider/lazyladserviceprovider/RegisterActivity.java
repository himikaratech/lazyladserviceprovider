package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;

import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.RegistrationSellerDetails;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.RegistrationSellerAreas;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiRequestModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiResponseModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiSuggestionService;

public class RegisterActivity extends ActionBarActivity {

    private ArrayList<CitySnippet> m_cityList;
    private ArrayList<AreaSnippet> m_areasList;
    private ArrayList<ServiceSnippet> m_serviceList;

    private String m_cityCodeSelected;

    private Spinner m_cityListSpinner;
    private Spinner m_areaListSpinner;
    private Spinner m_serviceListSpinner;

    private ArrayList<String> m_cityNamesList;
    private ArrayList<String> m_areaNamesList;
    private ArrayList<String> m_serviceNamesList;

    private TextView m_userInfo;
    private static Cursor m_cursor;

    String sellerName;
    String loginPassword;
    String sellerShopName;
    String sellerShopAddress;
    int sellerCityCode;
    int sellerServiceType;
    EditText phoneNumber;
    String number;
    private String mVerificationSource;

    public static final String PHONE_NUMBER_TAG = "PHONE_NUMBER_TAG";
    public static final String VERIFICATION_SOURCE_TAG = "VERIFICATION_SOURCE_TAG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Intent intent = getIntent();
        number = intent.getStringExtra(PHONE_NUMBER_TAG);
        mVerificationSource = intent.getStringExtra(VERIFICATION_SOURCE_TAG);

        m_cityCodeSelected = "";
        m_userInfo = (TextView) findViewById(R.id.user_info);
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/Roboto_Light.ttf");
        m_userInfo.setTypeface(typeFace);

        m_cityListSpinner = (Spinner) findViewById(R.id.city_list_spinner);
        m_areaListSpinner = (Spinner) findViewById(R.id.area_list_spinner);

        m_serviceListSpinner = (Spinner) findViewById(R.id.service_types_list_spinner);
        m_cityListSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                m_cityCodeSelected = m_cityList.get(position).m_cityCode;
                loadAreas(m_cityCodeSelected);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        insertNumberIntoDb();
        initializeUiwithData();

        Button submitButton = (Button) findViewById(R.id.register_button);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sellerName = ((EditText) findViewById(R.id.name)).getText().toString();
                loginPassword = ((EditText) findViewById(R.id.password)).getText().toString();
                sellerShopName = ((EditText) findViewById(R.id.shopname)).getText().toString();
                sellerShopAddress = ((EditText) findViewById(R.id.shopaddress)).getText().toString();
                if (sellerName.isEmpty() || sellerName == null) {
                    Toast.makeText(getApplicationContext(), "Please enter your Name", Toast.LENGTH_SHORT).show();
                } else if (loginPassword.isEmpty() || loginPassword == null) {
                    Toast.makeText(getApplicationContext(), "Please enter a Password", Toast.LENGTH_SHORT).show();
                } else if (sellerShopName.isEmpty() || sellerShopName == null) {
                    Toast.makeText(getApplicationContext(), "Please enter your Shop Name", Toast.LENGTH_SHORT).show();
                } else if (sellerShopAddress.isEmpty() || sellerShopAddress == null) {
                    Toast.makeText(getApplicationContext(), "Please enter your Shop Address", Toast.LENGTH_SHORT).show();
                } else {
                    RegisterOnServer(number);
                }
            }
        });
        setTracker();
    }

    private void RegisterOnServer(String number) {
        final ProgressDialog pDialog;
        pDialog = new ProgressDialog(RegisterActivity.this);
        pDialog.setMessage("Registering. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        final String params = number;
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Utils.RestApiPathUrl)

                .build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
        ApiRequestModel requestModel = new ApiRequestModel();
        ApiRequestModel.Input input = requestModel.new Input(params);
        apiSuggestionsService.availability(input, new Callback<ApiResponseModel.ResponseAvailability>() {

            @Override
            public void success(ApiResponseModel.ResponseAvailability responseAvailability, retrofit.client.Response response) {
                int success = responseAvailability.error;
                if (success == 1) {
                    int availabilityCode = responseAvailability.code;

                    int sellerCityCode = Integer.parseInt(m_cityList.get(m_cityListSpinner.getSelectedItemPosition()).m_cityCode);
                    int sellerAreaCode = Integer.parseInt(m_areasList.get(m_areaListSpinner.getSelectedItemPosition()).m_areaCode);
                    int sellerServiceType = Integer.parseInt(m_serviceList.get(m_serviceListSpinner.getSelectedItemPosition()).m_serviceCode);

                    switch (availabilityCode) {
                        case 0:
                            //some error in connecting with server
                            //Toast.makeText(getApplicationContext(), getApplicationContext().getResources().getString(R.string.toast_sync_completed), Toast.LENGTH_SHORT).show();
                            Toast.makeText(getApplicationContext(), "Some error in connecting with server occurred, Try Again", Toast.LENGTH_SHORT).show();
                            break;
                        case 1:
                            // success in registration
                            ContentValues values = new ContentValues();
                            values.put(RegistrationSellerDetails.COLUMN_SELLER_NAME, Utils.strForSqlite(sellerName));
                            values.put(RegistrationSellerDetails.COLUMN_PASSWORD, Utils.strForSqlite(loginPassword));
                            values.put(RegistrationSellerDetails.COLUMN_SHOP_NAME, Utils.strForSqlite(sellerShopName));
                            values.put(RegistrationSellerDetails.COLUMN_SHOP_ADDRESS, Utils.strForSqlite(sellerShopAddress));
                            values.put(RegistrationSellerDetails.COLUMN_SHOP_CITY, sellerCityCode);
                            values.put(RegistrationSellerDetails.COLUMN_SHOP_AREAS, sellerAreaCode);
                            values.put(RegistrationSellerDetails.COLUMN_SHOP_TYPE, sellerServiceType);
                            values.put(RegistrationSellerDetails.COLUMN_VERIFICATION_SOURCE, mVerificationSource);

                            getContentResolver().update(RegistrationSellerDetails.CONTENT_URI, values, LazyLadServiceProviderContract.RegistrationSellerDetails.COLUMN_SELLER_PHONE + " =? ", new String[]{String.valueOf(params)});

                            Intent in = new Intent(RegisterActivity.this, ShopDetailsActivity.class);
                            in.putExtra("sellerServiceType", sellerServiceType);
                            startActivity(in);
                            break;
                        case 2:
                            //seller already registered with unique user name choosen (in our case presently its phone number)
                            Toast.makeText(getApplicationContext(), "Seller with this phone number already registered. Please try to login or call support for password", Toast.LENGTH_SHORT).show();
                            break;
                        case 3:
                            //some error on server
                            Toast.makeText(getApplicationContext(), "Some error on server occured, Try Again", Toast.LENGTH_SHORT).show();
                            break;
                        case 4:
                            //some other errors
                        default:
                            // also considered as error
                            Toast.makeText(getApplicationContext(), "Some other Error occured. We'll figure and fix this soon.", Toast.LENGTH_SHORT).show();
                    }
                }
                pDialog.dismiss();
            }

            @Override
            public void failure(final RetrofitError error) {
                pDialog.dismiss();
            }
        });
    }

    private void insertNumberIntoDb() {

        clearData();

        ContentValues values = new ContentValues();
        values.put(RegistrationSellerDetails.COLUMN_SELLER_NAME, "");
        values.put(RegistrationSellerDetails.COLUMN_SELLER_PHONE, number);
        values.put(RegistrationSellerDetails.COLUMN_SELLER_PHONE_VAL_FLAG, 1);
        values.put(RegistrationSellerDetails.COLUMN_PASSWORD, "");
        values.put(RegistrationSellerDetails.COLUMN_SHOP_NAME, "");
        values.put(RegistrationSellerDetails.COLUMN_SHOP_ADDRESS, "");
        values.put(RegistrationSellerDetails.COLUMN_SHOP_CITY, 0);
        values.put(RegistrationSellerDetails.COLUMN_SHOP_AREAS, 0);
        values.put(RegistrationSellerDetails.COLUMN_SHOP_TYPE, 0);
        values.put(RegistrationSellerDetails.COLUMN_VERIFICATION_SOURCE, mVerificationSource);


        getContentResolver().insert(RegistrationSellerDetails.CONTENT_URI, values);

    }

    private void initializeUiwithData() {

        loadServiceTypes();

        phoneNumber = ((EditText) findViewById(R.id.phone));
        phoneNumber.setText(number);
        phoneNumber.setKeyListener(null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    private void clearData() {

        m_cursor = getContentResolver().query(RegistrationSellerDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() > 0) {

                getContentResolver().delete(RegistrationSellerDetails.CONTENT_URI,
                        null,
                        null
                );
            }
        }

        Cursor cursor2 = getContentResolver().query(RegistrationSellerAreas.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (cursor2 != null && cursor2.moveToFirst()) {
            if (cursor2.getCount() > 0) {
                getContentResolver().delete(RegistrationSellerAreas.CONTENT_URI,
                        null,
                        null
                );
            }
        }
    }

    private void loadCities() {

        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(RegisterActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Utils.RestApiPathUrl).build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
        apiSuggestionsService.cities(new Callback<CityModel>() {
            @Override
            public void success(CityModel cityModel, retrofit.client.Response response) {
                m_cityList = new ArrayList<technologies.angular.lazyladserviceprovider.lazyladserviceprovider.CitySnippet>();
                m_cityList = cityModel.cities;
                m_cityNamesList = new ArrayList<String>(m_cityList.size());
                for (int i = 0; i < m_cityList.size(); i++) {
                    m_cityNamesList.add(m_cityList.get(i).m_cityName);
                }
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.my_spinner, m_cityNamesList);
                dataAdapter.setDropDownViewResource(R.layout.my_spinner_dropdown);
                m_cityListSpinner.setAdapter(dataAdapter);
                progressDialog.dismiss();
            }

            @Override
            public void failure(final RetrofitError error) {
                progressDialog.dismiss();
                loadCities();
            }
        });
    }

    private void loadAreas(final String cityCode) {

        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(RegisterActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Utils.RestApiPathUrl).build();
        ApiSuggestionService api = restAdapter.create(ApiSuggestionService.class);
        String param = cityCode;
        api.areas(param, new Callback<AreaModel>() {
            @Override
            public void success(AreaModel areaModel, retrofit.client.Response response) {
                m_areasList = new ArrayList<AreaSnippet>();
                m_areasList = areaModel.areas;
                m_areaNamesList = new ArrayList<String>(m_areasList.size());
                for (int i = 0; i < m_areasList.size(); i++) {
                    m_areaNamesList.add(m_areasList.get(i).m_areaName);
                }
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.my_spinner, m_areaNamesList);
                dataAdapter.setDropDownViewResource(R.layout.my_spinner_dropdown);
                m_areaListSpinner.setAdapter(dataAdapter);
                progressDialog.dismiss();
            }

            @Override
            public void failure(final RetrofitError error) {
                progressDialog.dismiss();
                loadAreas(cityCode);
            }
        });
    }

    private void loadServiceTypes() {

        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(RegisterActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Utils.RestApiPathUrl).build();
        ApiSuggestionService api = restAdapter.create(ApiSuggestionService.class);
        api.serviceTypes(new Callback<ServiceModel>() {
            @Override
            public void success(ServiceModel serviceModel, retrofit.client.Response response) {
                m_serviceList = new ArrayList<technologies.angular.lazyladserviceprovider.lazyladserviceprovider.ServiceSnippet>();
                m_serviceList = serviceModel.service_types;
                m_serviceNamesList = new ArrayList<String>(m_serviceList.size());
                for (int i = 0; i < m_serviceList.size(); i++) {
                    m_serviceNamesList.add(m_serviceList.get(i).m_serviceName);
                }
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.my_spinner, m_serviceNamesList);
                dataAdapter.setDropDownViewResource(R.layout.my_spinner_dropdown);
                m_serviceListSpinner.setAdapter(dataAdapter);

                progressDialog.dismiss();

                loadCities();
            }

            @Override
            public void failure(final RetrofitError error) {
                progressDialog.dismiss();
                loadServiceTypes();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(this.getClass().getCanonicalName());

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }
}
