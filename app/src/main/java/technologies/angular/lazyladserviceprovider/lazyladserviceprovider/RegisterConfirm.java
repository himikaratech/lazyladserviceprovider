package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;

import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.RegistrationSellerDetails;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.RegistrationSellerAreas;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiRequestModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiResponseModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiSuggestionService;


public class RegisterConfirm extends ActionBarActivity {

    private String suggestion;
    private String number;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_confirm);

        toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        Button bNextAction = (Button) findViewById(R.id.register_confirm_login_button);
        TextView message = (TextView) findViewById(R.id.textViewMessage);
        TextView queryCall = (TextView) findViewById(R.id.textViewQueryCall);
        final Button suggestionSubmit = (Button) findViewById(R.id.feedback_btn);
        final EditText suggestionEditText = (EditText) findViewById(R.id.feedback_editView);

        Intent intent = getIntent();
        number = intent.getStringExtra("phone");

        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/Roboto_Light.ttf");
        Typeface typeFace1 = Typeface.createFromAsset(getAssets(), "fonts/Roboto_Regular.ttf");

        message.setTypeface(typeFace);
        queryCall.setTypeface(typeFace1);

        suggestionSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                suggestion = suggestionEditText.getText().toString();

                RestAdapter restAdapter = new RestAdapter.Builder()
                        .setEndpoint(Utils.RestApiPathUrl).build();
                ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
                ApiRequestModel requestModel = new ApiRequestModel();
                ApiRequestModel.Suggestion comment = requestModel.new Suggestion(number, suggestion);
                apiSuggestionsService.suggestion(comment, new Callback<ApiResponseModel.ResponseSuggestion>() {

                    @Override
                    public void success(ApiResponseModel.ResponseSuggestion responseSuggestion, retrofit.client.Response response) {
                        int success = responseSuggestion.error;
                        Log.i("error", success + "");
                        if (success == 1) {
                            Toast.makeText(getApplicationContext(), "Thanks for the feedback!", Toast.LENGTH_SHORT).show();
                            suggestionEditText.setText("");
                            suggestionEditText.setEnabled(false);
                            suggestionSubmit.setEnabled(false);
                        }
                    }

                    @Override
                    public void failure(final RetrofitError error) {
                        Log.i("Failure", error+" retro");
                    }
                });
            }
        });
        int regCode = getIntent().getIntExtra("register_code", 0);
        switch (regCode) {
            case 0:
                //some error on server
                //Toast.makeText(getApplicationContext(), "Some error on server occurred, Try Again", Toast.LENGTH_SHORT).show();
                getSupportActionBar().setTitle("Sorry! Try Again");
                message.setText("Some error on server occurred");
                bNextAction.setText("Go");
                bNextAction.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(view.getContext(), RegisterActivity.class));
                    }
                });
                break;
            case 1:
                // success in registration
                //Cleaning all Registration data from local registration tables
                clearData();
                getSupportActionBar().setTitle("Successful Registration");
                message.setText("Registration Successful");
                bNextAction.setText("Login");
                bNextAction.setOnClickListener(new View.OnClickListener()

                {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(view.getContext(), LoginActivity.class));
                    }
                });
                break;

            case 2:
                //seller already registered with unique user name choosen (in our case presently its phone number)
                //Toast.makeText(getApplicationContext(), "Seller with this number already registered. Please try to login or use another number to register", Toast.LENGTH_SHORT).show();
                // success in registration
                getSupportActionBar().setTitle("Already Registered");
                message.setText("Seller with this number already registered. Please try to login or use another number to register");
                bNextAction.setText("Login");
                bNextAction.setOnClickListener(new View.OnClickListener()

                {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(view.getContext(), LoginActivity.class));
                    }
                });
                break;

            case 3:
                //some other errors
            default:
                // also considered as error
                //Toast.makeText(getApplicationContext(), "Some unknown Error occured. We'll figure and fix this soon.", Toast.LENGTH_SHORT).show();
                getSupportActionBar().setTitle("Sorry!");
                message.setText("Some unknown Error occurred. We'll figure and fix this soon.");
                bNextAction.setText("Register");
                bNextAction.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(view.getContext(), RegisterActivity.class));
                    }
                });
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent myIntent = new Intent(this, LoginActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(myIntent);
    }


    private void clearData() {
        Uri deleteUri;
        Uri selectUri;

        String[] dummy = new String[]{"a", "b"};

        selectUri = Uri.parse((String) ("content://technologies.angular.lazyladserviceprovider.lazyladserviceprovider.provider/SELECT"));

        deleteUri = Uri.parse((String) ("content://technologies.angular.lazyladserviceprovider.lazyladserviceprovider.provider/DELETE"));

        Cursor cursor = getContentResolver().query(RegistrationSellerDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (cursor != null && cursor.moveToFirst()) {
            if (cursor.getCount() > 0) {
                getContentResolver().delete(RegistrationSellerDetails.CONTENT_URI,
                        null,
                        null
                );
            }
            cursor.close();
        }

        cursor = getContentResolver().query(RegistrationSellerAreas.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (cursor != null && cursor.moveToFirst()) {
            if (cursor.getCount() > 0) {
                getContentResolver().delete(RegistrationSellerAreas.CONTENT_URI,
                        null,
                        null
                );
            }
            cursor.close();
        }
    }
}
