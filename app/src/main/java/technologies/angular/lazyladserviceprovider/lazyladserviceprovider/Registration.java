package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.truecaller.android.sdk.ITrueCallback;
import com.truecaller.android.sdk.TrueButton;
import com.truecaller.android.sdk.TrueClient;
import com.truecaller.android.sdk.TrueError;
import com.truecaller.android.sdk.TrueProfile;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiRequestModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiResponseModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiSuggestionService;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.com.checkmobi.sdk.AeSimpleSHA1;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.com.checkmobi.sdk.CallReceiver;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.com.checkmobi.sdk.AsyncResponse;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.com.checkmobi.sdk.CheckMobiService;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.com.checkmobi.sdk.ErrorCode;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.com.checkmobi.sdk.ValidationType;

/**
 * Created by Sakshi on 29/06/15.
 */
public class Registration extends AppCompatActivity implements ITrueCallback {

    private WeakReference<ProgressDialog> loadingDialog;

    private EditText phoneNumberEditText;
    private EditText pinEditText;
    private ImageButton pinSubmission;
    private Button resendCode;
    private ImageView imageForPhone;
    private Button rcli;
    private ImageButton numberSubmission;
    private TextView startingText;
    private TextView chargeLabel;
    private LinearLayout retryVerificationLayout;
    private LinearLayout numberVerificationLayout;
    private LinearLayout pinVerificationLayout;

    private String callId;
    private String dialingNumber;
    private String validationKey;
    private boolean pinStep = false;
    private String mobileNumber;
    private String pin_hash;
    private String key;
    private int validationTypeFlag = 0;

    private String callerid_hash;
    private Timer timer;
    private TimerTask timerTask;
    private Handler handler = new Handler();
    public static String smsCode = "";

    int pinsubmitted = 0;
    int resendFlag = 0;
    Boolean isInternetPresent = false;

    private Toolbar m_toolbar;
    private Typeface typeFace;

    private TrueClient mTrueClient;

    private static final int TRUE_ERROR_TYPE_INTERNAL = 0;
    private static final int TRUE_ERROR_TYPE_NETWORK = 1;
    private static final int TRUE_ERROR_TYPE_USER_DENIED = 2;
    private static final int TRUE_ERROR_TYPE_UNAUTHORIZED_PARTNER = 3;
    private static final int TRUE_ERROR_TYPE_UNAUTHORIZED_USER = 4;

    private static final String SOURCE_TRUECALLER = "truecaller";
    private static final String SOURCE_MOBILE = "check_mobi";

    private String mNoVerificationSource;

    final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(CallReceiver.MSG_CALL_START)) {
                if (!intent.getBooleanExtra("incoming", false)) {
                    if (Registration.this.dialingNumber == null || Registration.this.validationKey == null || Registration.this.callId != null)
                        return;
                    Registration.this.callId = intent.getStringExtra("number");
                } else {
                    String number = intent.getStringExtra("number");

                    //on some devices the number is in local format for local number.
                    boolean matching = false;
                    try {
                        String hash = AeSimpleSHA1.SHA1(number.substring(number.length() - 3));

                        if (hash.equals(callerid_hash))
                            matching = true;
                    } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Incoming call received: " + number + " hash: " + callerid_hash + " matching: " + matching);

                    if (matching) {
                        StopReverseCliTimer();
                        HangupCall();

                        //check validation
                        String pinNumber = number.substring(number.length() - 4);

                        CheckMobiService.getInstance().VerifyPin(Registration.this.validationKey, pinNumber, new AsyncResponse() {
                            @Override
                            public void OnRequestCompleted(int httpStatus, Map<String, Object> result, String error) {
                                if (validationTypeFlag == 1 || pinsubmitted == 1)
                                    ShowLoadingMessage(false);

                                if (httpStatus == CheckMobiService.STATUS_SUCCESS && result != null) {
                                    Boolean validated = (Boolean) result.get("validated");

                                    if (!validated) {
                                        AeSimpleSHA1.Utils1.ShowMessageBox(new AlertDialog.Builder(Registration.this), "Error", "Validation failed!");
                                        return;
                                    }

                                    Toast.makeText(Registration.this, "Verification Completed.", Toast.LENGTH_SHORT).show();
                                    Intent in = new Intent(Registration.this, RegisterActivity.class);
                                    in.putExtra("phoneNumber", mobileNumber);
                                    in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    smsCode = "";
                                    pinsubmitted = 1;
                                    startActivity(in);
                                    Registration.this.finish();
                                } else {
                                    HandleValidationServiceError(httpStatus, result, error);
                                }
                            }
                        });
                    }
                }
            }
            if (intent.getAction().equals(CallReceiver.MSG_CALL_END)) {
                if (!intent.getBooleanExtra("incoming", false)) {
                    Registration pThis = Registration.this;

                    if (pThis.dialingNumber == null || pThis.validationKey == null || pThis.callId == null)
                        return;
                    if (pThis.callId.compareTo(intent.getStringExtra("number")) != 0)
                        return;

                    DismissKeyboard();
                    if (validationTypeFlag == 1 || pinsubmitted == 1)
                        ShowLoadingMessage(true);

                    CheckMobiService.getInstance().CheckValidationStatus(pThis.validationKey, new AsyncResponse() {
                        @Override
                        public void OnRequestCompleted(int httpStatus, Map<String, Object> result, String error) {
                            if (validationTypeFlag == 1 || pinsubmitted == 1)
                                ShowLoadingMessage(false);

                            if (httpStatus == CheckMobiService.STATUS_SUCCESS && result != null) {
                                Boolean validated = (Boolean) result.get("validated");

                                if (!validated) {
                                    AeSimpleSHA1.Utils1.ShowMessageBox(new AlertDialog.Builder(Registration.this), "Error", "Number not validated ! Check your phone number!");
                                    return;
                                }

                                Toast.makeText(Registration.this, "Verification Completed.", Toast.LENGTH_SHORT).show();
                                Intent in = new Intent(Registration.this, RegisterActivity.class);
                                in.putExtra("phoneNumber", mobileNumber);
                                in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                smsCode = "";
                                pinsubmitted = 1;
                                startActivity(in);

                                Registration.this.finish();
                            } else {
                                HandleValidationServiceError(httpStatus, result, error);
                            }
                        }
                    });
                }
            }
        }
    };

    private void StopReverseCliTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
            timerTask = null;
        }
    }

    private void HangupCall() {
        try {
            TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            Class c = Class.forName(tm.getClass().getName());
            Method m = c.getDeclaredMethod("getITelephony");
            m.setAccessible(true);
            Object telephonyService = m.invoke(tm);
            c = Class.forName(telephonyService.getClass().getName());
            m = c.getDeclaredMethod("endCall");
            m.setAccessible(true);
            m.invoke(telephonyService);
        } catch (Exception ex) {
            System.out.println("Failed to hangup the call...:" + ex.getMessage());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.number_verification);

        m_toolbar = (Toolbar) findViewById(R.id.verify_toolbar);
        setSupportActionBar(m_toolbar);

        this.phoneNumberEditText = (EditText) findViewById(R.id.phoneNrTextEdit);
        this.pinEditText = (EditText) findViewById(R.id.pinTextEdit);
        this.rcli = (Button) findViewById(R.id.rcli);
        this.imageForPhone = (ImageView) findViewById(R.id.phone_number_image);
        this.numberSubmission = (ImageButton) findViewById(R.id.submitBtn);
        this.pinSubmission = (ImageButton) findViewById(R.id.submitPinBtn);
        this.resendCode = (Button) findViewById(R.id.resetBtt);
        retryVerificationLayout = (LinearLayout) findViewById(R.id.retry_verification_layout);
        numberVerificationLayout = (LinearLayout) findViewById(R.id.number_verification_layout);
        pinVerificationLayout = (LinearLayout) findViewById(R.id.pin_verification_layout);

        this.startingText.setText("Please enter your 10 digit \nmobile number");
        this.imageForPhone.setBackgroundResource(R.drawable.phone_number);

        typeFace = Typeface.createFromAsset(getAssets(), "fonts/Roboto_Light.ttf");
        startingText.setTypeface(typeFace);
        phoneNumberEditText.setTypeface(typeFace);
        pinEditText.setTypeface(typeFace);
        chargeLabel.setTypeface(typeFace);

        registerReceiver(receiver, new IntentFilter(CallReceiver.MSG_CALL_START));
        registerReceiver(receiver, new IntentFilter(CallReceiver.MSG_CALL_END));

        pinsubmitted = 0;

        AeSimpleSHA1.Utils1.TrustInvalidSslCertificates();
        CheckMobiService.getInstance().SetSecretKey("AAAAAAAA-AAAA-BBBB-AAAA-IIIIIIIIIIIA");

        numberSubmission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isInternetPresent = isNetworkAvailable();
                if (isInternetPresent) {
                    mobileNumber = phoneNumberEditText.getText().toString();
                    String params = mobileNumber;
                    if (params.length() == 10) {
                        mNoVerificationSource = SOURCE_MOBILE;
                        RegisterOnServer(params);
                    } else {
                        AeSimpleSHA1.Utils1.ShowMessageBox(new AlertDialog.Builder(Registration.this), "Error", "Please provide a valid mobile number");
                        DismissKeyboard();
                    }
                } else {
                    AeSimpleSHA1.Utils1.ShowMessageBox(new AlertDialog.Builder(Registration.this), "Error", "Internet not available , Please try again");
                }
            }
        });

        resendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resendFlag = 1;
                pinStep = false;
                validationTypeFlag = 0;
                OnClickValidate();
                ShowLoadingMessage(true);
            }
        });

        rcli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pinStep = false;
                validationTypeFlag = 1;
                OnClickValidate();
            }
        });

        TrueButton trueButton = (TrueButton) findViewById(R.id.com_truecaller_android_sdk_truebutton);
        boolean isTrueBtnUsable = trueButton.isUsable();
        if (isTrueBtnUsable) {

            mTrueClient = new TrueClient(this, this);
            trueButton.setTrueClient(mTrueClient);
        } else {

            trueButton.setVisibility(View.GONE);
        }
    }

    private void RegisterOnServer(String number) {

        final String params = number;
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Utils.RestApiPathUrl).build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
        ApiRequestModel requestModel = new ApiRequestModel();
        ApiRequestModel.Input input = requestModel.new Input(params);
        apiSuggestionsService.availability(input, new Callback<ApiResponseModel.ResponseAvailability>() {

            @Override
            public void success(ApiResponseModel.ResponseAvailability responseAvailability, retrofit.client.Response response) {
                int success = responseAvailability.error;
                if (success == 1) {
                    int availabilityCode = responseAvailability.code;
                    RegisterCallProcessing(availabilityCode);
                } else
                    RegisterCallProcessing(3);
            }

            @Override
            public void failure(final RetrofitError error) {
                RegisterCallProcessing(0);
            }
        });
    }

    private void RegisterCallProcessing(int availabilityCode) {

        switch (availabilityCode) {
            case 0:
                Toast.makeText(getApplicationContext(), "Some error in connecting with server occurred, Try Again", Toast.LENGTH_SHORT).show();
                DismissKeyboard();
                phoneNumberEditText.setText("");
                break;
            case 1:
                numberVerificationLayout.setVisibility(View.GONE);
                this.imageForPhone.setBackgroundResource(R.drawable.code);
                pinVerificationLayout.setVisibility(View.VISIBLE);
                retryVerificationLayout.setVisibility(View.VISIBLE);
                OnClickValidate();
                break;
            case 2:
                //seller already registered with unique user name choosen (in our case presently its phone number)
                Toast.makeText(getApplicationContext(), "Seller with this phone number already registered. Please try to login or call support for password", Toast.LENGTH_SHORT).show();
                DismissKeyboard();
                phoneNumberEditText.setText("");
                break;
            case 3:
                //some error on server
                Toast.makeText(getApplicationContext(), "Some error on server occured, Try Again", Toast.LENGTH_SHORT).show();
                DismissKeyboard();
                phoneNumberEditText.setText("");
                break;
            case 4:
                //some other errors
            default:
                // also considered as error
                Toast.makeText(getApplicationContext(), "Some other Error occured. We'll figure it out and fix this soon.", Toast.LENGTH_SHORT).show();
                DismissKeyboard();
                phoneNumberEditText.setText("");
        }
    }

    private void reverseCli(String key, String hash) {
        this.validationKey = key;
        this.callerid_hash = hash;

        timer = new Timer();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {

                    @Override
                    public void run() {
                        StopReverseCliTimer();
                        if (validationTypeFlag == 1 || pinsubmitted == 1)
                            ShowLoadingMessage(false);
                        Toast.makeText(Registration.this, "Error : Validation failed!", Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    }
                });
            }
        };

        timer.schedule(timerTask, 15000);
    }

    protected void onDestroy() {
        unregisterReceiver(receiver);
        super.onDestroy();
    }

    private ValidationType GetCurrentValidationType() {
        if (validationTypeFlag == 1)
            return ValidationType.REVERSE_CLI;
        else
            return ValidationType.SMS;
    }

    private void OnClickValidate() {
        switch (mNoVerificationSource){
            case SOURCE_MOBILE:
                if (!AeSimpleSHA1.Utils1.isNetworkAvailable(this)) {
                    AeSimpleSHA1.Utils1.ShowMessageBox(new AlertDialog.Builder(this), "Error", "No internet connection available!");
                    return;
                }

                if (!this.pinStep) {
                    if (this.phoneNumberEditText.getText().length() == 0) {
                        AeSimpleSHA1.Utils1.ShowMessageBox(new AlertDialog.Builder(this), "Invalid number", "Please provide a valid number");
                        return;
                    }

                    DismissKeyboard();
                    if (validationTypeFlag == 1 || pinsubmitted == 1)
                        ShowLoadingMessage(true);

                    CheckMobiService.getInstance().RequestValidation(GetCurrentValidationType(), ("+91" + this.phoneNumberEditText.getText().toString()), new AsyncResponse() {
                        @Override
                        public void OnRequestCompleted(int httpStatus, Map<String, Object> result, String error) {
                            System.out.println("Status= " + httpStatus + " result= " + (result != null ? result.toString() : "null") + " error=" + error);

                            if (httpStatus == CheckMobiService.STATUS_SUCCESS && result != null) {
                                key = String.valueOf(result.get("id"));
                                String type = String.valueOf(result.get("type"));
                                pin_hash = String.valueOf(result.get("pin_hash"));

                                @SuppressWarnings("unchecked")
                                Map<String, Object> validation_info = (Map<String, Object>) result.get("validation_info");

                                if (validationTypeFlag == 0)
                                    PerformPinValidation(key);
                                else
                                    reverseCli(key, pin_hash);
                            } else {
                                if (validationTypeFlag == 1 || pinsubmitted == 1)
                                    ShowLoadingMessage(false);
                                HandleValidationServiceError(httpStatus, result, error);
                            }
                        }
                    });
                } else {
                    CheckMobiService.getInstance().VerifyPin(this.validationKey, smsCode, new AsyncResponse() {
                        @Override
                        public void OnRequestCompleted(int httpStatus, Map<String, Object> result, String error) {
                            if (validationTypeFlag == 1 || pinsubmitted == 1)
                                ShowLoadingMessage(false);

                            if (httpStatus == CheckMobiService.STATUS_SUCCESS && result != null) {
                                Boolean validated = (Boolean) result.get("validated");

                                if (!validated) {
                                    AeSimpleSHA1.Utils1.ShowMessageBox(new AlertDialog.Builder(Registration.this), "Error", "Invalid PIN!");
                                    pinEditText.setText("");
                                    return;
                                }
                                Toast.makeText(Registration.this, "Verification Completed.", Toast.LENGTH_SHORT).show();
                                Intent in = new Intent(Registration.this, RegisterActivity.class);
                                in.putExtra(RegisterActivity.PHONE_NUMBER_TAG, mobileNumber);
                                in.putExtra(RegisterActivity.VERIFICATION_SOURCE_TAG, mNoVerificationSource);
                                in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                smsCode = "";
                                pinsubmitted = 1;
                                startActivity(in);
                                Registration.this.finish();
                            } else {
                                HandleValidationServiceError(httpStatus, result, error);
                            }
                        }
                    });
                }
                break;
            case SOURCE_TRUECALLER:

                Intent in = new Intent(Registration.this, RegisterActivity.class);
                in.putExtra(RegisterActivity.PHONE_NUMBER_TAG, mobileNumber);
                in.putExtra(RegisterActivity.VERIFICATION_SOURCE_TAG, mNoVerificationSource);
                in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(in);
                Registration.this.finish();
                break;
        }

    }

    private void PerformPinValidation(String key) {

        this.pinStep = true;
        this.validationKey = key;

        if (validationTypeFlag == 1 || pinsubmitted == 1 || resendFlag == 1)
            ShowLoadingMessage(false);

        pinSubmission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isInternetPresent = isNetworkAvailable();
                if (isInternetPresent) {
                    smsCode = pinEditText.getText().toString();
                    pinsubmitted = 1;
                    ShowLoadingMessage(true);
                    OnClickValidate();
                } else {
                    AeSimpleSHA1.Utils1.ShowMessageBox(new AlertDialog.Builder(Registration.this), "Error", "Internet not available , Please try again");
                }
            }
        });

    }

    private void HandleValidationServiceError(int httpStatus, Map<String, Object> body, String error) {
        System.out.println("HandleValidationServiceError status= " + httpStatus + " body= " + (body != null ? body.toString() : "null") + " error=" + error);

        if (body != null) {
            Number err = (Number) body.get("code");
            String error_message;

            switch (ErrorCode.get(err.intValue())) {
                case ErrorCodeInvalidPhoneNumber:
                    error_message = "Invalid phone number. Please provide the number in E164 format.";
                    break;

                default:
                    error_message = "Service unavailable. Please try later.";
            }

            AeSimpleSHA1.Utils1.ShowMessageBox(new AlertDialog.Builder(this), "Error", error_message);
        } else
            AeSimpleSHA1.Utils1.ShowMessageBox(new AlertDialog.Builder(this), "Error", "Service unavailable. Please try later.");
    }

    private void ShowLoadingMessage(boolean show) {
        if (show) {
            if (this.loadingDialog == null)
                this.loadingDialog = new WeakReference<>(ProgressDialog.show(Registration.this, "", "Please wait...", true));
        } else {
            if (this.loadingDialog != null) {
                this.loadingDialog.get().dismiss();
                this.loadingDialog = null;
            }
        }
    }

    private void DismissKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(phoneNumberEditText.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(pinEditText.getWindowToken(), 0);
    }

    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        smsCode = "";
        pinsubmitted = 1;
        this.finish();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onSuccesProfileShared(@NonNull TrueProfile trueProfile) {
        String contact = trueProfile.phoneNumber;
        if (contact.length() == 10) {
            mobileNumber = contact;
        } else if (contact.length() > 10) {
            mobileNumber = contact.substring(contact.length() - 10);
        } else {
            Toast.makeText(this, "Mobile Number can not be less than 10 characters", Toast.LENGTH_LONG).show();
            throw new IllegalArgumentException("Mobile Number is less than 10 characters!");
        }
        mNoVerificationSource = SOURCE_TRUECALLER;
        RegisterOnServer(contact);
    }

    @Override
    public void onFailureProfileShared(@NonNull TrueError trueError) {
        String msg = "";
        switch (trueError.getErrorType()) {
            case TRUE_ERROR_TYPE_INTERNAL:
                msg = "Truecaller internal error,not recoverable";
                break;
            case TRUE_ERROR_TYPE_NETWORK:
                msg = "No current active internet connection on the device";
                break;
            case TRUE_ERROR_TYPE_USER_DENIED:
                msg = "User denied sharing his/her profile details with the partner";
                break;
            case TRUE_ERROR_TYPE_UNAUTHORIZED_USER:
                msg = "User does not have a Truecaller account";
                break;
            case TRUE_ERROR_TYPE_UNAUTHORIZED_PARTNER:
                msg = "We cannot authenticate the partner(things that can be wrong: partnerKey, certificate or package)";
                break;
            default:
                break;
        }

        Toast.makeText(this, "Failed sharing - Reason: " + msg, Toast.LENGTH_LONG).show();

    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (null != mTrueClient && mTrueClient.onActivityResult(requestCode, resultCode, data)) {

            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
