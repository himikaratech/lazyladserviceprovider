package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiRequestModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiResponseModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiSuggestionService;

/**
 * Created by Amud on 17/02/16.
 */
public class RequestPaymentClass extends ActionBarActivity {

    TextView m_hardCodedText;
    TextView m_requestedAmount;

    Button m_requestPayment;

    Double requested_amount;

    String m_spCode;

    int m_priceDiffChecked;
    int m_discountChecked;
    int m_walletChecked;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.request_payment_layout);
        Intent intent= getIntent();
        m_priceDiffChecked=0;
        m_discountChecked=0;
        m_walletChecked=0;
        requested_amount=intent.getDoubleExtra(LazyCosntant.REQUESTED_AMOUNT_CONSTANT, 0.0);
        m_priceDiffChecked=intent.getIntExtra(LazyCosntant.PRICE_DIFFERENCE_CONSTANT, 0);
        m_walletChecked=intent.getIntExtra(LazyCosntant.WALLET_PAYMENT_CONSTANT, 0);
        m_discountChecked=intent.getIntExtra(LazyCosntant.DISCOUNT_CONSTANT, 0);

        Log.d("requested_amount",requested_amount+"");
        Log.d("m_priceDiffChecked",m_priceDiffChecked+"");
        Log.d("m_walletChecked",m_walletChecked+"");
        Log.d("m_discountChecked",m_discountChecked+"");


        m_hardCodedText=(TextView)findViewById(R.id.hard_codded_text);
        m_requestedAmount=(TextView)findViewById(R.id.requested_amount);

        m_requestedAmount.setText("₹ "+String.valueOf(requested_amount));

        m_hardCodedText.setText(getResources().getString(R.string.settle_up_instruction));
        m_requestPayment=(Button)findViewById(R.id.request_payment);

        m_requestPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(requested_amount==0.0)
                {
                    Toast.makeText(RequestPaymentClass.this,"Please select some payments",Toast.LENGTH_LONG).show();
                    RequestPaymentClass.this.finish();
                }
                else
                requestPayment();
            }
        });



    }


    private void requestPayment() {


        Cursor m_cursor = getContentResolver().query(
                LazyLadServiceProviderContract.UserDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    m_spCode = m_cursor.getString(1);
                    m_cursor.moveToNext();
                }
            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }
        if(m_cursor != null)m_cursor.close();

        final ProgressDialog pDialog;
        pDialog = new ProgressDialog(this, R.style.MyTheme);
        pDialog.setMessage("Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        pDialog.show();

        String user_code = m_spCode;
        String owner_type = "1";

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://www.angulartechnologies.com").build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
        ApiRequestModel requestModel = new ApiRequestModel();
        ApiRequestModel.RequestPaymentInput input = requestModel.new RequestPaymentInput(m_spCode, m_priceDiffChecked,m_walletChecked,m_discountChecked);
        apiSuggestionsService.requestPayment(input, new Callback<ApiResponseModel.ResponseDataPayment>() {

            @Override
            public void success(ApiResponseModel.ResponseDataPayment output, retrofit.client.Response response) {
                boolean success = output.success;
                if (success == true) {
                    Toast.makeText(RequestPaymentClass.this,"Sussessfully Requested",Toast.LENGTH_LONG).show();
                    RequestPaymentClass.this.finish();
                }
                if (pDialog.isShowing())
                    pDialog.dismiss();
            }

            @Override
            public void failure(final RetrofitError error) {
                Log.e("Failure", error + " retro");
                if (pDialog.isShowing())
                    pDialog.dismiss();
            }
        });
    }
}
