package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;

import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.UserDetails;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiRequestModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiResponseModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiSuggestionService;

/**
 * Created by Sakshi Gupta on 26-06-2015.
 */
public class ScheduledOrders extends Fragment {
    private Boolean dataLoading = false;

    private String m_servProvId;
    private ArrayList<ScheduledOrdersSnippet> m_scheduledOrdersList = null;
    private ScheduledOrdersAdapter m_scheduledOrdersAdapter = null;
    private ListView m_scheduledOrdersDetails;

    public static final String EXTRA_MESSAGE = "message";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTracker();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.scheduled_orders, container, false);

        if (!Utils.isNetworkAvailable(getActivity())) {
            Toast.makeText(getActivity(), "Please Check Your Network Connectivity", Toast.LENGTH_SHORT).show();
        }

        Cursor m_cursor = getActivity().getContentResolver().query(UserDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    m_servProvId = m_cursor.getString(1);
                    m_cursor.moveToNext();
                }
            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }

        m_scheduledOrdersDetails = (ListView) rootView.findViewById(R.id.scheduled_orders_list_view);

        m_scheduledOrdersDetails.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) { // TODO Auto-generated method stub
                if (null == m_scheduledOrdersDetails)
                    return;

                int threshold = 1;
                int count = m_scheduledOrdersDetails.getCount();

                if (scrollState == SCROLL_STATE_IDLE) {
                    if (!dataLoading && m_scheduledOrdersDetails.getLastVisiblePosition() >= count - threshold) {
                        dataLoading = true;
                        // Execute LoadMoreDataTask AsyncTask
                        LoadScheduledOrders();
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                //leave this empty
            }
        });
        return rootView;
    }

    /**
     * Receiving push messages
     */
    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
            // Waking up mobile if it is sleeping
            WakeLocker.acquire(getActivity().getApplicationContext());
            newMessage = "love is life";
            /**
             * Take appropriate action on this message
             * depending upon your app requirement
             * For now i am just displaying it on the screen
             * */

            // Showing received message
            Toast.makeText(getActivity(), "New Message: " + newMessage, Toast.LENGTH_LONG).show();

            // Releasing wake lock
            WakeLocker.release();

            LoadScheduledOrders();

        }
    };

    @Override
    public void onStart() {
        super.onStart();
        m_scheduledOrdersList = null;
        LoadScheduledOrders();
    }

    private void LoadScheduledOrders() {

        final ProgressDialog pDialog;

        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Loading Orders. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        String orderId;
        if (m_scheduledOrdersList == null) {
            orderId = "0";
        } else {
            orderId = m_scheduledOrdersList.get(m_scheduledOrdersList.size() - 1).m_scheduledOrderCode;
        }
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Utils.RestApiPathUrl).build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
        ApiRequestModel requestModel = new ApiRequestModel();
        ApiRequestModel.InputForOrders scheduled = requestModel. new InputForOrders(m_servProvId, orderId);
        apiSuggestionsService.scheduledOrders(scheduled, new Callback<ApiResponseModel.ScheduledOrdersModel>() {

            @Override
            public void success(ApiResponseModel.ScheduledOrdersModel responseScheduledOrders, Response response) {
                int success = responseScheduledOrders.error;
                if (success == 1) {

                    if (null != responseScheduledOrders.scheduledOrdersList && !responseScheduledOrders.scheduledOrdersList.isEmpty()) {

                        if (m_scheduledOrdersList == null) {
                            m_scheduledOrdersList = responseScheduledOrders.scheduledOrdersList;
                            m_scheduledOrdersAdapter = new ScheduledOrdersAdapter(getActivity(), m_scheduledOrdersList);
                            m_scheduledOrdersDetails.setAdapter((ListAdapter) m_scheduledOrdersAdapter);
                        } else {
                            m_scheduledOrdersList.addAll(responseScheduledOrders.scheduledOrdersList);
                            m_scheduledOrdersAdapter.notifyDataSetChanged();
                        }
                    }
                } else {
                }
                pDialog.dismiss();
                dataLoading = false;
            }

            @Override
            public void failure(final RetrofitError error) {
                pDialog.dismiss();
                dataLoading = false;
            }
        });
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getActivity().getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(this.getClass().getCanonicalName());

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }
}
