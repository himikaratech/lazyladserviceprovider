package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Sakshi Gupta on 26-06-2015.
 */
public class ScheduledOrdersAdapter extends BaseAdapter {

    private ArrayList<ScheduledOrdersSnippet> m_scheduledOrdersDetails;
    private Activity m_activity;

    public ScheduledOrdersAdapter(Activity activity, ArrayList<ScheduledOrdersSnippet> scheduledOrdersDetails) {
        m_activity = activity;
        m_scheduledOrdersDetails = scheduledOrdersDetails;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (m_scheduledOrdersDetails != null) {
            count = m_scheduledOrdersDetails.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (m_scheduledOrdersDetails != null) {
            return m_scheduledOrdersDetails.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_scheduledOrdersDetails != null) {
            return m_scheduledOrdersDetails.get(position).scheduledOrdersId;
        }
        return 0;
    }

    public static String GetHumanReadableDate(long epochSec, String dateFormatStr) {
        Date date = new Date(epochSec * 1000);
        SimpleDateFormat format = new SimpleDateFormat(dateFormatStr);
        return format.format(date);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (m_scheduledOrdersDetails != null) {

            final int positionFinal = position;
            final String scheduledOrderCode = ((ScheduledOrdersSnippet) getItem(position)).m_scheduledOrderCode;
            String scheduledOrderAddress = ((ScheduledOrdersSnippet) getItem(position)).m_address;
            int scheduledOrderAmount = ((ScheduledOrdersSnippet) getItem(position)).m_amount;
            String scheduledOrderTimePlaced = ((ScheduledOrdersSnippet) getItem(position)).m_timeOrderPlaced;
            Long engTime = ((ScheduledOrdersSnippet) getItem(position)).m_expectedTime;
            String scheduledOrderTimeExpected = GetHumanReadableDate(engTime, "E LLL dd, yyyy HH:mm");
            String scheduledOrderUserName = ((ScheduledOrdersSnippet) getItem(position)).m_userName;
            final String scheduledOrderPhoneNumber = ((ScheduledOrdersSnippet) getItem(position)).m_userPhoneNum;
            String orderStatus = ((ScheduledOrdersSnippet) getItem(position)).m_orderStatus;
            final String userCode = ((ScheduledOrdersSnippet) getItem(position)).m_userCode;
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.scheduled_orders_display_list, (ViewGroup) null);
            }
            TextView scheduledOrderCodeTextView = (TextView) convertView.findViewById(R.id.scheduled_order_code_textview);
            TextView scheduledOrderAddressTextView = (TextView) convertView.findViewById(R.id.scheduled_order_address_textview);
            TextView scheduledOrderAmountTextView = (TextView) convertView.findViewById(R.id.amount_scheduled_order_textview);
            TextView scheduledOrderTimePlacedTextView = (TextView) convertView.findViewById(R.id.scheduled_order_del_textview);
            TextView scheduledOrderTimeExpectedTextView = (TextView) convertView.findViewById(R.id.scheduled_order_exp_textview);
            TextView scheduledOrderUserNameTextView = (TextView) convertView.findViewById(R.id.scheduled_order_username_textview);
            TextView scheduledOrderUserPhoneTextView = (TextView) convertView.findViewById(R.id.scheduled_order_number_textview);
            LinearLayout scheduledOrderDetailView = (LinearLayout) convertView.findViewById(R.id.detail_layout);
            TextView scheduledOrderNumber = (TextView) convertView.findViewById(R.id.order_number);
            TextView viewDetail = (TextView) convertView.findViewById(R.id.view_detail_textview);
            LinearLayout numberLayoutView = (LinearLayout) convertView.findViewById(R.id.number_layout);

            Typeface typeFace = Typeface.createFromAsset(m_activity.getAssets(), "fonts/Roboto_Medium.ttf");
            Typeface typeFace1 = Typeface.createFromAsset(m_activity.getAssets(), "fonts/Roboto_Regular.ttf");
            Typeface typeFace2 = Typeface.createFromAsset(m_activity.getAssets(), "fonts/Roboto_Light.ttf");

            scheduledOrderNumber.setTypeface(typeFace);
            scheduledOrderCodeTextView.setTypeface(typeFace);
            scheduledOrderAmountTextView.setTypeface(typeFace1);
            scheduledOrderTimePlacedTextView.setTypeface(typeFace1);
            scheduledOrderTimeExpectedTextView.setTypeface(typeFace1);
            scheduledOrderUserNameTextView.setTypeface(typeFace);
            scheduledOrderAddressTextView.setTypeface(typeFace1);
            scheduledOrderUserPhoneTextView.setTypeface(typeFace2);
            viewDetail.setTypeface(typeFace2);

            scheduledOrderCodeTextView.setText(scheduledOrderCode);
            scheduledOrderAddressTextView.setText(scheduledOrderAddress);
            scheduledOrderAmountTextView.setText("₹ " + Integer.toString(scheduledOrderAmount));
            scheduledOrderTimePlacedTextView.setText(scheduledOrderTimePlaced);
            scheduledOrderTimeExpectedTextView.setText(scheduledOrderTimeExpected);
            scheduledOrderUserNameTextView.setText(scheduledOrderUserName);
            scheduledOrderUserPhoneTextView.setText(scheduledOrderPhoneNumber);

            numberLayoutView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel: " + scheduledOrderPhoneNumber));
                    m_activity.startActivity(callIntent);
                }
            });

            scheduledOrderDetailView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if ((((ScheduledOrdersSnippet) getItem(positionFinal)).m_orderStatus).equals("Pending"))
                        startCurrentOrderDetilsActivity(scheduledOrderCode, userCode);

                    else if ((((ScheduledOrdersSnippet) getItem(positionFinal)).m_orderStatus).equals("Confirmed"))
                        startPendingOrderDetilsActivity(scheduledOrderCode, userCode);
                }
            });
        }
        return convertView;
    }

    private void startCurrentOrderDetilsActivity(String currentOrderCode, String userCode) {
        Intent intent = new Intent(m_activity, CurrentOrderDetails.class);
        String data[] = {currentOrderCode, userCode};
        intent.putExtra("data_send", data);
        m_activity.startActivity(intent);
    }

    private void startPendingOrderDetilsActivity(String pendingOrderCode, String userCode) {
        Intent intent = new Intent(m_activity, PendingOrderDetails.class);
        String data[] = {pendingOrderCode, userCode};
        intent.putExtra("data_send_pending_details", data);
        m_activity.startActivity(intent);
    }
}
