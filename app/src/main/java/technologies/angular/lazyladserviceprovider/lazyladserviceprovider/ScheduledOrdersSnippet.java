package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sakshi Gupta on 26-06-2015.
 */
public class ScheduledOrdersSnippet {


    public int scheduledOrdersId;
    @SerializedName("scheduled_order_code")
    public String m_scheduledOrderCode;
    @SerializedName("delivery_address")
    public String m_address;
    @SerializedName("user_code")
    public String m_userCode;
    @SerializedName("order_time")
    public String m_timeOrderPlaced;
    @SerializedName("expected_del_time")
    public Long m_expectedTime;
    @SerializedName("total_amount")
    public int m_amount;
    @SerializedName("user_name")
    public String m_userName;
    @SerializedName("user_phone_number")
    public String m_userPhoneNum;
    @SerializedName("order_status")
    public String m_orderStatus;

    public ScheduledOrdersSnippet() {
    }

    public ScheduledOrdersSnippet(int id, String scheduledOrderCode, String customer_address, String user_code, String time_order_placed, Long expected_time, int total_amount, String userName, String userPhoneNum, String order_status) {
        scheduledOrdersId = id;
        m_scheduledOrderCode = scheduledOrderCode;
        m_address = customer_address;
        m_userCode = user_code;
        m_timeOrderPlaced = time_order_placed;
        m_expectedTime = expected_time;
        m_amount = total_amount;
        m_userName = userName;
        m_userPhoneNum = userPhoneNum;
        m_orderStatus = order_status;
    }
}
