package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;

/**
 * Created by Sakshi Gupta on 24-06-2015.
 */
public class SearchResultsActivity extends ActionBarActivity {

    private DbHelperForDelivered mDbHelper;
    private int numberOfSearchedOrders;
    private ListView deliveredOrdersDetails;
    private Toolbar toolbar;
    private DeliveredOrdersAdapter deliveredOrdersAdapter;
    private MenuItem searchMenuItem;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_delivered_orders);

        deliveredOrdersDetails = (ListView) findViewById(R.id.search_orders_list_view);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        mDbHelper = new DbHelperForDelivered(this);
        mDbHelper.open();

        handleIntent(getIntent());
        setTracker();
    }


    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            displayResults(query);
        }
    }


    private void displayResults(String query) {

        Cursor cursor = mDbHelper.searchByInputText((query != null ? query : "@@@@"));

        if (cursor != null && cursor.getCount() > 0) {
            numberOfSearchedOrders = cursor.getCount();
            ArrayList<DeliveredOrdersSnippet> deliveredOrdersList = new ArrayList<DeliveredOrdersSnippet>(numberOfSearchedOrders);

            for (int i = 0; i < numberOfSearchedOrders; i++) {
                int deliveredOrderId = i + 1;
                String code = cursor.getString(cursor.getColumnIndex("code"));
                String add = cursor.getString(cursor.getColumnIndex("address"));
                String time = cursor.getString(cursor.getColumnIndex("time"));
                double amt = cursor.getDouble(cursor.getColumnIndex("amount"));
                String user = cursor.getString(cursor.getColumnIndex("name"));
                String userPhone = cursor.getString(cursor.getColumnIndex("number"));
                DeliveredOrdersSnippet searchedOrderObj = new DeliveredOrdersSnippet(deliveredOrderId, code, add, time, amt, user, userPhone);
                deliveredOrdersList.add(searchedOrderObj);
                cursor.moveToNext();
            }
            cursor.close();

            deliveredOrdersAdapter = new DeliveredOrdersAdapter(SearchResultsActivity.this, deliveredOrdersList);
            deliveredOrdersDetails.setAdapter((ListAdapter) deliveredOrdersAdapter);
        } else {
            Toast.makeText(this, "Sorry! no matching items found.", Toast.LENGTH_SHORT).show();
        }
    }

    public MenuItem getSearchMenuItem() {
        return searchMenuItem;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_delivered_orders, menu);

        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchMenuItem = menu.findItem(R.id.search);
        SearchView searchView =
                (SearchView) searchMenuItem.getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                MenuItem searchMenuItem = getSearchMenuItem();
                if (searchMenuItem != null) {
                    SearchView sv = (SearchView) searchMenuItem.getActionView();
                    sv.onActionViewCollapsed();

                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }

        });
        return super.onCreateOptionsMenu(menu);
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(this.getClass().getCanonicalName());

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }
}
