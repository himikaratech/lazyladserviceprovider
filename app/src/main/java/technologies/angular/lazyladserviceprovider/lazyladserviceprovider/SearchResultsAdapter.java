package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by user on 14-07-2015.
 */
public class SearchResultsAdapter extends BaseAdapter {

    private Activity m_activity;
    private ArrayList<SearchResultsSnippet> m_searchResultItemsDetails;

    private class ViewHolder {
        TextView itemNameTextView;
        TextView itemDescTextView;
        TextView itemMrpTextView;
        TextView itemPriceText;
        TextView itemCodeTextView;
        EditText itemCostEditText;
        ImageButton changePriceButton;
        ImageView itemImageView;
        RadioGroup itemAvailabilityStatus;
        RadioButton availableRadioButton;
        RadioButton nonAvailableRadioButton;
        RadioButton outOfStockRadioButton;
    }

    public SearchResultsAdapter(Activity activity, ArrayList<SearchResultsSnippet> searchResultItemDetails) {
        m_activity = activity;
        m_searchResultItemsDetails = searchResultItemDetails;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (m_searchResultItemsDetails != null) {
            count = m_searchResultItemsDetails.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (m_searchResultItemsDetails != null) {
            return m_searchResultItemsDetails.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_searchResultItemsDetails != null) {
            return Long.parseLong(m_searchResultItemsDetails.get(position).m_itemId);
        }
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (m_searchResultItemsDetails != null) {

            final ViewHolder holder;

            String itemCode = ((SearchResultsSnippet) getItem(position)).m_itemCode;
            String itemName = ((SearchResultsSnippet) getItem(position)).m_itemName;
            String itemImageAdd = ((SearchResultsSnippet) getItem(position)).m_itemImgAddress;
            String itemDesc = ((SearchResultsSnippet) getItem(position)).m_itemDesc;
            double itemCost = ((SearchResultsSnippet) getItem(position)).m_itemCost;

            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.search_result_activity_list_view, (ViewGroup) null);
                holder = new ViewHolder();

                holder.itemNameTextView = (TextView) convertView.findViewById(R.id.item_name_textview);
                holder.itemDescTextView = (TextView) convertView.findViewById(R.id.item_desc_textview);
                holder.itemMrpTextView = (TextView) convertView.findViewById(R.id.item_mrp_textview);
                holder.itemCodeTextView = (TextView) convertView.findViewById(R.id.item_code_textview);
                holder.itemCostEditText = (EditText) convertView.findViewById(R.id.item_cost_editview);
                holder.itemAvailabilityStatus = (RadioGroup) convertView.findViewById(R.id.availability_status);
                holder.changePriceButton = (ImageButton) convertView.findViewById(R.id.change_price_button);
                holder.availableRadioButton = (RadioButton) convertView.findViewById(R.id.radio_available);
                holder.nonAvailableRadioButton = (RadioButton) convertView.findViewById(R.id.radio_nAvailable);
                holder.outOfStockRadioButton = (RadioButton) convertView.findViewById(R.id.radio_outOfStock);
                holder.itemPriceText = (TextView) convertView.findViewById(R.id.price_text);
                holder.itemImageView = (ImageView) convertView.findViewById(R.id.item_image);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
                holder.itemAvailabilityStatus.setOnCheckedChangeListener(null);
            }

            holder.availableRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
            holder.nonAvailableRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
            holder.outOfStockRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
            holder.availableRadioButton.setButtonDrawable(R.drawable.inactive);
            holder.nonAvailableRadioButton.setButtonDrawable(R.drawable.inactive);
            holder.outOfStockRadioButton.setButtonDrawable(R.drawable.inactive);


            switch (m_searchResultItemsDetails.get(position).m_itemStatus) {
                default:
                case 0:
                    holder.itemAvailabilityStatus.clearCheck();
                    break;
                case 1:
                    holder.itemAvailabilityStatus.check(R.id.radio_available);
                    holder.availableRadioButton.setTextColor(Color.parseColor("#ffffff"));
                    holder.availableRadioButton.setButtonDrawable(R.drawable.active);
                    holder.nonAvailableRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
                    holder.outOfStockRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
                    holder.nonAvailableRadioButton.setButtonDrawable(R.drawable.inactive);
                    holder.outOfStockRadioButton.setButtonDrawable(R.drawable.inactive);

                    break;
                case 2:
                    holder.itemAvailabilityStatus.check(R.id.radio_nAvailable);
                    holder.nonAvailableRadioButton.setTextColor(Color.parseColor("#ffffff"));
                    holder.nonAvailableRadioButton.setButtonDrawable(R.drawable.active);
                    holder.availableRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
                    holder.outOfStockRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
                    holder.availableRadioButton.setButtonDrawable(R.drawable.inactive);
                    holder.outOfStockRadioButton.setButtonDrawable(R.drawable.inactive);

                    break;
                case 3:
                    holder.itemAvailabilityStatus.check(R.id.radio_outOfStock);
                    holder.outOfStockRadioButton.setTextColor(Color.parseColor("#ffffff"));
                    holder.outOfStockRadioButton.setButtonDrawable(R.drawable.active);
                    holder.availableRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
                    holder.nonAvailableRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
                    holder.availableRadioButton.setButtonDrawable(R.drawable.inactive);
                    holder.nonAvailableRadioButton.setButtonDrawable(R.drawable.inactive);

                    break;
            }

            Typeface typeFace = Typeface.createFromAsset(m_activity.getAssets(), "fonts/Roboto_Regular.ttf");
            holder.itemNameTextView.setTypeface(typeFace);
            holder.itemMrpTextView.setTypeface(typeFace);
            holder.itemPriceText.setTypeface(typeFace);
            holder.itemCostEditText.setTypeface(typeFace);

            holder.itemNameTextView.setText(itemName);
            holder.itemDescTextView.setText(itemDesc);
            holder.itemMrpTextView.setText(Double.toString(itemCost));
            holder.itemCodeTextView.setText(itemCode);

            holder.itemNameTextView.setText(itemName);
            holder.itemDescTextView.setText(itemDesc);
            holder.itemMrpTextView.setText(Double.toString(itemCost));
            holder.itemCodeTextView.setText(itemCode);

            if (itemImageAdd == null || itemImageAdd == "") itemImageAdd = "\"\"";
            Picasso.with(parent.getContext())
                    .load(itemImageAdd)
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.no_image)
                    .fit().centerInside()
                    .into(holder.itemImageView);

            holder.itemAvailabilityStatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    switch (checkedId) {
                        default:
                            m_searchResultItemsDetails.get(position).m_itemStatus = 0;
                            break;
                        case R.id.radio_available:
                            m_searchResultItemsDetails.get(position).m_itemStatus = 1;
                            holder.availableRadioButton.setTextColor(Color.parseColor("#ffffff"));
                            holder.availableRadioButton.setButtonDrawable(R.drawable.active);
                            holder.nonAvailableRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
                            holder.outOfStockRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
                            holder.nonAvailableRadioButton.setButtonDrawable(R.drawable.inactive);
                            holder.outOfStockRadioButton.setButtonDrawable(R.drawable.inactive);

                            break;
                        case R.id.radio_nAvailable:
                            m_searchResultItemsDetails.get(position).m_itemStatus = 2;
                            holder.nonAvailableRadioButton.setTextColor(Color.parseColor("#ffffff"));
                            holder.nonAvailableRadioButton.setButtonDrawable(R.drawable.active);
                            holder.availableRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
                            holder.outOfStockRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
                            holder.availableRadioButton.setButtonDrawable(R.drawable.inactive);
                            holder.outOfStockRadioButton.setButtonDrawable(R.drawable.inactive);

                            break;
                        case R.id.radio_outOfStock:
                            m_searchResultItemsDetails.get(position).m_itemStatus = 3;
                            holder.outOfStockRadioButton.setTextColor(Color.parseColor("#ffffff"));
                            holder.outOfStockRadioButton.setButtonDrawable(R.drawable.active);
                            holder.availableRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
                            holder.nonAvailableRadioButton.setTextColor(Color.parseColor("#b3b3b3"));
                            holder.availableRadioButton.setButtonDrawable(R.drawable.inactive);
                            holder.nonAvailableRadioButton.setButtonDrawable(R.drawable.inactive);

                            break;
                    }
                }
            });

            holder.changePriceButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    double cost;
                    String mystring = holder.itemCostEditText.getText().toString();
                    if (mystring.matches("^[0-9]*\\.?[0-9]*$") && !mystring.isEmpty()) {
                        cost = Double.parseDouble(mystring);
                        m_searchResultItemsDetails.get(position).m_itemCost = cost;
                        holder.itemMrpTextView.setText(Double.toString(cost));
                        m_searchResultItemsDetails.get(position).m_priceChanged = true;
                    }
                }
            });

        }
        return convertView;
    }
}
