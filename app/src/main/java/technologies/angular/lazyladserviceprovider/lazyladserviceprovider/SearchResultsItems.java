package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.ServiceTypeCategories;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.UserDetails;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.SearchResult;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiRequestModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiResponseModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiSuggestionService;

/**
 * Created by Sakshi Gupta on 09-07-2015.
 */
public class SearchResultsItems extends ActionBarActivity {

    private static final String TAG = "SearchResultsItem";

    private ListView m_itemDetailsListView;
    private ProgressDialog progressDialog;
    private Toolbar toolbar;
    private Intent m_searchIntent;
    private String m_query = null;
    private String m_itemCode;
    private ArrayList<SearchResultsSnippet> m_searchItemsDetailsArrayList;
    private SearchResultsAdapter m_searchResultsAdapter;
    private String JsonDataArray;

    public static String[] dummy = new String[]{"a", "b"};
    public static Uri deleteUri;
    public static Uri insertUri;
    public static Uri selectUri;
    public static Uri updateUri;

    public int items_type;
    private Button m_saveSelectedItemsButton;
    public static ArrayList<String> m_itemsCategoryNames;
    public static ArrayList<Integer> m_itemsCategoryCode;
    private String m_stCode;
    private String m_spCode;
    private AsyncTask m_async5;
    private Boolean doneLoading = true;
    private Boolean shallLoad = true;

    static {
        SearchResultsItems.selectUri = Uri.parse((String) ("content://technologies.angular.lazyladserviceprovider.lazyladserviceprovider.provider/SELECT"));
    }

    static {
        SearchResultsItems.deleteUri = Uri.parse((String) ("content://technologies.angular.lazyladserviceprovider.lazyladserviceprovider.provider/DELETE"));
    }

    static {
        SearchResultsItems.insertUri = Uri.parse((String) ("content://technologies.angular.lazyladserviceprovider.lazyladserviceprovider.provider/INSERT"));
    }

    static {
        SearchResultsItems.updateUri = Uri.parse((String) ("content://technologies.angular.lazyladserviceprovider.lazyladserviceprovider.provider/UPDATE"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_results_for_items);

        Intent intent;
        String value = getIntent().getStringExtra("items_type");
        items_type = Integer.parseInt(value);

        toolbar = (Toolbar) findViewById(R.id.items_toolbar);

        setSupportActionBar(toolbar);
        if (items_type == 1)
            getSupportActionBar().setTitle("Available Items");
        else if (items_type == 2)
            getSupportActionBar().setTitle("Non Available Items");
        else if (items_type == 3)
            getSupportActionBar().setTitle("Out of Stock Items");

        Cursor cursor = getContentResolver().query(ServiceTypeCategories.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (cursor != null && cursor.moveToFirst()) {
            m_itemsCategoryNames = new ArrayList<String>(cursor.getCount());
            m_itemsCategoryCode = new ArrayList<Integer>(cursor.getCount());
            for (int i = 0; i < cursor.getCount(); i++) {
                m_itemsCategoryNames.add(cursor.getString(3));
                m_itemsCategoryCode.add(Integer.parseInt(cursor.getString(2)));
                cursor.moveToNext();
            }
        }

        cursor = getContentResolver().query(UserDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (cursor != null && cursor.moveToFirst()) {
            if (cursor.getCount() != 0) {
                for (int i = 0; i < cursor.getCount(); i++) {
                    m_spCode = cursor.getString(1);
                    m_stCode = cursor.getString(4);
                    cursor.moveToNext();
                }
            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }
        if (cursor != null) cursor.close();

        m_itemDetailsListView = (ListView) findViewById(R.id.search_result_list_view);
        m_searchIntent = getIntent();

        handleIntent(m_searchIntent);

        m_saveSelectedItemsButton = (Button) findViewById(R.id.confirm_items_aval_button);
        m_saveSelectedItemsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveUpdatedItemDetails();
            }
        });

        if (Intent.ACTION_SEARCH.equals(m_searchIntent.getAction())) {
            m_itemDetailsListView.setOnScrollListener(new LL_ScrollListener());
        }
        setTracker();
    }

    private void saveUpdatedItemDetails() {

        JSONArray jsonObject = new JSONArray();
        for (int i = 0; i < m_searchItemsDetailsArrayList.size(); i++) {
            JSONObject smallJsonObject = new JSONObject();
            if (m_searchItemsDetailsArrayList.get(i).m_itemStatus == 1 || m_searchItemsDetailsArrayList.get(i).m_itemStatus == 2
                    || m_searchItemsDetailsArrayList.get(i).m_itemStatus == 3 || m_searchItemsDetailsArrayList.get(i).m_priceChanged) {
                String m_itemCode = m_searchItemsDetailsArrayList.get(i).m_itemCode;
                String m_itemName = m_searchItemsDetailsArrayList.get(i).m_itemName;
                String m_itemImgFlag = m_searchItemsDetailsArrayList.get(i).m_itemImgFlag;
                String m_itemImgAddress = m_searchItemsDetailsArrayList.get(i).m_itemImgAddress;
                String m_itemUnit = m_searchItemsDetailsArrayList.get(i).m_itemUnit;
                String m_itemShortDesc = m_searchItemsDetailsArrayList.get(i).m_itemShortDesc;
                String m_itemDesc = m_searchItemsDetailsArrayList.get(i).m_itemDesc;

                int m_itemStatus = m_searchItemsDetailsArrayList.get(i).m_itemStatus;
                if (m_itemStatus == 0) m_itemStatus = (SearchResultsItems.this).items_type;

                double m_itemCost = m_searchItemsDetailsArrayList.get(i).m_itemCost;
                String m_itemTypeCode = m_searchItemsDetailsArrayList.get(i).m_itemTypeCode;
                String m_itemQuantity = m_searchItemsDetailsArrayList.get(i).m_itemQuantity;
                String m_scCode = m_searchItemsDetailsArrayList.get(i).m_scCode;
                try {
                    smallJsonObject.put("item_code", m_itemCode);
                    smallJsonObject.put("item_name", m_itemName);
                    smallJsonObject.put("item_img_flag", m_itemImgFlag);
                    smallJsonObject.put("item_img_add", m_itemImgAddress);
                    smallJsonObject.put("item_unit", m_itemUnit);
                    smallJsonObject.put("item_short_desc", m_itemShortDesc);
                    smallJsonObject.put("item_desc", m_itemDesc);
                    smallJsonObject.put("item_status", m_itemStatus);
                    smallJsonObject.put("item_cost", m_itemCost);
                    smallJsonObject.put("item_type_code", m_itemTypeCode);
                    smallJsonObject.put("item_qunatity", m_itemQuantity);
                    smallJsonObject.put("item_sc_code", m_scCode);
                    jsonObject.put(smallJsonObject);
                    JsonDataArray = jsonObject.toString();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }


        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(SearchResultsItems.this);
        progressDialog.setMessage("Updating. Please wait...");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Utils.RestApiPathUrl).build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
        ApiRequestModel requestModel = new ApiRequestModel();
        ApiRequestModel.saveInSearch save = requestModel. new saveInSearch(JsonDataArray.toString(), m_spCode);
        apiSuggestionsService.save(save, new Callback<ApiResponseModel.ResponseSaveInSearch>() {
            @Override
            public void success(ApiResponseModel.ResponseSaveInSearch responseData, retrofit.client.Response response) {
                int success = responseData.error;
                if (success == 1) {
                    progressDialog.dismiss();
                    Toast.makeText(SearchResultsItems.this, "Updated Successfully", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                } else {
                }
            }

            @Override
            public void failure(final RetrofitError error) {
                Log.e(TAG, error.toString());
                progressDialog.dismiss();
            }
        });
    }

    private void getMoreSearchedItemsFromServerLimited() {
        Log.e(TAG, "loading");

        String previous_item_code;
        if (null == m_searchItemsDetailsArrayList || m_searchItemsDetailsArrayList.isEmpty())
            previous_item_code = "0";
        else
            previous_item_code = m_searchItemsDetailsArrayList.get(m_searchItemsDetailsArrayList.size() - 1).m_itemCode;

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Utils.RestApiPathUrl).build();

        ApiSuggestionService searchService = restAdapter.create(ApiSuggestionService.class);
        searchService.searchFeeds(m_spCode, m_stCode, items_type, m_query, previous_item_code, new Callback<ApiResponseModel.SearchModel>() {

            @Override
            public void success(ApiResponseModel.SearchModel searchResponse, Response response) {

                ArrayList<SearchResultsSnippet> itemsList = new ArrayList<SearchResultsSnippet>();
                itemsList = searchResponse.items_service_providers;
                if (null != itemsList && !itemsList.isEmpty()) {

                    m_async5 = new StoreItemsInSqliteandLoad(m_query).execute(itemsList);
                } else
                    shallLoad = false;
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e(TAG, error.toString());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        m_itemDetailsListView.setOnScrollListener(null);

        m_searchItemsDetailsArrayList = null;
        handleIntent(intent);

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            m_itemDetailsListView.setOnScrollListener(new LL_ScrollListener());
        }
    }

    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            m_query = intent.getStringExtra(SearchManager.QUERY);

            String previous_item_code;
            if (null == m_searchItemsDetailsArrayList || m_searchItemsDetailsArrayList.isEmpty())
                previous_item_code = "0";
            else
                previous_item_code = m_searchItemsDetailsArrayList.get(m_searchItemsDetailsArrayList.size() - 1).m_itemCode;

            final String selection = " CAST( " + SearchResult.COLUMN_ITEM_CODE + " as INTEGER ) > ? ";

            Cursor cursor = getContentResolver().query(SearchResult.CONTENT_URI,
                    null,
                    selection,
                    new String[]{String.valueOf(previous_item_code)},
                    null);

            processCursorData(cursor);

        } else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            // Handle a suggestions click (because the suggestions all use ACTION_VIEW)
            m_itemCode = intent.getDataString();

            final String sSearchSelection = SearchResult.COLUMN_ITEM_CODE + " = ? ";

            Cursor cursor = getContentResolver().query(SearchResult.buildSearchResultUri(SyncContentProvider.SEARCH_QUERY),
                    null,
                    sSearchSelection,
                    new String[]{m_itemCode},
                    String.valueOf(1)
            );
            processCursorData(cursor);
        }
    }

    private void processCursorData(Cursor cursor) {
        ArrayList<SearchResultsSnippet> searchItemsDetailsArrayList = new ArrayList<SearchResultsSnippet>(cursor.getCount());
        if (cursor != null && cursor.moveToFirst()) {
            if (cursor.getCount() > 0) {
                for (int i = 0; i < cursor.getCount(); i++) {
                    String itemid = cursor.getString(1);
                    String itemCode = cursor.getString(2);
                    String itemName = cursor.getString(3);
                    String item_img_flag = cursor.getString(4);
                    String item_img_address = cursor.getString(5);
                    String item_unit = cursor.getString(6);
                    double item_cost = cursor.getDouble(7);
                    String item_short_desc = cursor.getString(8);
                    String item_desc = cursor.getString(9);
                    int item_status = cursor.getInt(10);
                    String type = cursor.getString(11);
                    String quantity = cursor.getString(12);
                    String sc_code = cursor.getString(13);
                    SearchResultsSnippet m_itemSnipObj = new SearchResultsSnippet(itemid, itemCode, itemName, item_img_flag, item_img_address, item_unit, item_short_desc, item_desc, item_status, item_cost, type, quantity, sc_code);
                    searchItemsDetailsArrayList.add(m_itemSnipObj);
                    cursor.moveToNext();
                }
            }
        }
        if (cursor != null) cursor.close();

        if (null == m_searchItemsDetailsArrayList) {
            m_searchItemsDetailsArrayList = searchItemsDetailsArrayList;
            m_searchResultsAdapter = new SearchResultsAdapter(SearchResultsItems.this, m_searchItemsDetailsArrayList);
            m_itemDetailsListView.setAdapter((ListAdapter) m_searchResultsAdapter);
        } else {
            m_searchItemsDetailsArrayList.addAll(searchItemsDetailsArrayList);
            m_searchResultsAdapter.notifyDataSetChanged();
        }
        if (!doneLoading)
            doneLoading = true;
    }

    class StoreItemsInSqliteandLoad extends AsyncTask<ArrayList<SearchResultsSnippet>, Void, Cursor> {

        String searchText;

        public StoreItemsInSqliteandLoad(String searchText) {
            this.searchText = searchText;
        }

        @Override
        protected Cursor doInBackground(ArrayList<SearchResultsSnippet>... params) {
            ArrayList<SearchResultsSnippet> itemsList = params[0];

            storeItemsinSqliteForQuery(itemsList, searchText);

            String previous_item_code;
            if (null == m_searchItemsDetailsArrayList || m_searchItemsDetailsArrayList.isEmpty())
                previous_item_code = "0";
            else
                previous_item_code = m_searchItemsDetailsArrayList.get(m_searchItemsDetailsArrayList.size() - 1).m_itemCode;

            final String selection = " CAST( " + SearchResult.COLUMN_ITEM_CODE + " as INTEGER ) > ? ";

            Cursor cursor = getContentResolver().query(SearchResult.CONTENT_URI,
                    null,
                    selection,
                    new String[]{String.valueOf(previous_item_code)},
                    null);
            return cursor;
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            processCursorData(cursor);
        }
    }

    public void storeItemsinSqliteForQuery(ArrayList<SearchResultsSnippet> itemsList, String searchText) {

        String searchTextM = Utils.strForSqlite(searchText);
        for (int i = 0; i < itemsList.size(); i++) {
            String itemId = itemsList.get(i).m_itemId;
            String itemCode = itemsList.get(i).m_itemCode;
            String itemName = Utils.strForSqlite(itemsList.get(i).m_itemName);
            String itemImgFlag = itemsList.get(i).m_itemImgFlag;
            String itemImgAddress = Utils.strForSqlite(itemsList.get(i).m_itemImgAddress);
            String itemUnit = Utils.strForSqlite(itemsList.get(i).m_itemUnit);
            String itemShortDesc = Utils.strForSqlite(itemsList.get(i).m_itemShortDesc);
            String itemDesc = Utils.strForSqlite(itemsList.get(i).m_itemDesc);
            int itemStatus = itemsList.get(i).m_itemStatus;
            double itemCost = itemsList.get(i).m_itemCost;
            String itemTypeCode = itemsList.get(i).m_itemTypeCode;
            String itemQuantity = itemsList.get(i).m_itemQuantity;
            String scCode = itemsList.get(i).m_scCode;

            ContentValues values = new ContentValues();
            values.put(SearchResult.COLUMN_ITEM_ID, itemId);
            values.put(SearchResult.COLUMN_ITEM_CODE, itemCode);
            values.put(SearchResult.COLUMN_ITEM_NAME, itemName);
            values.put(SearchResult.COLUMN_ITEM_IMG_FLAG, itemImgFlag);
            values.put(SearchResult.COLUMN_ITEM_IMG_ADD, itemImgAddress);
            values.put(SearchResult.COLUMN_ITEM_UNIT, itemUnit);
            values.put(SearchResult.COLUMN_ITEM_COST, itemCost);
            values.put(SearchResult.COLUMN_ITEM_SHORT_DESC, itemShortDesc);
            values.put(SearchResult.COLUMN_ITEM_DESC, itemDesc);
            values.put(SearchResult.COLUMN_ITEM_STATUS, itemStatus);
            values.put(SearchResult.COLUMN_ITEM_TYPE, itemTypeCode);
            values.put(SearchResult.COLUMN_ITEM_QUANTITY_SELECTED, itemQuantity);
            values.put(SearchResult.COLUMN_SC_CODE, scCode);

            getContentResolver().insert(SearchResult.CONTENT_URI, values);
        }
    }

    @Override
    public void onBackPressed() {
        SearchResultsItems.this.finish();
        super.onBackPressed();
    }

    private class LL_ScrollListener implements AbsListView.OnScrollListener {
        int visibleThreshold = 1;
        int visibleItemCount;
        int totalItemCount;
        int firstVisibleItem;


        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == SCROLL_STATE_IDLE) {
                if (shallLoad && doneLoading && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + visibleThreshold)) {
                    doneLoading = false;
                    getMoreSearchedItemsFromServerLimited();
                }
            }
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            this.firstVisibleItem = firstVisibleItem;
            this.visibleItemCount = visibleItemCount;
            this.totalItemCount = totalItemCount;
            if (this.totalItemCount == 0)
                onScrollStateChanged(view, SCROLL_STATE_IDLE);
        }
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(this.getClass().getCanonicalName());

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }
}
