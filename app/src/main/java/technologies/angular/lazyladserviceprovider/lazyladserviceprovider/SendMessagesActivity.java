package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.SmsManager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by user on 29-07-2015.
 */
public class SendMessagesActivity extends Activity {
    private ListView m_personContactList;
    String referralCode;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact);

        Bundle b = getIntent().getExtras();
        referralCode = b.getString("referralCode");

        // Intent intent = new Intent();
        // referralCode = intent.getStringExtra("referralCode");

        if (referralCode.isEmpty() || referralCode == null) {
            Toast.makeText(SendMessagesActivity.this, "No Referral Code.", Toast.LENGTH_SHORT).show();

        } else {
            Log.i("referral Code Sms", referralCode);

            (findViewById(R.id.confirm_contacts_button)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new sendMessages().execute();
                }
            });

            m_personContactList = (ListView) findViewById(R.id.contacts_list_view);

            Cursor c = this.managedQuery(ContactsContract.Data.CONTENT_URI, null,
                    ContactsContract.Data.MIMETYPE + "=?", // condition
                    new String[]{ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE}, // value
                    ContactsContract.Contacts.DISPLAY_NAME + " ASC");

            ArrayList<Contact> contacts = new ArrayList<Contact>();

            while (c.moveToNext()) {
                int type = c.getInt(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                if (type == ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE) {
                    Contact con = new Contact(c.getString(c
                            .getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)), c.getString(c
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
                    contacts.add(con);
                }
            }
            m_personContactList.setAdapter(new ContactsAdapter(this, contacts));
        }
    }
    class sendMessages extends AsyncTask<String, Void, String> {

        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(SendMessagesActivity.this);
            pDialog.setMessage("Sending Messages. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            Boolean delieverable;
            String personMobile;
            ContactsAdapter adapter = (ContactsAdapter) m_personContactList.getAdapter();
            int totalChecked = adapter.cblist.size();
            Log.d("total contacts", totalChecked + "");
            SmsManager smsManager = SmsManager.getDefault();

            for (int i = 0; i < totalChecked; i++) {
                delieverable = adapter.cblist.get(i);
                if (delieverable) {

                    personMobile = ((Contact) (adapter.getItem(i))).getMobile();
                    Log.i("mobile number", personMobile);
                    smsManager.sendTextMessage(personMobile, null, "Now shop from my store using your mobile via LazyLad app. Download using referral code - " + Html.fromHtml("<b>" + referralCode + "</b>") + " and get free credits in your wallet. " + "bit.ly/shoplazy", null, null);
                }
            }
            return  null;
        }

        protected void onPostExecute(String result) {
            pDialog.dismiss();
            Toast.makeText(getApplicationContext(), "Messages sent", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(SendMessagesActivity.this, MainActivity.class);
            startActivity(intent);

        }
    }
}