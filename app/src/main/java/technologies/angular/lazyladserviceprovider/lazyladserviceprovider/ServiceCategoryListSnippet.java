package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Saurabh on 13/04/15.
 */
public class ServiceCategoryListSnippet {

    @SerializedName("id")
    public int m_id;

    @SerializedName("st_code")
    public String m_srvTypeCode;

    @SerializedName("sc_code")
    public String m_srvCatCode;

    @SerializedName("sc_name")
    public String m_srvCatName;

    public ServiceCategoryListSnippet() {
    }

    public ServiceCategoryListSnippet(int id, String srvTypeCode, String srvCatCode, String srvCatName) {
        m_id = id;
        m_srvTypeCode = srvTypeCode;
        m_srvCatCode = srvCatCode;
        m_srvCatName = srvCatName;
    }
}

