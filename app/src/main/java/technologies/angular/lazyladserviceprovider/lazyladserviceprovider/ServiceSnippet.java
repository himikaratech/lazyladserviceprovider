package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Paresh on 10/06/15.
 */
public class ServiceSnippet {

    @SerializedName("id")
    public int m_serviceId;
    @SerializedName("st_code")
    public String m_serviceCode;
    @SerializedName("st_name")
    public String m_serviceName;

    public ServiceSnippet() {

    }

    public ServiceSnippet(int id, String serviceCode, String serviceName) {
        m_serviceId = id;
        m_serviceCode = serviceCode;
        m_serviceName = serviceName;
    }

}
