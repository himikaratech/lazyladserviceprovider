package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiRequestModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiResponseModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiSuggestionService;

/**
 * Created by sakshigupta on 28/09/15.
 */
public class SettleUp extends ActionBarActivity{

    EditText amountToBeSettled;
    Button cancelSettlement;
    Button submitSettlement;
    TextView currentBalance;
    double amount, afterSettledAmount;
    private String user_code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settle_up);

        if (!Utils.isNetworkAvailable(this)) {
            Toast.makeText(SettleUp.this, "Please Check Your Network Connectivity", Toast.LENGTH_SHORT).show();
        }

        Cursor m_cursor = getContentResolver().query(
                LazyLadServiceProviderContract.UserDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    user_code = m_cursor.getString(1);
                    m_cursor.moveToNext();
                }
            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }
        if(m_cursor != null)m_cursor.close();

        Bundle b = getIntent().getExtras();
        amount = b.getDouble("amount");
        Log.i("amount received", amount+"");

        amountToBeSettled = (EditText)findViewById(R.id.amount_to_be_settled);
        cancelSettlement = (Button)findViewById(R.id.cancel_settle_up_amount);
        submitSettlement = (Button)findViewById(R.id.submit_settle_up_amount);
        currentBalance = (TextView)findViewById(R.id.current_bal);

        currentBalance.setText("₹ " + amount);
        cancelSettlement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettleUp.this, AccountDetails.class);
                //intent.getDoubleExtra("amount", amount);
                startActivity(intent);
            }
        });

        submitSettlement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String settleAmountInString = amountToBeSettled.getText().toString();
              //  double settledAmount = Double.parseDouble((amountToBeSettled.getText().toString()));
              //  if (Double.compare(settledAmount,Double.NaN)==0) {
                if(settleAmountInString.isEmpty() || settleAmountInString == null){
                    Toast.makeText(getApplicationContext(), "enter amount you want to settle up", Toast.LENGTH_SHORT).show();
                } else {
                    double settledAmount = Double.parseDouble(settleAmountInString);

                    if (settledAmount > 0 && settledAmount <= amount) {
                        afterSettledAmount = amount - settledAmount;
                        Log.i("afterSettledAmount", afterSettledAmount+"");
                        sendSettleUp();
                       // Toast.makeText(SettleUp.this, "Successful ", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SettleUp.this, "Please enter valid amount", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        setTracker();
    }

    private void sendSettleUp() {

        String t_type = "SETTLE_UP";

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://www.angulartechnologies.com/newserver/account").build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
        ApiRequestModel requestModel = new ApiRequestModel();
        ApiRequestModel.settleInput input = requestModel. new settleInput(t_type, user_code, afterSettledAmount);
        apiSuggestionsService.settle(input, new Callback<ApiResponseModel.settleOutput>() {

            @Override
            public void success(ApiResponseModel.settleOutput responseResult, retrofit.client.Response response) {
                boolean success = responseResult.success;
                Log.i("success settle up", success+"");
                if (success == true) {
                    String tId = responseResult.t_id;
                    amount = responseResult.amount;
                    Log.i("amount", amount+"");
                    Intent intent = new Intent(SettleUp.this, AccountDetails.class);
                  //  Bundle b = new Bundle();
                  //  b.putDouble("amount", amount);
                   // intent.putExtras(b);
                    startActivity(intent);
                }
            }
            @Override
            public void failure(final RetrofitError error) {
              //  Log.e("Failure", error+" retro");
            }
        });
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(this.getClass().getCanonicalName());

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }
}