package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiRequestModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiResponseModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiSuggestionService;
/**
 * Created by user on 30-07-2015.
 */
public class ShareVia extends ActionBarActivity {

    TextView referralSMSEditBox;
    TextView referralFacebookEditBox;
    TextView referralWhatsappEditBox;
    TextView referralTwitterEditBox;
    TextView referralTextView;
    private String m_servProvId;
    private String referralCode;
    private Toolbar m_toolbar;

    View shareLAyout;
    View referralLayout;
    View scrollReferral;
    String referralString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //FacebookSdk.sdkInitialize(getApplicationContext());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.share_via_screen);
        m_toolbar = (Toolbar) findViewById(R.id.toolbar);

        referralLayout = findViewById(R.id.referralLayout);
        shareLAyout = findViewById(R.id.shareLayout);
        scrollReferral = findViewById(R.id.scrollReferral);
        scrollReferral.setVisibility(View.GONE);
        setSupportActionBar(m_toolbar);

        referralSMSEditBox = (TextView) findViewById(R.id.referralSMS);
        referralFacebookEditBox = (TextView) findViewById(R.id.referralFacebook);
        referralWhatsappEditBox = (TextView) findViewById(R.id.referralWhatsapp);
        referralTwitterEditBox = (TextView) findViewById(R.id.referralTwitter);
        referralTextView = (TextView) findViewById(R.id.referralTextView);
        referralCode = new String();

        Cursor m_cursor = getContentResolver().query(
                LazyLadServiceProviderContract.UserDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    m_servProvId = m_cursor.getString(1);
                    m_cursor.moveToNext();
                }
            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }
        if (m_cursor != null) m_cursor.close();

        getReferralCode();
        setTracker();
    }

    private void getReferralCode() {

        String user_code = m_servProvId;
        String owner_type = "1";
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://www.angulartechnologies.com/newserver/referral").build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
        ApiRequestModel requestModel = new ApiRequestModel();
        ApiRequestModel.ReferralInput input = requestModel. new ReferralInput(user_code, owner_type);
        apiSuggestionsService.referralCode(input, new Callback<ApiResponseModel.ReferralOutput>() {

            @Override
            public void success(ApiResponseModel.ReferralOutput responseResult, retrofit.client.Response response) {
                boolean success = responseResult.success;
                if (success == true) {
                    scrollReferral.setVisibility(View.VISIBLE);
                    referralCode = responseResult.referral_code;
                    referralString = "Now shop from my store using your mobile via LazyLad app. Download using referral code - " + Html.fromHtml("<b>" + referralCode + "</b>") + " and get free credits in your wallet. " + "bit.ly/shoplazy";
                    referralTextView.setText(referralCode);
                }
            }

            @Override
            public void failure(final RetrofitError error) {
                Log.e("Failure", error+" retro");
            }
        });
    }

    public void SMS(View v) {
        Intent intent = new Intent(ShareVia.this, SendMessagesActivity.class);
        //intent.putExtra("referralCode", referralCode);
        Bundle b = new Bundle();
        b.putString("referralCode", referralCode);
        intent.putExtras(b);
        startActivity(intent);
    }

    public void FACEBOOK(View v) {

        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
       // shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Great App for daily needs");
        shareIntent.putExtra(Intent.EXTRA_TEXT, "https://www.facebook.com/lazyladapp");
       // shareIntent.putExtra(Intent.EXTRA_STREAM, "https://www.facebook.com/lazyladapp");

        PackageManager pm = v.getContext().getPackageManager();
        List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);

        boolean resolved = false;
        for (final ResolveInfo app : activityList) {
            if ((app.activityInfo.name).contains("facebook")) {
                final ActivityInfo activity = app.activityInfo;
                final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
                shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                shareIntent.setComponent(name);
                v.getContext().startActivity(shareIntent);
                resolved = true;
                break;
            }
        }

            if (resolved) {
                startActivity(shareIntent);
            } else {
                Intent i = new Intent();
                i.putExtra(Intent.EXTRA_TEXT, "https://www.facebook.com/lazyladapp");
                i.setAction(Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://www.facebook.com"));
                startActivity(i);
                Toast.makeText(this, "Twitter app isn't found", Toast.LENGTH_LONG).show();
            }
    }

    public void WHATSAPP(View v) {
        PackageManager pm = getPackageManager();
        try {

            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            String text = referralString;

            PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
            //Check if package exists or not. If not then code
            //in catch block will be called
            waIntent.setPackage("com.whatsapp");

            waIntent.putExtra(Intent.EXTRA_TEXT, text);
            startActivity(Intent.createChooser(waIntent, "Share with"));

        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(this, "whatsapp not Installed", Toast.LENGTH_SHORT)
                    .show();
        }
    }


    public void TWITTER(View v) {
        Intent tweetIntent = new Intent(Intent.ACTION_SEND);
        tweetIntent.putExtra(Intent.EXTRA_TEXT, referralString);
        tweetIntent.setType("text/plain");

        PackageManager packManager = getPackageManager();
        List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(tweetIntent, PackageManager.MATCH_DEFAULT_ONLY);

        boolean resolved = false;
        for (ResolveInfo resolveInfo : resolvedInfoList) {
            if (resolveInfo.activityInfo.packageName.startsWith("com.twitter.android")) {
                tweetIntent.setClassName(
                        resolveInfo.activityInfo.packageName,
                        resolveInfo.activityInfo.name);
                resolved = true;
                break;
            }
        }
        if (resolved) {
            startActivity(tweetIntent);
        } else {
            Intent i = new Intent();
            i.putExtra(Intent.EXTRA_TEXT, referralString);
            i.setAction(Intent.ACTION_VIEW);
            i.setData(Uri.parse("https://twitter.com/intent/tweet?text=message&via=profileName"));
            startActivity(i);
            Toast.makeText(this, "Twitter app isn't found", Toast.LENGTH_LONG).show();
        }
    }

    public void EMAIL(View v) {
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "", null));
        intent.putExtra(Intent.EXTRA_SUBJECT, "Shop from my store on mobile using LazyLad");
        intent.putExtra(Intent.EXTRA_TEXT, referralString);
        this.startActivity(Intent.createChooser(intent, "Choose an Email client :"));
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(this.getClass().getCanonicalName());

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }
}