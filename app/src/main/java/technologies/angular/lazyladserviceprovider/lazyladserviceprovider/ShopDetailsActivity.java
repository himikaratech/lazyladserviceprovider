package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.LazyLadServiceProviderContract.RegistrationSellerDetails;

import java.util.ArrayList;

import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.CustomViews.ToggleButtonGroupTableLayout;


public class ShopDetailsActivity extends ActionBarActivity {

    int sellerServiceType;
    RadioGroup midnightDeliveryView;
    CardView midnightCardView;
    int deliveryTime;
    int minFreedelivery;
    int openTime;
    int closeTime;
    int shopOpenBits;
    int inventoryByUs;
    int logistics;
    int sodexo;
    int midnight;
    String referral;

    ToggleButtonGroupTableLayout deliveryTimeView;
    ToggleButtonGroupTableLayout minFreedeliveryView;
    ToggleButtonGroupTableLayout openTimeView;
    ToggleButtonGroupTableLayout closeTimeView;

    RadioGroup inventoryByUsView;
    RadioGroup sodexoCouponsView;
    RadioGroup logisticsServicesView;

    CheckBox shopOpenMonday;
    CheckBox shopOpenTuesday;
    CheckBox shopOpenWednesday;
    CheckBox shopOpenThursday;
    CheckBox shopOpenFriday;
    CheckBox shopOpenSaturday;
    CheckBox shopOpenSunday;

    RadioButton delTime30;
    RadioButton delTime60;
    RadioButton delTime90;
    RadioButton delTime120;
    RadioButton delTime150;
    RadioButton delTime180;

    RadioButton delAmount100;
    RadioButton delAmount200;
    RadioButton delAmount250;
    RadioButton delAmount300;
    RadioButton delAmount400;
    RadioButton delAmount500;

    RadioButton open9;
    RadioButton open10;
    RadioButton open11;
    RadioButton open12;

    RadioButton close7;
    RadioButton close8;
    RadioButton close9;
    RadioButton close10;

    RadioButton byUsYes;
    RadioButton byUsNo;

    RadioButton logisticsYes;
    RadioButton logisticsNo;

    RadioButton couponYes;
    RadioButton couponNo;

    RadioButton midnightYes;
    RadioButton midnightNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_details);
        midnightCardView = (CardView) findViewById(R.id.card_view_midnight);
        midnightDeliveryView = (RadioGroup) findViewById(R.id.midnight_delivery);

        deliveryTimeView = (ToggleButtonGroupTableLayout) findViewById(R.id.delivery_time);
        minFreedeliveryView = (ToggleButtonGroupTableLayout) findViewById(R.id.min_free_delivery);
        openTimeView = (ToggleButtonGroupTableLayout) findViewById(R.id.open_time);
        closeTimeView = (ToggleButtonGroupTableLayout) findViewById(R.id.close_time);

        inventoryByUsView = (RadioGroup) findViewById(R.id.inventory_by_us);
        sodexoCouponsView = (RadioGroup) findViewById(R.id.sodexo_coupon);
        logisticsServicesView = (RadioGroup) findViewById(R.id.logistics);

        shopOpenMonday = (CheckBox) findViewById(R.id.shop_open_monday);
        shopOpenTuesday = (CheckBox) findViewById(R.id.shop_open_tuesday);
        shopOpenWednesday = (CheckBox) findViewById(R.id.shop_open_wednesday);
        shopOpenThursday = (CheckBox) findViewById(R.id.shop_open_thursday);
        shopOpenFriday = (CheckBox) findViewById(R.id.shop_open_friday);
        shopOpenSaturday = (CheckBox) findViewById(R.id.shop_open_saturday);
        shopOpenSunday = (CheckBox) findViewById(R.id.shop_open_sunday);


        delTime30 = (RadioButton) findViewById(R.id.delTime30);
        delTime60 = (RadioButton) findViewById(R.id.delTime60);
        delTime90 = (RadioButton) findViewById(R.id.delTime90);
        delTime120 = (RadioButton) findViewById(R.id.delTime120);
        delTime150 = (RadioButton) findViewById(R.id.delTime150);
        delTime180 = (RadioButton) findViewById(R.id.delTime180);

        delAmount100 = (RadioButton) findViewById(R.id.delAmt100);
        delAmount200 = (RadioButton) findViewById(R.id.delAmt200);
        delAmount250 = (RadioButton) findViewById(R.id.delAmt250);
        delAmount300 = (RadioButton) findViewById(R.id.delAmt300);
        delAmount400 = (RadioButton) findViewById(R.id.delAmt400);
        delAmount500 = (RadioButton) findViewById(R.id.delAmt500);

        open9 = (RadioButton) findViewById(R.id.openTime9);
        open10 = (RadioButton) findViewById(R.id.openTime10);
        open11 = (RadioButton) findViewById(R.id.openTime11);
        open12 = (RadioButton) findViewById(R.id.openTime12);

        close7 = (RadioButton) findViewById(R.id.closeTime7);
        close8 = (RadioButton) findViewById(R.id.closeTime8);
        close9 = (RadioButton) findViewById(R.id.closeTime9);
        close10 = (RadioButton) findViewById(R.id.closeTime10);

        byUsYes = (RadioButton) findViewById(R.id.by_us_yes);
        byUsNo = (RadioButton) findViewById(R.id.by_us_no);

        logisticsYes = (RadioButton) findViewById(R.id.logistics_yes);
        logisticsNo = (RadioButton) findViewById(R.id.logistics_no);

        couponYes = (RadioButton) findViewById(R.id.coupon_yes);
        couponNo = (RadioButton) findViewById(R.id.coupon_no);

        midnightYes = (RadioButton) findViewById(R.id.midnight_yes);
        midnightNo = (RadioButton) findViewById(R.id.midnight_no);

        TextView delTime = (TextView) findViewById(R.id.max_del_time_textView);
        TextView delAmount = (TextView) findViewById(R.id.min_del_amt_textView);
        TextView openTimeText = (TextView) findViewById(R.id.opening_time_textView);
        TextView closeTimeText = (TextView) findViewById(R.id.closing_time_textView);
        TextView opDays = (TextView) findViewById(R.id.operational_textView);
        TextView inventoryText = (TextView) findViewById(R.id.inventory_textView);
        TextView logServices = (TextView) findViewById(R.id.logistics_textView);
        TextView sodexoText = (TextView) findViewById(R.id.sodexo_textView);
        TextView midnightText = (TextView) findViewById(R.id.midnightTextView);
        TextView referralText = (TextView) findViewById(R.id.referralTextView);

        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/Roboto_Light.ttf");
        delTime.setTypeface(typeFace);
        delAmount.setTypeface(typeFace);
        openTimeText.setTypeface(typeFace);
        closeTimeText.setTypeface(typeFace);
        opDays.setTypeface(typeFace);
        inventoryText.setTypeface(typeFace);
        logServices.setTypeface(typeFace);
        sodexoText.setTypeface(typeFace);
        midnightText.setTypeface(typeFace);
        referralText.setTypeface(typeFace);


        Intent intent = this.getIntent();
        if (intent != null)
            sellerServiceType = intent.getIntExtra("sellerServiceType", 0);

        if (sellerServiceType == 3 || sellerServiceType == 7) {
            midnightCardView.setVisibility(View.VISIBLE);
        } else {
            midnightCardView.setVisibility(View.GONE);
        }

        delTime30.setTextColor(Color.parseColor("#b3b3b3"));
        delTime30.setBackgroundColor(Color.parseColor("#ffffff"));
        delTime30.setButtonDrawable(R.drawable.inactive);
        delTime30.setBackgroundResource(R.drawable.shop_details_custom_radio);
        delTime60.setTextColor(Color.parseColor("#b3b3b3"));
        delTime60.setBackgroundColor(Color.parseColor("#ffffff"));
        delTime60.setButtonDrawable(R.drawable.inactive);
        delTime60.setBackgroundResource(R.drawable.shop_details_custom_radio);
        delTime90.setTextColor(Color.parseColor("#b3b3b3"));
        delTime90.setBackgroundColor(Color.parseColor("#ffffff"));
        delTime90.setButtonDrawable(R.drawable.inactive);
        delTime90.setBackgroundResource(R.drawable.shop_details_custom_radio);
        delTime120.setTextColor(Color.parseColor("#b3b3b3"));
        delTime120.setBackgroundColor(Color.parseColor("#ffffff"));
        delTime120.setButtonDrawable(R.drawable.inactive);
        delTime120.setBackgroundResource(R.drawable.shop_details_custom_radio);
        delTime150.setTextColor(Color.parseColor("#b3b3b3"));
        delTime150.setBackgroundColor(Color.parseColor("#ffffff"));
        delTime150.setButtonDrawable(R.drawable.inactive);
        delTime150.setBackgroundResource(R.drawable.shop_details_custom_radio);
        delTime180.setTextColor(Color.parseColor("#b3b3b3"));
        delTime180.setBackgroundColor(Color.parseColor("#ffffff"));
        delTime180.setButtonDrawable(R.drawable.inactive);
        delTime180.setBackgroundResource(R.drawable.shop_details_custom_radio);

        delAmount100.setTextColor(Color.parseColor("#b3b3b3"));
        delAmount100.setBackgroundColor(Color.parseColor("#ffffff"));
        delAmount100.setButtonDrawable(R.drawable.inactive);
        delAmount100.setBackgroundResource(R.drawable.shop_details_custom_radio);
        delAmount200.setTextColor(Color.parseColor("#b3b3b3"));
        delAmount200.setBackgroundColor(Color.parseColor("#ffffff"));
        delAmount200.setButtonDrawable(R.drawable.inactive);
        delAmount200.setBackgroundResource(R.drawable.shop_details_custom_radio);
        delAmount250.setTextColor(Color.parseColor("#b3b3b3"));
        delAmount250.setBackgroundColor(Color.parseColor("#ffffff"));
        delAmount250.setButtonDrawable(R.drawable.inactive);
        delAmount250.setBackgroundResource(R.drawable.shop_details_custom_radio);
        delAmount300.setTextColor(Color.parseColor("#b3b3b3"));
        delAmount300.setBackgroundColor(Color.parseColor("#ffffff"));
        delAmount300.setButtonDrawable(R.drawable.inactive);
        delAmount300.setBackgroundResource(R.drawable.shop_details_custom_radio);
        delAmount400.setTextColor(Color.parseColor("#b3b3b3"));
        delAmount400.setBackgroundColor(Color.parseColor("#ffffff"));
        delAmount400.setButtonDrawable(R.drawable.inactive);
        delAmount400.setBackgroundResource(R.drawable.shop_details_custom_radio);
        delAmount500.setTextColor(Color.parseColor("#b3b3b3"));
        delAmount500.setBackgroundColor(Color.parseColor("#ffffff"));
        delAmount500.setButtonDrawable(R.drawable.inactive);
        delAmount500.setBackgroundResource(R.drawable.shop_details_custom_radio);

        open9.setTextColor(Color.parseColor("#b3b3b3"));
        open9.setBackgroundColor(Color.parseColor("#ffffff"));
        open9.setButtonDrawable(R.drawable.inactive);
        open9.setBackgroundResource(R.drawable.shop_details_custom_radio);
        open10.setTextColor(Color.parseColor("#b3b3b3"));
        open10.setBackgroundColor(Color.parseColor("#ffffff"));
        open10.setButtonDrawable(R.drawable.inactive);
        open10.setBackgroundResource(R.drawable.shop_details_custom_radio);
        open11.setTextColor(Color.parseColor("#b3b3b3"));
        open11.setBackgroundColor(Color.parseColor("#ffffff"));
        open11.setButtonDrawable(R.drawable.inactive);
        open11.setBackgroundResource(R.drawable.shop_details_custom_radio);
        open12.setTextColor(Color.parseColor("#b3b3b3"));
        open12.setBackgroundColor(Color.parseColor("#ffffff"));
        open12.setButtonDrawable(R.drawable.inactive);
        open12.setBackgroundResource(R.drawable.shop_details_custom_radio);

        close7.setTextColor(Color.parseColor("#b3b3b3"));
        close7.setBackgroundColor(Color.parseColor("#ffffff"));
        close7.setButtonDrawable(R.drawable.inactive);
        close7.setBackgroundResource(R.drawable.shop_details_custom_radio);
        close8.setTextColor(Color.parseColor("#b3b3b3"));
        close8.setBackgroundColor(Color.parseColor("#ffffff"));
        close8.setButtonDrawable(R.drawable.inactive);
        close8.setBackgroundResource(R.drawable.shop_details_custom_radio);
        close9.setTextColor(Color.parseColor("#b3b3b3"));
        close9.setBackgroundColor(Color.parseColor("#ffffff"));
        close9.setButtonDrawable(R.drawable.inactive);
        close9.setBackgroundResource(R.drawable.shop_details_custom_radio);
        close10.setTextColor(Color.parseColor("#b3b3b3"));
        close10.setBackgroundColor(Color.parseColor("#ffffff"));
        close10.setButtonDrawable(R.drawable.inactive);
        close10.setBackgroundResource(R.drawable.shop_details_custom_radio);

        shopOpenMonday.setTextColor(Color.parseColor("#b3b3b3"));
        shopOpenMonday.setBackgroundColor(Color.parseColor("#ffffff"));
        shopOpenMonday.setButtonDrawable(R.drawable.inactive);
        shopOpenMonday.setBackgroundResource(R.drawable.shop_details_custom_radio);
        shopOpenTuesday.setTextColor(Color.parseColor("#b3b3b3"));
        shopOpenTuesday.setBackgroundColor(Color.parseColor("#ffffff"));
        shopOpenTuesday.setButtonDrawable(R.drawable.inactive);
        shopOpenTuesday.setBackgroundResource(R.drawable.shop_details_custom_radio);
        shopOpenWednesday.setTextColor(Color.parseColor("#b3b3b3"));
        shopOpenWednesday.setBackgroundColor(Color.parseColor("#ffffff"));
        shopOpenWednesday.setButtonDrawable(R.drawable.inactive);
        shopOpenWednesday.setBackgroundResource(R.drawable.shop_details_custom_radio);
        shopOpenThursday.setTextColor(Color.parseColor("#b3b3b3"));
        shopOpenThursday.setBackgroundColor(Color.parseColor("#ffffff"));
        shopOpenThursday.setButtonDrawable(R.drawable.inactive);
        shopOpenThursday.setBackgroundResource(R.drawable.shop_details_custom_radio);
        shopOpenFriday.setTextColor(Color.parseColor("#b3b3b3"));
        shopOpenFriday.setBackgroundColor(Color.parseColor("#ffffff"));
        shopOpenFriday.setButtonDrawable(R.drawable.inactive);
        shopOpenFriday.setBackgroundResource(R.drawable.shop_details_custom_radio);
        shopOpenSaturday.setTextColor(Color.parseColor("#b3b3b3"));
        shopOpenSaturday.setBackgroundColor(Color.parseColor("#ffffff"));
        shopOpenSaturday.setButtonDrawable(R.drawable.inactive);
        shopOpenSaturday.setBackgroundResource(R.drawable.shop_details_custom_radio);
        shopOpenSunday.setTextColor(Color.parseColor("#b3b3b3"));
        shopOpenSunday.setBackgroundColor(Color.parseColor("#ffffff"));
        shopOpenSunday.setButtonDrawable(R.drawable.inactive);
        shopOpenSunday.setBackgroundResource(R.drawable.shop_details_custom_radio);

        byUsYes.setTextColor(Color.parseColor("#b3b3b3"));
        byUsYes.setBackgroundColor(Color.parseColor("#ffffff"));
        byUsYes.setButtonDrawable(R.drawable.inactive);
        byUsYes.setBackgroundResource(R.drawable.shop_details_custom_radio);
        byUsNo.setTextColor(Color.parseColor("#b3b3b3"));
        byUsNo.setBackgroundColor(Color.parseColor("#ffffff"));
        byUsNo.setButtonDrawable(R.drawable.inactive);
        byUsNo.setBackgroundResource(R.drawable.shop_details_custom_radio);

        logisticsYes.setTextColor(Color.parseColor("#b3b3b3"));
        logisticsYes.setBackgroundColor(Color.parseColor("#ffffff"));
        logisticsYes.setButtonDrawable(R.drawable.inactive);
        logisticsYes.setBackgroundResource(R.drawable.shop_details_custom_radio);
        logisticsNo.setTextColor(Color.parseColor("#b3b3b3"));
        logisticsNo.setBackgroundColor(Color.parseColor("#ffffff"));
        logisticsNo.setButtonDrawable(R.drawable.inactive);
        logisticsNo.setBackgroundResource(R.drawable.shop_details_custom_radio);

        couponYes.setTextColor(Color.parseColor("#b3b3b3"));
        couponYes.setBackgroundColor(Color.parseColor("#ffffff"));
        couponYes.setButtonDrawable(R.drawable.inactive);
        couponYes.setBackgroundResource(R.drawable.shop_details_custom_radio);
        couponNo.setTextColor(Color.parseColor("#b3b3b3"));
        couponNo.setBackgroundColor(Color.parseColor("#ffffff"));
        couponNo.setButtonDrawable(R.drawable.inactive);
        couponNo.setBackgroundResource(R.drawable.shop_details_custom_radio);

        midnightYes.setTextColor(Color.parseColor("#b3b3b3"));
        midnightYes.setBackgroundColor(Color.parseColor("#ffffff"));
        midnightYes.setButtonDrawable(R.drawable.inactive);
        midnightYes.setBackgroundResource(R.drawable.shop_details_custom_radio);
        midnightNo.setTextColor(Color.parseColor("#b3b3b3"));
        midnightNo.setBackgroundColor(Color.parseColor("#ffffff"));
        midnightNo.setButtonDrawable(R.drawable.inactive);
        midnightNo.setBackgroundResource(R.drawable.shop_details_custom_radio);


        deliveryTimeView.setOnCheckedChangeListener(toggleListener);
        minFreedeliveryView.setOnCheckedChangeListener(toggleListener);
        openTimeView.setOnCheckedChangeListener(toggleListener);
        closeTimeView.setOnCheckedChangeListener(toggleListener);
        shopOpenMonday.setOnCheckedChangeListener(checkBoxListener);
        shopOpenTuesday.setOnCheckedChangeListener(checkBoxListener);
        shopOpenWednesday.setOnCheckedChangeListener(checkBoxListener);
        shopOpenThursday.setOnCheckedChangeListener(checkBoxListener);
        shopOpenFriday.setOnCheckedChangeListener(checkBoxListener);
        shopOpenSaturday.setOnCheckedChangeListener(checkBoxListener);
        shopOpenSunday.setOnCheckedChangeListener(checkBoxListener);

        inventoryByUsView.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                switch (id) {
                    case -1:
                        break;
                    case R.id.by_us_yes:
                        byUsYes.setTextColor(Color.parseColor("#ffffff"));
                        byUsYes.setBackgroundColor(Color.parseColor("#78bd1e"));
                        byUsYes.setButtonDrawable(R.drawable.active);
                        byUsYes.setBackgroundResource(R.drawable.shop_details_custom_radio);
                        byUsNo.setTextColor(Color.parseColor("#b3b3b3"));
                        byUsNo.setBackgroundColor(Color.parseColor("#ffffff"));
                        byUsNo.setButtonDrawable(R.drawable.inactive);
                        byUsNo.setBackgroundResource(R.drawable.shop_details_custom_radio);

                        break;
                    case R.id.by_us_no:
                        byUsYes.setTextColor(Color.parseColor("#b3b3b3"));
                        byUsYes.setBackgroundColor(Color.parseColor("#ffffff"));
                        byUsYes.setButtonDrawable(R.drawable.inactive);
                        byUsYes.setBackgroundResource(R.drawable.shop_details_custom_radio);
                        byUsNo.setTextColor(Color.parseColor("#ffffff"));
                        byUsNo.setBackgroundColor(Color.parseColor("#78bd1e"));
                        byUsNo.setButtonDrawable(R.drawable.active);
                        byUsNo.setBackgroundResource(R.drawable.shop_details_custom_radio);
                        break;
                }
            }
        });


        logisticsServicesView.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                switch (id) {
                    case -1:
                        break;
                    case R.id.logistics_yes:
                        logisticsYes.setTextColor(Color.parseColor("#ffffff"));
                        logisticsYes.setBackgroundColor(Color.parseColor("#78bd1e"));
                        logisticsYes.setButtonDrawable(R.drawable.active);
                        logisticsYes.setBackgroundResource(R.drawable.shop_details_custom_radio);
                        logisticsNo.setTextColor(Color.parseColor("#b3b3b3"));
                        logisticsNo.setBackgroundColor(Color.parseColor("#ffffff"));
                        logisticsNo.setButtonDrawable(R.drawable.inactive);
                        logisticsNo.setBackgroundResource(R.drawable.shop_details_custom_radio);

                        break;
                    case R.id.logistics_no:
                        logisticsYes.setTextColor(Color.parseColor("#b3b3b3"));
                        logisticsYes.setBackgroundColor(Color.parseColor("#ffffff"));
                        logisticsYes.setButtonDrawable(R.drawable.inactive);
                        logisticsYes.setBackgroundResource(R.drawable.shop_details_custom_radio);
                        logisticsNo.setTextColor(Color.parseColor("#ffffff"));
                        logisticsNo.setBackgroundColor(Color.parseColor("#78bd1e"));
                        logisticsNo.setButtonDrawable(R.drawable.active);
                        logisticsNo.setBackgroundResource(R.drawable.shop_details_custom_radio);
                        break;
                }
            }
        });

        sodexoCouponsView.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                switch (id) {
                    case -1:
                        break;
                    case R.id.coupon_yes:
                        couponYes.setTextColor(Color.parseColor("#ffffff"));
                        couponYes.setBackgroundColor(Color.parseColor("#78bd1e"));
                        couponYes.setButtonDrawable(R.drawable.active);
                        couponYes.setBackgroundResource(R.drawable.shop_details_custom_radio);
                        couponNo.setTextColor(Color.parseColor("#b3b3b3"));
                        couponNo.setBackgroundColor(Color.parseColor("#ffffff"));
                        couponNo.setButtonDrawable(R.drawable.inactive);
                        couponNo.setBackgroundResource(R.drawable.shop_details_custom_radio);

                        break;
                    case R.id.coupon_no:
                        couponYes.setTextColor(Color.parseColor("#b3b3b3"));
                        couponYes.setBackgroundColor(Color.parseColor("#ffffff"));
                        couponYes.setButtonDrawable(R.drawable.inactive);
                        couponYes.setBackgroundResource(R.drawable.shop_details_custom_radio);
                        couponNo.setTextColor(Color.parseColor("#ffffff"));
                        couponNo.setBackgroundColor(Color.parseColor("#78bd1e"));
                        couponNo.setButtonDrawable(R.drawable.active);
                        couponNo.setBackgroundResource(R.drawable.shop_details_custom_radio);
                        break;
                }
            }
        });

        midnightDeliveryView.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                switch (id) {
                    case -1:
                        break;
                    case R.id.midnight_yes:
                        midnightYes.setTextColor(Color.parseColor("#ffffff"));
                        midnightYes.setBackgroundColor(Color.parseColor("#78bd1e"));
                        midnightYes.setButtonDrawable(R.drawable.active);
                        midnightYes.setBackgroundResource(R.drawable.shop_details_custom_radio);
                        midnightNo.setTextColor(Color.parseColor("#b3b3b3"));
                        midnightNo.setBackgroundColor(Color.parseColor("#ffffff"));
                        midnightNo.setButtonDrawable(R.drawable.inactive);
                        midnightNo.setBackgroundResource(R.drawable.shop_details_custom_radio);

                        break;
                    case R.id.midnight_no:
                        midnightYes.setTextColor(Color.parseColor("#b3b3b3"));
                        midnightYes.setBackgroundColor(Color.parseColor("#ffffff"));
                        midnightYes.setButtonDrawable(R.drawable.inactive);
                        midnightYes.setBackgroundResource(R.drawable.shop_details_custom_radio);
                        midnightNo.setTextColor(Color.parseColor("#ffffff"));
                        midnightNo.setBackgroundColor(Color.parseColor("#78bd1e"));
                        midnightNo.setButtonDrawable(R.drawable.active);
                        midnightNo.setBackgroundResource(R.drawable.shop_details_custom_radio);
                        break;
                }
            }
        });

        Button submitButton = (Button) findViewById(R.id.next_button);
        submitButton.setOnClickListener(new submitButtonListenerImpl());
        setTracker();
    }

    private CompoundButton.OnCheckedChangeListener checkBoxListener = new CompoundButton.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch (buttonView.getId()) {
                case R.id.shop_open_monday:
                    if (isChecked) {
                        shopOpenMonday.setTextColor(Color.parseColor("#ffffff"));
                        shopOpenMonday.setBackgroundColor(Color.parseColor("#78bd1e"));
                        shopOpenMonday.setButtonDrawable(R.drawable.active);
                        shopOpenMonday.setBackgroundResource(R.drawable.shop_details_custom_radio);

                    } else {
                        shopOpenMonday.setTextColor(Color.parseColor("#b3b3b3"));
                        shopOpenMonday.setBackgroundColor(Color.parseColor("#ffffff"));
                        shopOpenMonday.setButtonDrawable(R.drawable.inactive);
                        shopOpenMonday.setBackgroundResource(R.drawable.shop_details_custom_radio);
                    }
                    break;
                case R.id.shop_open_tuesday:
                    if (isChecked) {
                        shopOpenTuesday.setTextColor(Color.parseColor("#ffffff"));
                        shopOpenTuesday.setBackgroundColor(Color.parseColor("#78bd1e"));
                        shopOpenTuesday.setButtonDrawable(R.drawable.active);
                        shopOpenTuesday.setBackgroundResource(R.drawable.shop_details_custom_radio);

                    } else {
                        shopOpenTuesday.setTextColor(Color.parseColor("#b3b3b3"));
                        shopOpenTuesday.setBackgroundColor(Color.parseColor("#ffffff"));
                        shopOpenTuesday.setButtonDrawable(R.drawable.inactive);
                        shopOpenTuesday.setBackgroundResource(R.drawable.shop_details_custom_radio);
                    }
                    break;
                case R.id.shop_open_wednesday:
                    if (isChecked) {
                        shopOpenWednesday.setTextColor(Color.parseColor("#ffffff"));
                        shopOpenWednesday.setBackgroundColor(Color.parseColor("#78bd1e"));
                        shopOpenWednesday.setButtonDrawable(R.drawable.active);
                        shopOpenWednesday.setBackgroundResource(R.drawable.shop_details_custom_radio);

                    } else {
                        shopOpenWednesday.setTextColor(Color.parseColor("#b3b3b3"));
                        shopOpenWednesday.setBackgroundColor(Color.parseColor("#ffffff"));
                        shopOpenWednesday.setButtonDrawable(R.drawable.inactive);
                        shopOpenWednesday.setBackgroundResource(R.drawable.shop_details_custom_radio);
                    }
                    break;
                case R.id.shop_open_thursday:
                    if (isChecked) {
                        shopOpenThursday.setTextColor(Color.parseColor("#ffffff"));
                        shopOpenThursday.setBackgroundColor(Color.parseColor("#78bd1e"));
                        shopOpenThursday.setButtonDrawable(R.drawable.active);
                        shopOpenThursday.setBackgroundResource(R.drawable.shop_details_custom_radio);

                    } else {
                        shopOpenThursday.setTextColor(Color.parseColor("#b3b3b3"));
                        shopOpenThursday.setBackgroundColor(Color.parseColor("#ffffff"));
                        shopOpenThursday.setButtonDrawable(R.drawable.inactive);
                        shopOpenThursday.setBackgroundResource(R.drawable.shop_details_custom_radio);
                    }
                    break;
                case R.id.shop_open_friday:
                    if (isChecked) {
                        shopOpenFriday.setTextColor(Color.parseColor("#ffffff"));
                        shopOpenFriday.setBackgroundColor(Color.parseColor("#78bd1e"));
                        shopOpenFriday.setButtonDrawable(R.drawable.active);
                        shopOpenFriday.setBackgroundResource(R.drawable.shop_details_custom_radio);

                    } else {
                        shopOpenFriday.setTextColor(Color.parseColor("#b3b3b3"));
                        shopOpenFriday.setBackgroundColor(Color.parseColor("#ffffff"));
                        shopOpenFriday.setButtonDrawable(R.drawable.inactive);
                        shopOpenFriday.setBackgroundResource(R.drawable.shop_details_custom_radio);
                    }
                    break;
                case R.id.shop_open_saturday:
                    if (isChecked) {
                        shopOpenSaturday.setTextColor(Color.parseColor("#ffffff"));
                        shopOpenSaturday.setBackgroundColor(Color.parseColor("#78bd1e"));
                        shopOpenSaturday.setButtonDrawable(R.drawable.active);
                        shopOpenSaturday.setBackgroundResource(R.drawable.shop_details_custom_radio);

                    } else {
                        shopOpenSaturday.setTextColor(Color.parseColor("#b3b3b3"));
                        shopOpenSaturday.setBackgroundColor(Color.parseColor("#ffffff"));
                        shopOpenSaturday.setButtonDrawable(R.drawable.inactive);
                        shopOpenSaturday.setBackgroundResource(R.drawable.shop_details_custom_radio);
                    }
                    break;
                case R.id.shop_open_sunday:
                    if (isChecked) {
                        shopOpenSunday.setTextColor(Color.parseColor("#ffffff"));
                        shopOpenSunday.setBackgroundColor(Color.parseColor("#78bd1e"));
                        shopOpenSunday.setButtonDrawable(R.drawable.active);
                        shopOpenSunday.setBackgroundResource(R.drawable.shop_details_custom_radio);

                    } else {
                        shopOpenSunday.setTextColor(Color.parseColor("#b3b3b3"));
                        shopOpenSunday.setBackgroundColor(Color.parseColor("#ffffff"));
                        shopOpenSunday.setButtonDrawable(R.drawable.inactive);
                        shopOpenSunday.setBackgroundResource(R.drawable.shop_details_custom_radio);
                    }
                    break;
            }
        }
    };

    private CompoundButton.OnCheckedChangeListener toggleListener = new CompoundButton.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            if (isChecked) {
                buttonView.setTextColor(Color.parseColor("#ffffff"));
                buttonView.setBackgroundColor(Color.parseColor("#78bd1e"));
                buttonView.setButtonDrawable(R.drawable.active);
                buttonView.setBackgroundResource(R.drawable.shop_details_custom_radio);
            } else {
                buttonView.setTextColor(Color.parseColor("#b3b3b3"));
                buttonView.setBackgroundColor(Color.parseColor("#ffffff"));
                buttonView.setButtonDrawable(R.drawable.inactive);
                buttonView.setBackgroundResource(R.drawable.shop_details_custom_radio);
            }
        }
    };

    private class submitButtonListenerImpl implements View.OnClickListener {
        @Override
        public void onClick(View view) {

            referral = ((EditText) findViewById(R.id.referral_editText)).getText().toString();

            if (referral.isEmpty() || referral == null) {
                referral = "SELF";
            }

            switch (deliveryTimeView.getCheckedRadioButtonId()) {
                default:
                case -1:
                    Toast.makeText(getApplicationContext(), "Please select your maximum delivery time",
                            Toast.LENGTH_SHORT).show();
                    return;
                case R.id.delTime30:
                    deliveryTime = 30;
                    break;
                case R.id.delTime60:
                    deliveryTime = 60;
                    break;
                case R.id.delTime90:
                    deliveryTime = 90;
                    break;
                case R.id.delTime120:
                    deliveryTime = 120;
                    break;
                case R.id.delTime150:
                    deliveryTime = 150;
                    break;
                case R.id.delTime180:
                    deliveryTime = 180;
                    break;
            }

            switch (minFreedeliveryView.getCheckedRadioButtonId()) {
                default:
                case -1:
                    Toast.makeText(getApplicationContext(), "Please select your minimum free delivery amount",
                            Toast.LENGTH_SHORT).show();
                    return;
                case R.id.delAmt100:
                    minFreedelivery = 100;
                    break;
                case R.id.delAmt200:
                    minFreedelivery = 200;
                    break;
                case R.id.delAmt250:
                    minFreedelivery = 250;
                    break;
                case R.id.delAmt300:
                    minFreedelivery = 300;
                    break;
                case R.id.delAmt400:
                    minFreedelivery = 400;
                    break;
                case R.id.delAmt500:
                    minFreedelivery = 500;
                    break;
            }

            switch (openTimeView.getCheckedRadioButtonId()) {
                default:
                case -1:
                    Toast.makeText(getApplicationContext(), "Please select your shop opening time",
                            Toast.LENGTH_SHORT).show();
                    return;
                case R.id.openTime9:
                    openTime = 9;
                    break;
                case R.id.openTime10:
                    openTime = 10;
                    break;
                case R.id.openTime11:
                    openTime = 11;
                    break;
                case R.id.openTime12:
                    openTime = 12;
                    break;
            }

            switch (closeTimeView.getCheckedRadioButtonId()) {
                default:
                case -1:
                    Toast.makeText(getApplicationContext(), "Please select your shop closing time",
                            Toast.LENGTH_SHORT).show();
                    return;
                case R.id.closeTime7:
                    closeTime = 12 + 7;
                    break;
                case R.id.closeTime8:
                    closeTime = 12 + 8;
                    break;
                case R.id.closeTime9:
                    closeTime = 12 + 9;
                    break;
                case R.id.closeTime10:
                    closeTime = 12 + 10;
                    break;
            }

            shopOpenBits = 0;
            shopOpenBits = (shopOpenBits << 1) | (shopOpenMonday.isChecked() ? 1 : 0);
            shopOpenBits = (shopOpenBits << 1) | (shopOpenTuesday.isChecked() ? 1 : 0);
            shopOpenBits = (shopOpenBits << 1) | (shopOpenWednesday.isChecked() ? 1 : 0);
            shopOpenBits = (shopOpenBits << 1) | (shopOpenThursday.isChecked() ? 1 : 0);
            shopOpenBits = (shopOpenBits << 1) | (shopOpenFriday.isChecked() ? 1 : 0);
            shopOpenBits = (shopOpenBits << 1) | (shopOpenSaturday.isChecked() ? 1 : 0);
            shopOpenBits = (shopOpenBits << 1) | (shopOpenSunday.isChecked() ? 1 : 0);

            switch (inventoryByUsView.getCheckedRadioButtonId()) {
                default:
                case -1:
                    return;
                case R.id.by_us_yes:
                    inventoryByUs = 1;
                    break;
                case R.id.by_us_no:
                    inventoryByUs = 0;
                    break;
            }

            switch (logisticsServicesView.getCheckedRadioButtonId()) {
                default:
                case -1:
                    return;
                case R.id.logistics_yes:
                    logistics = 1;
                    break;
                case R.id.logistics_no:
                    logistics = 0;
                    break;
            }

            switch (sodexoCouponsView.getCheckedRadioButtonId()) {
                default:
                case -1:
                    return;
                case R.id.coupon_yes:
                    sodexo = 1;
                    break;
                case R.id.coupon_no:
                    sodexo = 0;
                    break;
            }

            if ((midnightDeliveryView.getCheckedRadioButtonId()) == (-1)) {
                midnight = 0;
            } else {
                switch (midnightDeliveryView.getCheckedRadioButtonId()) {
                    default:
                    case -1:
                        return;
                    case R.id.midnight_yes:
                        midnight = 1;
                        break;
                    case R.id.midnight_no:
                        midnight = 0;
                        break;
                }
            }
            ArrayList<ShopDetailsSnippet> shopDetailsData = new ArrayList<ShopDetailsSnippet>();

            ShopDetailsSnippet shopDetails = new ShopDetailsSnippet(deliveryTime, minFreedelivery, openTime, closeTime, shopOpenBits, inventoryByUs, logistics, sodexo, midnight, referral);
            shopDetailsData.add(shopDetails);

            new ValidateSaveAndStartServiceArea(view.getContext()).execute(shopDetailsData);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_shop_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class ValidateSaveAndStartServiceArea extends AsyncTask<ArrayList<ShopDetailsSnippet>, Object, Integer> {
        Context cntx;

        public ValidateSaveAndStartServiceArea(Context context) {
            this.cntx = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(ArrayList<ShopDetailsSnippet>... shopDetailsData) {

            String[]projection={RegistrationSellerDetails.COLUMN_SELLER_PHONE};

            Cursor cursor = getContentResolver().query(RegistrationSellerDetails.CONTENT_URI,
                    projection,
                    null,
                    null,
                    null);

            if (cursor != null && cursor.moveToFirst()) {
                if (cursor.getCount() != 0) {

                    String phone = cursor.getString(0);

                    ContentValues values=new ContentValues();
                    values.put(RegistrationSellerDetails.COLUMN_SHOP_OPEN_TIME,(openTime * 60));
                    values.put(RegistrationSellerDetails.COLUMN_SHOP_CLOSE_TIME,(closeTime * 60));
                    values.put(RegistrationSellerDetails.COLUMN_DELIVERY_TIME,deliveryTime);
                    values.put(RegistrationSellerDetails.COLUMN_MIN_DELIVERY_AMT,minFreedelivery);
                    values.put(RegistrationSellerDetails.COLUMN_SHOP_OPEN_BITS, shopOpenBits);
                    values.put(RegistrationSellerDetails.COLUMN_INVENTORY_BY_US, inventoryByUs);
                    values.put(RegistrationSellerDetails.COLUMN_LOGISTICS_SERVICES, logistics);
                    values.put(RegistrationSellerDetails.COLUMN_SODEXO_COUPONS, sodexo);
                    values.put(RegistrationSellerDetails.COLUMN_MIDNIGHT_DELIVERY, midnight);
                    values.put(RegistrationSellerDetails.COLUMN_REFERRAL, referral);

                    getContentResolver().update(RegistrationSellerDetails.CONTENT_URI, values, LazyLadServiceProviderContract.RegistrationSellerDetails.COLUMN_SELLER_PHONE + " =? ", new String[]{String.valueOf(phone)});

                    return 1;
                } else {
                    return 0;
                    //TODO Some UI Element asking to add address
                }
            } else {
                return 0;
                //TODO Error Handling
            }
        }

        @Override
        protected void onPostExecute(Integer resCode) {
            super.onPostExecute(resCode);
            switch (resCode) {
                default:
                case 0:
                    //some error on server
                    //Toast.makeText(getApplicationContext(), getApplicationContext().getResources().getString(R.string.toast_sync_completed), Toast.LENGTH_SHORT).show();
                    Toast.makeText(getApplicationContext(), "Some error in Procedure, Please Try Again", Toast.LENGTH_SHORT).show();
                    break;
                case 1:
                    // success in registration
                    cntx.startActivity(new Intent(cntx, DeliveryAreas.class));
                    break;
            }
        }
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(this.getClass().getCanonicalName());

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }
}
