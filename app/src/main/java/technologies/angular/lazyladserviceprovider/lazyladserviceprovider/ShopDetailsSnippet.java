package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

/**
 * Created by Sakshi on 19/08/15.
 */
public class ShopDetailsSnippet {

    public int deliveryTime;
    public int minFreedelivery;
    public int openTime;
    public int closeTime;
    public int shopOpenBits;
    public int inventoryByUs;
    public int logistics;
    public int sodexo;
    public int midnight;
    public String referralCode;

    public ShopDetailsSnippet() {
    }

    public ShopDetailsSnippet(int del_time, int mid_del, int open_time, int close_time, int open_bits, int inventory, int log, int sodex, int mid_night, String referral) {
        deliveryTime = del_time;
        minFreedelivery = mid_del;
        openTime = open_time;
        closeTime = close_time;
        shopOpenBits = open_bits;
        inventoryByUs = inventory;
        logistics = log;
        sodexo = sodex;
        midnight = mid_night;
        referralCode = referral;
    }
}
