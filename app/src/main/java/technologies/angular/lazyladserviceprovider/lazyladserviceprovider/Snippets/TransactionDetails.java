package technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Snippets;

/**
 * Created by Amud on 08/03/16.
 */
public class TransactionDetails {

    public Double lazylad_charges;
    public Double discount;
    public Double delivery_charges;
    public Double price_difference;
    public Double wallet;

}
