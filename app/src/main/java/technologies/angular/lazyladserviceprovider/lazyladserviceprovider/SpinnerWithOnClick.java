package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.widget.Spinner;
import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by Sakshi Gupta on 22-06-2015.
 */
public class SpinnerWithOnClick extends Spinner {

    OnItemSelectedListener listener;

    public SpinnerWithOnClick(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setSelection(int position) {
        super.setSelection(position);

        if (position == getSelectedItemPosition()) {
            listener.onItemSelected(null, null, position, 0);
        }
    }

    public void setOnItemSelectedListener(OnItemSelectedListener listener) {
        this.listener = listener;
    }
}
