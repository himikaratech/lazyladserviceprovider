package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiRequestModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiResponseModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiSuggestionService;

public class SplashScreen extends Activity {

    private String m_servProvId;

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splash_screen);

        Cursor m_cursor = getContentResolver().query(
                LazyLadServiceProviderContract.UserDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    m_servProvId = m_cursor.getString(1);
                    m_cursor.moveToNext();
                }
                Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(intent);
                finish();
            } else {
                m_servProvId = "0";
                Intent intent = new Intent(SplashScreen.this, LoginActivity.class);
                startActivity(intent);
                finish();
                //TODO Some UI Element asking to add address
            }
        } else {
            m_servProvId = "0";
            Intent intent = new Intent(SplashScreen.this, LoginActivity.class);
            startActivity(intent);
            finish();
            //TODO Error Handling
        }


        callUs();
        setTracker();
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(this.getClass().getCanonicalName());

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    protected void onResume() {
        super.onResume();
    }


    private void callUs() {

        final SharedPreferences prefs = getSharedPreferences(
                "technologies.angular.lazyladserviceprovider.lazyladserviceprovider", Context.MODE_PRIVATE);


        String sp_code = m_servProvId;


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://www.angulartechnologies.com").build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
        ApiRequestModel requestModel = new ApiRequestModel();
        ApiRequestModel.CallUsInput input = requestModel.new CallUsInput(sp_code);
        apiSuggestionsService.callUs(input, new Callback<ApiResponseModel.ResponseDataCallUs>() {

            @Override
            public void success(ApiResponseModel.ResponseDataCallUs output, retrofit.client.Response response) {
                boolean success = output.success;
                if (success == true) {

                    String customer_care = output.customer_care;
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString(EditorConstants.CUSTOMER_CARE_CONSTANT, customer_care);
                    editor.commit();
                }

            }

            @Override
            public void failure(final RetrofitError error) {
                Log.e("Failure", error + " retro");

            }
        });
    }
}
