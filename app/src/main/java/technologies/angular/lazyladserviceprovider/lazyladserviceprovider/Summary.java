package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiRequestModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiResponseModel;
import technologies.angular.lazyladserviceprovider.lazyladserviceprovider.Network.ApiSuggestionService;

/**
 * Created by user on 17-07-2015.
 */
public class Summary extends Fragment {

    TextView amount;
    ImageView wallet_ImageView;
    Button settleUp;
    double amt;
    private String m_spCode;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTracker();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.summary, container, false);

        if (!Utils.isNetworkAvailable(getActivity())) {
            Toast.makeText(getActivity(), "Please Check Your Network Connectivity", Toast.LENGTH_SHORT).show();
        }

        amount = (TextView)v.findViewById(R.id.seller_amount);
        settleUp = (Button)v.findViewById(R.id.settle_up);
        wallet_ImageView=(ImageView)v.findViewById(R.id.wallet_image);
        amount.setVisibility(View.GONE);
        wallet_ImageView.setVisibility(View.GONE);

        getSummaryDetails();

        //Intent intent = new Intent();
        //amt = intent.getDoubleExtra("amount", 0);

        settleUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SettleUp.class);
                Bundle b = new Bundle();
                b.putDouble("amount", amt);
                intent.putExtras(b);
                startActivity(intent);
            }
        });
        return v;
    }

    private void getSummaryDetails() {


        Cursor m_cursor = getActivity().getContentResolver().query(
                LazyLadServiceProviderContract.UserDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    m_spCode = m_cursor.getString(1);
                    m_cursor.moveToNext();
                }
            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }
        if(m_cursor != null)m_cursor.close();

        final ProgressDialog pDialog;
        pDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
        pDialog.setMessage("Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        pDialog.show();

        String user_code = m_spCode;
        String owner_type = "1";

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://www.angulartechnologies.com/newserver/account").build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
        ApiRequestModel requestModel = new ApiRequestModel();
        ApiRequestModel.WalletInput input = requestModel.new WalletInput(user_code, owner_type);
        apiSuggestionsService.walletDetails(input, new Callback<ApiResponseModel.WalletOutput>() {

            @Override
            public void success(ApiResponseModel.WalletOutput output, retrofit.client.Response response) {
                boolean success = output.success;
                if (success == true) {
                    amt = output.wallet.balance;
                    wallet_ImageView.setVisibility(View.VISIBLE);
                    amount.setVisibility(View.VISIBLE);
                    amount.setText("₹ " + String.valueOf(amt));
                    if (pDialog.isShowing())
                        pDialog.dismiss();
                }
            }

            @Override
            public void failure(final RetrofitError error) {
                Log.e("Failure", error+" retro");
                if (pDialog.isShowing())
                    pDialog.dismiss();
            }
        });
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getActivity().getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(this.getClass().getCanonicalName());

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }

}