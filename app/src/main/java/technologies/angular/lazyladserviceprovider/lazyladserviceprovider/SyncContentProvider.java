package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.database.sqlite.SQLiteQueryBuilder;

/**
 * Created by Sakshi on 15/09/15.
 */
public class SyncContentProvider extends ContentProvider {

    private static UriMatcher sUriMatcher  = buildUriMatcher();

    private DBHelper mOpenHelper;

    private static final int USER_DETAILS = 100;
    private static final int ITEMS_NOT_AVAILABLE = 101;
    private static final int ITEMS_AVAILABLE = 102;
    private static final int ITEMS_OUT_OF_STOCK = 103;
    private static final int SERVICE_TYPE_CATEGORY = 104;
    private static final int SEARCH_RESULT = 105;
    private static final int REGISTRATION_SELLER_DETAILS = 106;
    private static final int REGISTRATION_SERVICE_AREAS = 107;
    private static final int REGISTRATION_INVENTORY_DETAILS = 108   ;
    public static final int SEARCH_QUERY=109;

    private static final SQLiteQueryBuilder sLazyLadServiceProviderQueryBuilder;

    static{
        sLazyLadServiceProviderQueryBuilder = new SQLiteQueryBuilder();
        sLazyLadServiceProviderQueryBuilder.setTables(LazyLadServiceProviderContract.UserDetails.TABLE_NAME );
        sLazyLadServiceProviderQueryBuilder.setTables(LazyLadServiceProviderContract.NonAvailableItems.TABLE_NAME );
        sLazyLadServiceProviderQueryBuilder.setTables(LazyLadServiceProviderContract.OutOfStockItems.TABLE_NAME );
        sLazyLadServiceProviderQueryBuilder.setTables(LazyLadServiceProviderContract.AvailableItems.TABLE_NAME );
        sLazyLadServiceProviderQueryBuilder.setTables(LazyLadServiceProviderContract.ServiceTypeCategories.TABLE_NAME );
        sLazyLadServiceProviderQueryBuilder.setTables(LazyLadServiceProviderContract.SearchResult.TABLE_NAME );
        sLazyLadServiceProviderQueryBuilder.setTables(LazyLadServiceProviderContract.RegistrationSellerDetails.TABLE_NAME );
        sLazyLadServiceProviderQueryBuilder.setTables(LazyLadServiceProviderContract.RegistrationSellerAreas.TABLE_NAME );
        sLazyLadServiceProviderQueryBuilder.setTables(LazyLadServiceProviderContract.RegistrationInventoryDetails.TABLE_NAME );
    }


    private static UriMatcher buildUriMatcher() {
        // I know what you're thinking.  Why create a UriMatcher when you can use regular
        // expressions instead?  Because you're not crazy, that's why.

        // All paths added to the UriMatcher have a corresponding code to return when a match is
        // found.  The code passed into the constructor represents the code to return for the root
        // URI.  It's common to use NO_MATCH as the code for this case.
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = LazyLadServiceProviderContract.CONTENT_AUTHORITY;

        // For each type of URI you want to add, create a corresponding code.
        matcher.addURI(authority, LazyLadServiceProviderContract.PATH_USER_DETAILS, USER_DETAILS);
        matcher.addURI(authority, LazyLadServiceProviderContract.PATH_ITEMS_NOT_AVAILABLE, ITEMS_NOT_AVAILABLE);
        matcher.addURI(authority, LazyLadServiceProviderContract.PATH_ITEMS_AVAILABLE, ITEMS_AVAILABLE);
        matcher.addURI(authority, LazyLadServiceProviderContract.PATH_ITEMS_OUT_OF_STOCK, ITEMS_OUT_OF_STOCK);
        matcher.addURI(authority, LazyLadServiceProviderContract.PATH_SERVICE_TYPE_CATEGORY, SERVICE_TYPE_CATEGORY);
        matcher.addURI(authority, LazyLadServiceProviderContract.PATH_SEARCH_RESULT, SEARCH_RESULT);
        matcher.addURI(authority, LazyLadServiceProviderContract.PATH_REGISTRATION_SELLER_DETAILS, REGISTRATION_SELLER_DETAILS);
        matcher.addURI(authority, LazyLadServiceProviderContract.PATH_REGISTRATION_SERVICE_AREAS, REGISTRATION_SERVICE_AREAS);
        matcher.addURI(authority, LazyLadServiceProviderContract.PATH_REGISTRATION_INVENTORY_DETAILS, REGISTRATION_INVENTORY_DETAILS);
        matcher.addURI(authority, LazyLadServiceProviderContract.PATH_SEARCH_RESULT + "/*", SEARCH_QUERY);
        matcher.addURI(authority, LazyLadServiceProviderContract.PATH_SEARCH_RESULT + "/#", SEARCH_QUERY);

        return matcher;
    }

    private Cursor getSearchResultCursor(String[] projection,String searchSelection,String[] selectionArgs,String limit) {

        return mOpenHelper.getReadableDatabase().query(LazyLadServiceProviderContract.SearchResult.TABLE_NAME,
                projection,
                searchSelection,
                selectionArgs,
                null,
                null,
                null,
                limit
        );
    }
        @Override
    public boolean onCreate() {
        mOpenHelper = new DBHelper(getContext());
        return true;
    }


    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {

        Cursor retCursor;
        switch (sUriMatcher.match(uri)) {

            case SEARCH_QUERY: {
                retCursor = getSearchResultCursor(projection, selection,selectionArgs,sortOrder);
                break;
            }

            case USER_DETAILS:
            {
                retCursor = mOpenHelper.getReadableDatabase().query(
                        LazyLadServiceProviderContract.UserDetails.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case ITEMS_NOT_AVAILABLE:
            {
                retCursor = mOpenHelper.getReadableDatabase().query(
                        LazyLadServiceProviderContract.NonAvailableItems.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case ITEMS_AVAILABLE:
            {
                retCursor = mOpenHelper.getReadableDatabase().query(
                        LazyLadServiceProviderContract.AvailableItems.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case ITEMS_OUT_OF_STOCK:
            {
                retCursor = mOpenHelper.getReadableDatabase().query(
                        LazyLadServiceProviderContract.OutOfStockItems.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case SERVICE_TYPE_CATEGORY:
            {
                retCursor = mOpenHelper.getReadableDatabase().query(
                        LazyLadServiceProviderContract.ServiceTypeCategories.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case SEARCH_RESULT:
            {
                retCursor = mOpenHelper.getReadableDatabase().query(
                        LazyLadServiceProviderContract.SearchResult.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case REGISTRATION_SELLER_DETAILS:
            {
                retCursor = mOpenHelper.getReadableDatabase().query(
                        LazyLadServiceProviderContract.RegistrationSellerDetails.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }

            case REGISTRATION_SERVICE_AREAS:
            {
                retCursor = mOpenHelper.getReadableDatabase().query(
                        LazyLadServiceProviderContract.RegistrationSellerAreas.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }

            case REGISTRATION_INVENTORY_DETAILS:
            {
                retCursor = mOpenHelper.getReadableDatabase().query(
                        LazyLadServiceProviderContract.RegistrationInventoryDetails.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }


            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;
    }

    @Override
    public String getType(Uri uri) {

        // Use the Uri Matcher to determine what kind of URI this is.
        final int match = sUriMatcher.match(uri);

        switch (match) {
            case USER_DETAILS:
                return LazyLadServiceProviderContract.UserDetails.CONTENT_TYPE;
            case ITEMS_NOT_AVAILABLE:
                return LazyLadServiceProviderContract.NonAvailableItems.CONTENT_TYPE;
            case ITEMS_AVAILABLE:
                return LazyLadServiceProviderContract.AvailableItems.CONTENT_TYPE;
            case ITEMS_OUT_OF_STOCK:
                return LazyLadServiceProviderContract.OutOfStockItems.CONTENT_TYPE;
            case SERVICE_TYPE_CATEGORY:
                return LazyLadServiceProviderContract.ServiceTypeCategories.CONTENT_TYPE;
            case SEARCH_RESULT:
                return LazyLadServiceProviderContract.SearchResult.CONTENT_TYPE;
            case REGISTRATION_SELLER_DETAILS:
                return LazyLadServiceProviderContract.RegistrationSellerDetails.CONTENT_TYPE;
            case REGISTRATION_SERVICE_AREAS:
                return LazyLadServiceProviderContract.RegistrationSellerAreas.CONTENT_TYPE;
            case REGISTRATION_INVENTORY_DETAILS:
                return LazyLadServiceProviderContract.RegistrationInventoryDetails.CONTENT_TYPE;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri returnUri;

        switch (match) {
            case USER_DETAILS: {
                long _id = db.insert(LazyLadServiceProviderContract.UserDetails.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = LazyLadServiceProviderContract.UserDetails.buildUserDetailsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case ITEMS_NOT_AVAILABLE: {
                long _id = db.insert(LazyLadServiceProviderContract.NonAvailableItems.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = LazyLadServiceProviderContract.NonAvailableItems.buildNonAvailableItemsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case ITEMS_AVAILABLE: {
                long _id = db.insert(LazyLadServiceProviderContract.AvailableItems.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = LazyLadServiceProviderContract.AvailableItems.buildAvailableItemsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case ITEMS_OUT_OF_STOCK: {
                long _id = db.insert(LazyLadServiceProviderContract.OutOfStockItems.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = LazyLadServiceProviderContract.OutOfStockItems.buildOutOfStockItemsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case SERVICE_TYPE_CATEGORY: {
                long _id = db.insert(LazyLadServiceProviderContract.ServiceTypeCategories.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = LazyLadServiceProviderContract.ServiceTypeCategories.buildServiceTypeCategoriesUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case SEARCH_RESULT: {
                long _id = db.insert(LazyLadServiceProviderContract.SearchResult.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = LazyLadServiceProviderContract.SearchResult.buildSearchResultUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case REGISTRATION_SELLER_DETAILS: {
                long _id = db.insert(LazyLadServiceProviderContract.RegistrationSellerDetails.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = LazyLadServiceProviderContract.RegistrationSellerDetails.buildRegistrationSellerDetailsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case REGISTRATION_SERVICE_AREAS: {
                long _id = db.insert(LazyLadServiceProviderContract.RegistrationSellerAreas.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = LazyLadServiceProviderContract.RegistrationSellerAreas.buildRegistrationSellerAreasUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case REGISTRATION_INVENTORY_DETAILS: {
                long _id = db.insert(LazyLadServiceProviderContract.RegistrationInventoryDetails.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = LazyLadServiceProviderContract.RegistrationInventoryDetails.buildRegistrationInventoryDetailsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsDeleted;
        switch (match) {
            case USER_DETAILS: {
                rowsDeleted = db.delete(
                        LazyLadServiceProviderContract.UserDetails.TABLE_NAME, selection, selectionArgs);
                break;
            }
            case ITEMS_NOT_AVAILABLE: {
                rowsDeleted = db.delete(
                        LazyLadServiceProviderContract.NonAvailableItems.TABLE_NAME, selection, selectionArgs);
                break;
            }
            case ITEMS_AVAILABLE: {
                rowsDeleted = db.delete(
                        LazyLadServiceProviderContract.AvailableItems.TABLE_NAME, selection, selectionArgs);
                break;
            }
            case ITEMS_OUT_OF_STOCK: {
                rowsDeleted = db.delete(
                        LazyLadServiceProviderContract.OutOfStockItems.TABLE_NAME, selection, selectionArgs);
                break;
            }
            case SERVICE_TYPE_CATEGORY: {
                rowsDeleted = db.delete(
                        LazyLadServiceProviderContract.ServiceTypeCategories.TABLE_NAME, selection, selectionArgs);
                break;
            }
            case SEARCH_RESULT: {
                rowsDeleted = db.delete(
                        LazyLadServiceProviderContract.SearchResult.TABLE_NAME, selection, selectionArgs);
                break;
            }
            case REGISTRATION_SELLER_DETAILS: {
                rowsDeleted = db.delete(
                        LazyLadServiceProviderContract.RegistrationSellerDetails.TABLE_NAME, selection, selectionArgs);
                break;
            }
            case REGISTRATION_SERVICE_AREAS: {
                rowsDeleted = db.delete(
                        LazyLadServiceProviderContract.RegistrationSellerAreas.TABLE_NAME, selection, selectionArgs);
                break;
            }
            case REGISTRATION_INVENTORY_DETAILS: {
                rowsDeleted = db.delete(
                        LazyLadServiceProviderContract.RegistrationInventoryDetails.TABLE_NAME, selection, selectionArgs);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        // Because a null deletes all rows
        if (selection == null || rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }


    @Override
    public int update(
            Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsUpdated;

        switch (match) {
            case USER_DETAILS: {
                rowsUpdated = db.update(
                        LazyLadServiceProviderContract.UserDetails.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            }
            case ITEMS_NOT_AVAILABLE: {
                rowsUpdated = db.update(
                        LazyLadServiceProviderContract.NonAvailableItems.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            }
            case ITEMS_AVAILABLE: {
                rowsUpdated = db.update(
                        LazyLadServiceProviderContract.AvailableItems.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            }
            case ITEMS_OUT_OF_STOCK: {
                rowsUpdated = db.update(
                        LazyLadServiceProviderContract.OutOfStockItems.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            }
            case SERVICE_TYPE_CATEGORY: {
                rowsUpdated = db.update(
                        LazyLadServiceProviderContract.ServiceTypeCategories.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            }
            case SEARCH_RESULT: {
                rowsUpdated = db.update(
                        LazyLadServiceProviderContract.SearchResult.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            }
            case REGISTRATION_SELLER_DETAILS: {
                rowsUpdated = db.update(
                        LazyLadServiceProviderContract.RegistrationSellerDetails.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            }
            case REGISTRATION_SERVICE_AREAS: {
                rowsUpdated = db.update(
                        LazyLadServiceProviderContract.RegistrationSellerAreas.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            }
            case REGISTRATION_INVENTORY_DETAILS: {
                rowsUpdated = db.update(
                        LazyLadServiceProviderContract.RegistrationInventoryDetails.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsUpdated;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int returnCount = 0;

        switch (match) {
            case SEARCH_RESULT: {
                db.beginTransaction();
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(LazyLadServiceProviderContract.SearchResult.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            }
            case REGISTRATION_SERVICE_AREAS: {
                db.beginTransaction();
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(LazyLadServiceProviderContract.RegistrationSellerAreas.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            }

            default:
                return super.bulkInsert(uri, values);
        }
    }
}
