package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by sakshigupta on 20/08/15.
 */
public class TabViewForOrders extends ActionBarActivity {

    Toolbar toolbar;
    ViewPager pager;
    ViewPagerAdapter adapter;
    SlidingTabLayout tabs;
    CharSequence Titles[] = {"New Orders", "Confirmed Orders", "Scheduled Orders"};
    int Numboftabs = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tabs_for_order);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Current Orders");

        adapter = new ViewPagerAdapter(getSupportFragmentManager(), Titles, Numboftabs);
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true);

        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);
        setTracker();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(this.getClass().getCanonicalName());

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }
}
