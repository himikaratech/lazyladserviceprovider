package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

/**
 * Created by Saurabh on 28/01/15.
 */

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

public class Utils {

    static String Base_Url = "http://www.angulartechnologies.com/";
    static String Rest_Api_Path = "task_manager/v1";
    static String RestApiPathUrl = Base_Url + Rest_Api_Path;

    public static boolean isNetworkAvailable(Context context) {
        NetworkInfo networkInfo = (NetworkInfo) ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if ((networkInfo == null) || (!(networkInfo.isConnected()))) return false;
        return true;
    }

    public static String strForSqlite(String str) {
        return str.replace("'", "''");
    }


    public static RestAdapter providesRestAdapter(String url) {
        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(120, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(120, TimeUnit.SECONDS);

        return new RestAdapter.Builder()
                .setEndpoint(url)
                .setClient(new OkClient(okHttpClient))
                .build();
    }
}