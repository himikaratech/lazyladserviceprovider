package technologies.angular.lazyladserviceprovider.lazyladserviceprovider;

        import android.support.v4.app.FragmentStatePagerAdapter;
        import android.support.v4.app.Fragment;
        import android.support.v4.app.FragmentManager;

/**
 * Created by user on 17-07-2015.
 */
public class ViewPagerAdapterForAccounts extends FragmentStatePagerAdapter {

    CharSequence Titles[];
    int NumbOfTabs;
    public ViewPagerAdapterForAccounts(FragmentManager fm,CharSequence mTitles[], int mNumbOfTabsumb) {
        super(fm);

        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;

    }

    @Override
    public Fragment getItem(int position) {

       /* if(position == 0)         {
            Summary summaryTab = new Summary();
            return summaryTab;
        }*/

         if(position==0)
        {
            PaymentTab paymentTab = new PaymentTab();
            return paymentTab;
        }
         else if(position==1)
         {
             ChargesTab chargesTab = new ChargesTab();
             return chargesTab;
         }
        else
        {
            Accounts accountTab = new Accounts();
            return accountTab;
        }

    }
    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    @Override
    public int getCount() {
        return NumbOfTabs;
    }
}