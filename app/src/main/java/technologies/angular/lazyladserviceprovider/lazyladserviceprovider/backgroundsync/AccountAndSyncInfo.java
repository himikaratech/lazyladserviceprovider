package technologies.angular.lazyladserviceprovider.lazyladserviceprovider.backgroundsync;

import android.accounts.Account;

/**
 * Created by Saurabh on 11/02/15.
 */
public class AccountAndSyncInfo {
    public static String ACCOUNT = "dummyaccount";
    public static final String ACCOUNT_TYPE = "AngularTechnologiesLazyLadServProv.com";
    public static final String AUTHORITY = "technologies.angular.lazyladserviceprovider.lazyladserviceprovider.provider";
    public static final long MILLISECONDS_PER_SECOND = 1000;
    public static final long SECONDS_PER_MINUTE = 60;
    public static final long SYNC_INTERVAL = 3600;
    public static final long SYNC_INTERVAL_IN_MINUTES = 60;
    Account mAccount;
}
