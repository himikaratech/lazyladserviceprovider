package technologies.angular.lazyladserviceprovider.lazyladserviceprovider.backgroundsync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Created by Saurabh on 11/02/15.
 */
public class AuthenticatorService extends Service {
    private Authenticator mAuthenticator;

    public void onCreate() {
        // Create a new authenticator object
        mAuthenticator = new Authenticator(this);
    }

    /*
     * When the system binds to this Service to make the RPC call
     * return the authenticator's IBinder.
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mAuthenticator.getIBinder();
    }
}
